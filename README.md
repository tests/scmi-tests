# System Control and Management Interface - Compliance Suite

## Introduction
**System Control and Management Interface (SCMI), which is a set of operating system-independent software interfaces that are used in system management. SCMI is extensible and currently provides interfaces for:

 - Discovery and self-description of the interfaces it supports.
 - Power domain management, which is the ability to place a given device or domain into the various power-saving states that it supports.
 - Performance management, which is the ability to control the performance of a domain that is composed of compute engines such as application processors (APs), GPUs, or other accelerators.
 - Clock management, which is the ability to set and inquire rates on platform-managed clocks.
 - Sensor management, which is the ability to read sensor data, and be notified of sensor value changes.
 - Reset domain management, which is the ability to place a given device or domain into various reset states.
 - Voltage domain management, which is the ability to configure and manage the voltage level of a domain that provides voltage supply to a group of components.
 - Power capping and monitoring, which is the ability to configure, set power caps and monitor the power consumption of power capping domains.
 - Pin control protocol, which is intended for controlling pins or groups of pins, and their configuration

For more information, download the [SCMI Platform Design Document](https://developer.arm.com/documentation/den0056/e/?lang=en).

## SCMI - Architecture Compliance Suite
SCMI compliance suite is a collection of self-checking, portable C tests. SCMI compliance suite enables adopters of SCMI Platform Design Document \(PDD\) to test their own implementations. It provides a predefined library of tests and a test framework to execute these tests. The test suite can be adapted to different platforms. It also enables the extension of the current test library to include platform-specific custom commands and custom protocols.

## Release details
 - Code quality: REL v3.2 BET-1.
 - The tests are written for version 3.2 of the SCMI PDD.
 - The compliance suite maintains backward compatibility with version 3.1, 3.0, 2.0 and 1.0 of the SCMI PDD.
 - The compliance suite is not a substitute for design verification.
 - The compliance suite is verified against [SCP-firmware](https://github.com/ARM-software/SCP-firmware).

## Gitlab branch
  Use the master branch to get the latest version of the code with bug fixes and new features.

## Test scenarios
The mapping of the rules in the specification to the test cases and the steps followed in the tests are mentioned in the [Test Scenario].

## Getting started
See the [User Guide] for instructions to adapt, build and run the test suite.

### Additional reading
For details on the design of the SCMI test suite, see [Validation Methodology Document].

## License
The software is provided under Apache 2.0 license. Contributions to this project are accepted under the same license.

## Feedback and support
Arm values and welcomes any feedback and contributions to this project.

*   For feedback, use the issue tracker that is associated with this project [Issue Tracker](https://gitlab.arm.com/tests/scmi-tests/-/issues).
*   For support, send your queries to [support-scmi-acs@arm.com](mailto:support-scmi-acs@arm.com).
*   Arm licensees can contact Arm directly through their partner managers.
*   Arm welcomes code contributions through Gitlab pull requests.


- - - - - - - - - - - - - - - - - - - -

_Copyright (c) 2021, 2023-2024, Arm Limited and Contributors. All rights reserved._

[User Guide]:           ./docs/arm_system_control_and_management_interface_test_suite_user_guide.pdf "SCMI Test Suite User Guide"
[Validation Methodology Document]:      ./docs/arm_system_control_and_management_interface_test_suite_validation_methodology.pdf "SCMI Test Suite Design"
[Test Scenario]:       ./docs/arm_system_control_and_management_interface_test_suite_test_scenario.pdf "SCMI Test Scenario"
