/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/
#ifdef PIN_CONTROL_PROTOCOL

#include "val_interface.h"
#include "val_pin_control.h"

static PIN_CONTROL_INFO_s g_pin_control_info_table;

uint32_t pin_control_supported_protocols[SUPPORTED_PIN_CONTROL_PROTOCOLS_COUNT] = {
    PIN_CONTROL_PROTOCOL_VERSION_1,
};

/**
  @brief   This API is called from app layer to execute power domain tests
  @param   none
  @return  test execution result
**/
uint32_t val_pin_control_execute_tests(void)
{
    uint32_t version = 0;

    val_memset((void *)&g_pin_control_info_table, 0, sizeof(g_pin_control_info_table));

    if (val_agent_check_protocol_support(PROTOCOL_PIN_CONTROL)) {
        if (RUN_TEST(pin_control_query_protocol_version(&version)))
            return VAL_STATUS_FAIL;

        RUN_TEST(pin_control_query_protocol_attributes());
        RUN_TEST(pin_control_query_mandatory_command_support());
        RUN_TEST(pin_control_invalid_messageid_call());
        RUN_TEST(pin_control_attributes_check());
        RUN_TEST(pin_control_attributes_invalid_param_check());
        RUN_TEST(pin_control_query_list_associations());
        RUN_TEST(pin_control_query_list_associations_invalid_id_check());
        RUN_TEST(pin_control_query_settings_get_check());
        RUN_TEST(pin_control_settings_get_invalid_id_check());
        RUN_TEST(pin_control_query_settings_configure_check());
        RUN_TEST(pin_control_settings_configure_invalid_param_check());
        RUN_TEST(pin_control_settings_configure_invalid_id_check());
        RUN_TEST(pin_control_query_function_select_check());
        RUN_TEST(pin_control_query_function_select_invalid_case_check());
        RUN_TEST(pin_control_request_control_check());
        RUN_TEST(pin_control_query_request_invalid_case_check());
        RUN_TEST(pin_control_query_release_invalid_case_check());
        RUN_TEST(pin_control_query_ext_name_check());
        RUN_TEST(pin_control_query_ext_name_get_invalid_case_check());
        RUN_TEST(pin_control_set_permissions_check());
        RUN_TEST(pin_control_set_permission_invalid_param_check());
        RUN_TEST(pin_control_set_permission_invalid_id_check());
        RUN_TEST(pin_control_query_negotiate_protocol_version(&version));
    }
    else
        val_print(VAL_PRINT_ERR, "\n Calling agent have no access to PIN CONTROL protocol");

    return VAL_STATUS_PASS;
}

/**
  @brief   This API is used to set pin_control protocol info
           1. Caller       -  Test Suite.
           2. Prerequisite -  Pin control protocol info table.
  @param   param_identifier  id of parameter which will be set
  @param   pc_id             pin control identifier
  @param   param_value       value of parameter
  @return  none
**/
void val_pin_control_save_info(uint32_t param_identifier, uint32_t pc_id, uint32_t param_value)
{
    switch (param_identifier)
    {
    case PC_NUM_PINS:
        g_pin_control_info_table.num_pins = param_value;
        break;
    case PC_NUM_GROUPS:
        g_pin_control_info_table.num_groups = param_value;
        break;
    case PC_NUM_FUNCTIONS:
        g_pin_control_info_table.num_functions = param_value;
        break;
    case PC_NUM_PINS_IN_GROUP:
        g_pin_control_info_table.num_pins_in_group[pc_id] = param_value;
        break;
    case PC_NUM_GROUPS_IN_FUNCTION:
        g_pin_control_info_table.num_groups_in_function[pc_id] = param_value;
        break;
    case PC_FUNCTION_GPIO_SUPPORT:
        g_pin_control_info_table.function_supports_gpio[pc_id] = param_value;
        break;
    case PC_FUNCTION_GROUP_SUPPORT:
        g_pin_control_info_table.function_supports_group[pc_id] = param_value;
        break;
    default:
        val_print(VAL_PRINT_WARN, "\nUnidentified parameter %d", param_identifier);
    }
}

/**
  @brief   This API is used to get pin_control protocol info
           1. Caller       -  Test Suite.
           2. Prerequisite -  Pin Control  protocol info table.
  @param   pc_id             pin control identifier
  @param   param_identifier  id of parameter which will be set
  @return  param_value       value of the parameter
**/
uint32_t val_pin_control_get_info(uint32_t pc_id, uint32_t param_identifier)
{
    uint32_t param_value = 0;

    switch (param_identifier)
    {
    case PC_NUM_PINS:
        param_value = g_pin_control_info_table.num_pins;
        break;
    case PC_NUM_GROUPS:
        param_value = g_pin_control_info_table.num_groups;
        break;
    case PC_NUM_FUNCTIONS:
        param_value = g_pin_control_info_table.num_functions;
        break;
    case PC_NUM_PINS_IN_GROUP:
        param_value = g_pin_control_info_table.num_pins_in_group[pc_id];
        break;
    case PC_NUM_GROUPS_IN_FUNCTION:
        param_value = g_pin_control_info_table.num_groups_in_function[pc_id];
        break;
    case PC_FUNCTION_GPIO_SUPPORT:
        param_value = g_pin_control_info_table.function_supports_gpio[pc_id];
        break;
    case PC_FUNCTION_GROUP_SUPPORT:
        param_value = g_pin_control_info_table.function_supports_group[pc_id];
        break;
    default:
        val_print(VAL_PRINT_WARN, "\nUnidentified parameter %d", param_identifier);
    }

    return param_value;
}

/**
  @brief   This API is used to save pin control ext name support info
           1. Caller       -  Test Suite.
           2. Prerequisite -  Pin control protocol info table.
  @param   param_id          id of parameter which we save
  @param   pc_id             pin control identifier
  @param   param_value       value of parameter
  @return  none
**/
void val_pin_control_save_ext_name_info(uint32_t param_id, uint32_t pc_id, uint32_t param_value)
{
    switch (param_id)
    {
    case PC_PIN_NAME:
        g_pin_control_info_table.pin_ext_name_support[pc_id] = param_value;
        break;
    case PC_GROUP_NAME:
        g_pin_control_info_table.group_ext_name_support[pc_id] = param_value;
        break;
    case PC_FUNCTION_NAME:
        g_pin_control_info_table.function_ext_name_support[pc_id] = param_value;
        break;
    default:
        val_print(VAL_PRINT_WARN, "\nUnidentified parameter %d", param_id);
    }
}

/**
  @brief   This API is used to get pin_control ext name support info
           1. Caller       -  Test Suite.
           2. Prerequisite -  Pin Control  protocol info table.
  @param   pc_id             pin control identifier
  @param   param_id          id of parameter which will be get
  @return  param_value       value of the parameter
**/
uint32_t val_pin_control_get_ext_name_info(uint32_t pc_id, uint32_t param_id)
{
    uint32_t param_value = 0;

    switch (param_id)
    {
    case PC_PIN_NAME:
        param_value = g_pin_control_info_table.pin_ext_name_support[pc_id];
        break;
    case PC_GROUP_NAME:
        param_value = g_pin_control_info_table.group_ext_name_support[pc_id];
        break;
    case PC_FUNCTION_NAME:
        param_value = g_pin_control_info_table.function_ext_name_support[pc_id];
        break;
    default:
        val_print(VAL_PRINT_WARN, "\nUnidentified parameter %d", param_id);
    }

    return param_value;
}

/**
  @brief   This API is used to set pin control name
           1. Caller       -  Test Suite.
           2. Prerequisite -  Base protocol info table.
  @param   param_identifier  id of parameter which will be set
  @param   pc_id             pin control identifier
  @param   param_value       value of parameter
  @return  none
**/
void val_pin_control_save_name(uint32_t param_identifier, uint32_t pc_id, uint8_t *param_value)
{
    switch (param_identifier)
    {
    case PC_PIN_NAME:
        val_strcpy(&g_pin_control_info_table.pc_pin_name[pc_id][0], param_value);
        break;
    case PC_GROUP_NAME:
        val_strcpy(&g_pin_control_info_table.pc_group_name[pc_id][0], param_value);
        break;
    case PC_FUNCTION_NAME:
        val_strcpy(&g_pin_control_info_table.pc_function_name[pc_id][0], param_value);
        break;
    default:
        val_print(VAL_PRINT_WARN, "\nUnidentified parameter %d", param_identifier);
    }
}

/**
  @brief   This API is used for checking num of pins
  @param   none
  @return  num of pins
**/
uint32_t val_pin_control_get_expected_num_pins(void)
{
    return pal_pin_control_get_expected_num_pins();
}

/**
  @brief   This API is used for checking num of groups
  @param   none
  @return  num of groups
**/
uint32_t val_pin_control_get_expected_num_groups(void)
{
    return pal_pin_control_get_expected_num_groups();
}

/**
  @brief   This API is used for checking num of functions
  @param   none
  @return  num of functions
**/
uint32_t val_pin_control_get_expected_num_functions(void)
{
    return pal_pin_control_get_expected_num_functions();
}

/**
  @brief   This API is used for checking num of pins in a group
  @param   identifier of the pin
  @return  num of pins in a group
**/
uint32_t val_pin_control_get_expected_pins_in_group(uint32_t identifier)
{
    return pal_pin_control_get_expected_pins_in_group(identifier);
}

/**
  @brief   This API is used for checking num of groups associated with the function
  @param   identifier of the function
  @return  num of groups in function
**/
uint32_t val_pin_control_get_expected_groups_in_func(uint32_t identifier)
{
    return pal_pin_control_get_expected_groups_in_func(identifier);
}

/**
  @brief   This API is used to get identifier of pin associated with the group
  @param   identifier of the group
  @return  pin identifier of group in index
**/
uint32_t val_pin_control_get_expected_group_associations(uint32_t identifier, uint32_t index)
{
    return pal_pin_control_get_expected_group_associations(identifier, index);
}

/**
  @brief   This API is used to get identifier of group associated with the function
  @param   identifier of the function
  @return  pin identifier of function in index
**/
uint32_t val_pin_control_get_expected_function_associations(uint32_t identifier, uint32_t index)
{
    return pal_pin_control_get_expected_function_associations(identifier, index);
}

/**
@brief   This API is used to check if pin or group is accessible for given agent id
           1. Caller       -  Test Suite.
  @param   agent_id     agent id
  @param   identifier   The identifier of pin or group
  @param   selector     Selector of pin or group
  @return  status       0 on access
**/
uint32_t val_pinctrl_check_access(uint32_t agent_id, uint32_t identifier, uint32_t selector)
{
    return pal_pinctrl_check_access(agent_id, identifier, selector);
}
#endif
