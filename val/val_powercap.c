/** @file
 * Copyright (c) 2021, 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifdef POWERCAP_PROTOCOL

#include "val_interface.h"
#include "val_powercap.h"

static POWERCAP_INFO_s g_powercap_info_table;

uint32_t powercap_supported_protocols[SUPPORTED_POWERCAP_PROTOCOLS_COUNT] = {
    POWERCAP_PROTOCOL_VERSION_1,
    POWERCAP_PROTOCOL_VERSION_2
};

/**
  @brief   This API is called from app layer to execute powercap tests
  @param   none
  @return  test execution result
**/
uint32_t val_powercap_execute_tests(void)
{
    uint32_t version = 0;

    if (val_agent_check_protocol_support(PROTOCOL_POWERCAP)) {
        if (RUN_TEST(powercap_query_protocol_version(&version)))
            return VAL_STATUS_FAIL;

        RUN_TEST(powercap_query_protocol_attributes());
        RUN_TEST(powercap_query_mandatory_command_support());
        RUN_TEST(powercap_invalid_messageid_call());

        if (version >= POWERCAP_PROTOCOL_VERSION_2)
            RUN_TEST(powercap_query_domain_attributes_v2());
        else
            RUN_TEST(powercap_query_domain_attributes());

        RUN_TEST(powercap_query_domain_attributes_invalid_id());
        RUN_TEST(powercap_query_get_powercap_value());
        RUN_TEST(powercap_query_invalid_domain_id());
        RUN_TEST(powercap_set_power_cap_value());
        RUN_TEST(powercap_invalid_domain_set_power_cap_value());
        RUN_TEST(powercap_invalid_data_set_power_cap_value(version));
        RUN_TEST(powercap_query_get_pai_value());
        RUN_TEST(powercap_query_pai_invalid_domain_id());
        RUN_TEST(powercap_set_pai_value());
        RUN_TEST(powercap_invalid_domain_set_pai_value());
        RUN_TEST(powercap_invalid_data_set_pai_value());
        RUN_TEST(powercap_query_domain_name());
        RUN_TEST(powercap_query_domain_name_invalid_domain());
        RUN_TEST(powercap_query_measurement());
        RUN_TEST(powercap_cap_notification_query());
        RUN_TEST(powercap_cap_notification_invalid_query());
        RUN_TEST(powercap_measurement_notification_query());
        RUN_TEST(powercap_measurement_notification_invalid_query());
        RUN_TEST(powercap_query_describe_fast_channel());
        RUN_TEST(powercap_query_describe_fast_channel_invalid_domain());
        RUN_TEST(powercap_query_describe_fast_channel_invalid_message());
        RUN_TEST(powercap_query_describe_fast_channel_unsupported_domain());

        if (version >= POWERCAP_PROTOCOL_VERSION_2)
            RUN_TEST(powercap_query_negotiate_protocol_version(&version));
   } else
        val_print(VAL_PRINT_ERR, "\n Calling agent have no access to Powercap protocol");

    return VAL_STATUS_PASS;
}


/**
  @brief   This API is used for checking num of powercap domain
  @param   none
  @return  num of powercap domain
**/
uint32_t val_powercap_get_expected_num_domains(void)
{
    return pal_powercap_get_expected_num_domains();
}

/**
  @brief   This API is used to set powercap protocol info
           1. Caller       -  Test Suite.
           2. Prerequisite -  Powercap protocol info table.
  @param   param_identifier  id of parameter which will be set
  @param   domain_id         powercap domain ID.
  @param   param_value       value of parameter
  @return  none
**/
void val_powercap_save_info(uint32_t param_identifier, uint32_t domain_id, uint32_t param_value)
{
    switch (param_identifier)
    {
    case NUM_POWERCAP_DOMAINS:
        g_powercap_info_table.num_domains = param_value;
        break;
    case POWERCAP_ATTRIBUTES:
        g_powercap_info_table.domain_info[domain_id].attributes = param_value;
        break;
    case POWERCAP_POWERCAP:
        g_powercap_info_table.domain_info[domain_id].powercap = param_value;
        break;
    case POWERCAP_PAI:
        g_powercap_info_table.domain_info[domain_id].pai = param_value;
        break;
    case POWERCAP_MIN_POWER:
        g_powercap_info_table.domain_info[domain_id].min_power = param_value;
        break;
    case POWERCAP_MAX_POWER:
        g_powercap_info_table.domain_info[domain_id].max_power = param_value;
        break;
    case POWERCAP_MSG_FAST_CHNL_SUPP:
        switch (domain_id)
        {
        case POWERCAP_CAP_GET:
           g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_cap_get = param_value;
           break;
        case POWERCAP_CAP_SET:
           g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_cap_set = param_value;
           break;
        case POWERCAP_PAI_GET:
           g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_pai_get = param_value;
           break;
        case POWERCAP_PAI_SET:
           g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_pai_set = param_value;
           break;
        default:
           val_print(VAL_PRINT_ERR, "\nUnidentified Command %d,parameter identifier = %d",
                   domain_id, param_identifier);
        }
        break;
    default:
        val_print(VAL_PRINT_ERR, "\nUnidentified parameter %d", param_identifier);
    }
}


/**
  @brief   This API is used to get powercap protocol info
           1. Caller       -  Test Suite.
           2. Prerequisite -  Powercap protocol info table.
  @param   param_identifier  id of parameter which will be set
  @return  param_value       value of the parameter
**/
uint32_t val_powercap_get_info(uint32_t param_identifier, uint32_t domain_id)
{
    uint32_t param_value = 0;

    switch (param_identifier)
    {
    case NUM_POWERCAP_DOMAINS:
        param_value = g_powercap_info_table.num_domains;
        break;
    case POWERCAP_ATTRIBUTES:
        param_value = g_powercap_info_table.domain_info[domain_id].attributes;
        break;
    case POWERCAP_POWERCAP:
        param_value = g_powercap_info_table.domain_info[domain_id].powercap;
        break;
    case POWERCAP_PAI:
        param_value = g_powercap_info_table.domain_info[domain_id].pai;
        break;
    case POWERCAP_MIN_POWER:
        param_value = g_powercap_info_table.domain_info[domain_id].min_power;
        break;
    case POWERCAP_MAX_POWER:
        param_value = g_powercap_info_table.domain_info[domain_id].max_power;
        break;
    case POWERCAP_MSG_FAST_CHNL_SUPP:
         switch (domain_id)
         {
         case POWERCAP_CAP_GET:
             param_value = g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_cap_get;
             break;
         case POWERCAP_CAP_SET:
             param_value = g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_cap_set;
             break;
         case POWERCAP_PAI_GET:
             param_value = g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_pai_get;
             break;
         case POWERCAP_PAI_SET:
             param_value = g_powercap_info_table.pc_cmd_fast_ch_supp.powercap_pai_set;
             break;
         default:
             val_print(VAL_PRINT_ERR, "\nUnidentified Command %d,parameter identifier = %d",
                     domain_id, param_identifier);
         }
         break;
    default:
        val_print(VAL_PRINT_ERR, "\nUnidentified parameter %d", param_identifier);
    }

    return param_value;
}

/**
  @brief   This API is used for checking powercap domain name
  @param   domain id
  @return  powercap domain name
**/
uint8_t *val_powercap_get_expected_name(uint32_t domain_id)
{
    return pal_powercap_get_expected_name(domain_id);
}


/**
  @brief   This API is used to get saved power cap domain attribute.
  @param   domain_id      domain ID
  @return  attribute value
**/
uint32_t val_get_domain_powercap_attribute(uint32_t domain_id, uint32_t flag)
{
    uint32_t attribute;

    attribute = val_powercap_get_info(POWERCAP_ATTRIBUTES, domain_id);
    return val_get_powercap_attributes_flag(attribute, flag);
}


/**
  @breif    This API is used to get configuration flags from domain attributes
  @param    attribute     attribute value for domain
  @param    flag          powercap attribute flag.
  @return   flag value
**/
uint32_t val_get_powercap_attributes_flag(uint32_t attribute, uint32_t flag)
{
    uint32_t flag_value = 0;

    switch (flag)
    {
    case PC_CHANGE_NOTIFY_SUPP:
        flag_value = VAL_EXTRACT_BITS(attribute, 31, 31);
        break;
    case PC_MEASUR_CHANG_NOTIFY_SUPP:
        flag_value = VAL_EXTRACT_BITS(attribute, 30, 30);
        break;
    case PC_ASYNC_PC_SET_SUPP:
        flag_value = VAL_EXTRACT_BITS(attribute, 29, 29);
        break;
    case PC_EXTENDED_NAME:
        flag_value = VAL_EXTRACT_BITS(attribute, 28, 28);
        break;
    case PC_CONFIG_SUPP:
        flag_value = VAL_EXTRACT_BITS(attribute, 27, 27);
        break;
    case PC_POWER_MONITOR_SUPP:
        flag_value = VAL_EXTRACT_BITS(attribute, 26, 26);
        break;
    case PC_PAI_CONFIG_SUPP:
        flag_value = VAL_EXTRACT_BITS(attribute, 25, 25);
        break;
    case PC_POWER_UNIT:
        flag_value = VAL_EXTRACT_BITS(attribute, 23, 24);
        break;
    case PC_FASTCHANNEL_SUPPORT:
        flag_value = VAL_EXTRACT_BITS(attribute, 22, 22);
        break;
    default:
        val_print(VAL_PRINT_ERR, "\nUnidentified parameter %d", flag);
    }

    return flag_value;
}

uint32_t val_get_cap_set_flag(uint32_t async_resp_mode, uint32_t ignore_resp_mode)
{
    uint32_t flag = 0;

    flag = flag | (async_resp_mode << 1);
    flag = flag | ignore_resp_mode;

    return flag;
}
#endif
