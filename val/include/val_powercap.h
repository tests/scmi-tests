/** @file
 * Copyright (c) 2021, 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef __VAL_POWERCAP_H__
#define __VAL_POWERCAP_H__

#define ATTRIBUTE_OFFSET              0
#define NAME_OFFSET                   1
#define MIN_PAI_OFFSET                5
#define MAX_PAI_OFFSET                6
#define PAI_STEP_OFFSET               7
#define MIN_POWER_CAP_OFFSET          8
#define MAX_POWER_CAP_OFFSET          9
#define POWER_CAP_STEP_OFFSET         10
#define SUSTAINAIBLE_POWER_OFFSET     11
#define ACCURACY_OFFSET               12
#define PARENT_ID_OFFSET              13

#define MAX_NUM_OF_POWERCAP_DOMAINS   32
#define MAX_NUMBER_POWERCAP_LEVELS    32

#define POWERCAP_OFFSET               0
#define PAI_OFFSET                    0
#define PC_EXT_NAME_OFFSET            1
#define MSR_POWER_OFFSET              0
#define MSR_PAI_OFFSET                1

#define FAST_CH_RATE_LIMIT_OFFSET              1
#define CHAN_ADDR_LOW_OFFSET                   2
#define CHAN_ADDR_HIGH_OFFSET                  3
#define CHAN_SIZE_OFFSET                       4
#define DOORBELL_ADDR_LOW_OFFSET               5
#define DOORBELL_ADDR_HIGH_OFFSET              6
#define DOORBELL_MASK_LOW_OFFSET               7
#define DOORBELL_MASK_HIGH_OFFSET              8
#define DOORBELL_PRESERVE_LOW_OFFSET           9
#define DOORBELL_PRESERVE_HIGH_OFFSET          10
#define DELAYED_RESP_DOMAIN_ID_OFFSET          0
#define DELAYED_RESP_POWER_CAP_OFFSET          1
#define DEL_RESP_CAP_CHNG_AGNT_ID_OFFSET       0
#define DEL_RESP_CAP_CHNG_DMN_ID_OFFSET        1
#define DEL_RESP_CAP_CHNG_PC_ID_OFFSET         2
#define DEL_RESP_CAP_CHNG_PAI_ID_OFFSET        3

typedef struct {
    uint32_t attributes;
    uint32_t powercap;
    uint32_t pai;
    uint32_t min_power;
    uint32_t max_power;
} POWERCAP_DOMAIN_INFO_s;

typedef struct {
    uint8_t  powercap_cap_get;
    uint8_t  powercap_cap_set;
    uint8_t  powercap_pai_get;
    uint8_t  powercap_pai_set;
} POWERCAP_CMD_FAST_CH_SUPPORT_s;

typedef struct {
    uint32_t num_domains;
    POWERCAP_CMD_FAST_CH_SUPPORT_s pc_cmd_fast_ch_supp;
    POWERCAP_DOMAIN_INFO_s domain_info[MAX_NUM_OF_POWERCAP_DOMAINS];
} POWERCAP_INFO_s;


/* Common Tests */
uint32_t powercap_query_protocol_version(uint32_t *version);
uint32_t powercap_query_protocol_attributes(void);
uint32_t powercap_query_mandatory_command_support(void);
uint32_t powercap_invalid_messageid_call(void);
uint32_t powercap_query_domain_attributes(void);
uint32_t powercap_query_domain_attributes_invalid_id(void);
uint32_t powercap_query_get_powercap_value(void);
uint32_t powercap_query_invalid_domain_id(void);
uint32_t powercap_set_power_cap_value(void);
uint32_t powercap_invalid_domain_set_power_cap_value(void);
uint32_t powercap_query_get_pai_value(void);
uint32_t powercap_query_pai_invalid_domain_id(void);
uint32_t powercap_set_pai_value(void);
uint32_t powercap_invalid_domain_set_pai_value(void);
uint32_t powercap_query_domain_name(void);
uint32_t powercap_query_measurement(void);
uint32_t powercap_cap_notification_query(void);
uint32_t powercap_measurement_notification_query(void);
uint32_t powercap_query_describe_fast_channel(void);
uint32_t powercap_query_describe_fast_channel_invalid_domain(void);
uint32_t powercap_query_describe_fast_channel_invalid_message(void);
uint32_t powercap_query_describe_fast_channel_unsupported_domain(void);
uint32_t powercap_invalid_data_set_pai_value(void);
uint32_t powercap_query_domain_name_invalid_domain(void);
uint32_t powercap_cap_notification_invalid_query(void);
uint32_t powercap_measurement_notification_invalid_query(void);
uint32_t powercap_invalid_data_set_power_cap_value(uint32_t version);

/* Version 2 */
uint32_t powercap_query_domain_attributes_v2(void);
uint32_t powercap_query_negotiate_protocol_version(uint32_t *version);

/* VAL Apis used in tests */
uint32_t val_powercap_get_expected_num_domains(void);
uint8_t *val_powercap_get_expected_name(uint32_t domain_id);
uint32_t val_get_powercap_attributes_flag(uint32_t attribute, uint32_t flag);
void val_powercap_save_info(uint32_t param_identifier, uint32_t domain_id, uint32_t param_value);
uint32_t val_powercap_get_info(uint32_t param_identifier, uint32_t domain_id);
uint32_t val_get_domain_powercap_attribute(uint32_t domain_id, uint32_t flag);
uint32_t val_get_cap_set_flag(uint32_t async_resp_mode, uint32_t ignore_resp_mode);
#endif
