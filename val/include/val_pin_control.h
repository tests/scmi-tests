/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef __VAL_PIN_CONTROL_H__
#define __VAL_PIN_CONTROL_H__

#define MAX_PIN_IDENTIFIERS                    128
#define MAX_GROUP_IDENTIFIERS                  128
#define MAX_FUNCTIONS_IDENTIFIERS              128
#define PIN_SELECTOR                           0
#define GROUP_SELECTOR                         1
#define FUNCTION_SELECTOR                      2
#define GET_NO_CONFIGS                         2
#define GET_ALL_CONFIGS                        1
#define NO_FUNCTION_ENABLED                    0xFFFFFFFF

#define VERSION_OFFSET                         0
#define ATTRIBUTES_LOW_OFFSET                  0
#define ATTRIBUTES_HIGH_OFFSET                 1
#define ATTRIBUTE_OFFSET                       0
#define NAME_OFFSET                            1
#define RET_FLAGS_OFFSET                       0
#define PIN_CONTROL_IDS_ARRAY_OFFSET           1
#define RET_FUNCTION_SELECTED_OFFSET           0
#define RET_NUM_CONFIGS_OFFSET                 1
#define RET_CONFIGS_ARRAY_OFFSET               2
#define FUNCTION_SELECT_FLAGS_RESERVED_LOW     2
#define REQUEST_FLAGS_RESERVED_LOW             2
#define RELEASE_FLAGS_RESERVED_LOW             2
#define PIN_CONTROL_EXT_NAME_OFFSET            1

#define GET_ALL_CONFIGS_LOW                    18
#define SELECTOR_LOW                           16
#define SKIP_CONFIGS_LOW                       8
#define SET_CONFIGS_LOW                        2
#define SET_FUNCTION                           1
#define FUNCTION_ID_VALID                      10
#define FUNCTION_IGNORED                       0
#define PINCTRL_ACCESS_DENY                    0
#define PINCTRL_ACCESS_ALLOW                   1
#define PINCTRL_ACCESS_OFFSET                  2

#define DEFAULT_CONFIG_TYPE                    0
#define PINCTRL_SET_INVALID_ATTIBUTE           3
#define RESERVED_CONFIG_TYPE                   21
#define RESERVED_FLAGS_SET_PERM_LOW            3

typedef struct {
    uint32_t num_pins;
    uint32_t num_groups;
    uint32_t num_functions;
    uint16_t num_pins_in_group[MAX_GROUP_IDENTIFIERS];
    uint16_t num_groups_in_function[MAX_FUNCTIONS_IDENTIFIERS];
    uint8_t  pc_pin_name[MAX_PIN_IDENTIFIERS][SCMI_NAME_STR_SIZE];
    uint8_t  pc_group_name[MAX_GROUP_IDENTIFIERS][SCMI_NAME_STR_SIZE];
    uint8_t  pc_function_name[MAX_FUNCTIONS_IDENTIFIERS][SCMI_NAME_STR_SIZE];
    uint32_t pin_ext_name_support[MAX_PIN_IDENTIFIERS];
    uint32_t group_ext_name_support[MAX_GROUP_IDENTIFIERS];
    uint32_t function_ext_name_support[MAX_FUNCTIONS_IDENTIFIERS];
    uint32_t function_supports_gpio[MAX_FUNCTIONS_IDENTIFIERS];
    uint32_t function_supports_group[MAX_FUNCTIONS_IDENTIFIERS];
} PIN_CONTROL_INFO_s;

/* Tests*/
uint32_t pin_control_query_protocol_version(uint32_t *version);
uint32_t pin_control_query_protocol_attributes(void);
uint32_t pin_control_query_mandatory_command_support(void);
uint32_t pin_control_invalid_messageid_call(void);
uint32_t pin_control_attributes_check(void);
uint32_t pin_control_attributes_invalid_param_check(void);
uint32_t pin_control_query_list_associations(void);
uint32_t pin_control_query_list_associations_invalid_id_check(void);
uint32_t pin_control_query_settings_get_check(void);
uint32_t pin_control_settings_get_invalid_id_check(void);
uint32_t pin_control_query_settings_configure_check(void);
uint32_t pin_control_settings_configure_invalid_param_check(void);
uint32_t pin_control_settings_configure_invalid_id_check(void);
uint32_t pin_control_query_function_select_check(void);
uint32_t pin_control_query_function_select_invalid_case_check(void);
uint32_t pin_control_request_control_check(void);
uint32_t pin_control_query_request_invalid_case_check(void);
uint32_t pin_control_query_release_invalid_case_check(void);
uint32_t pin_control_query_ext_name_check(void);
uint32_t pin_control_query_ext_name_get_invalid_case_check(void);
uint32_t pin_control_set_permission_invalid_param_check(void);
uint32_t pin_control_set_permission_invalid_id_check(void);
uint32_t pin_control_set_permissions_check(void);
uint32_t pin_control_query_negotiate_protocol_version(uint32_t *version);

uint32_t val_pin_control_get_expected_num_pins(void);
uint32_t val_pin_control_get_expected_num_groups(void);
uint32_t val_pin_control_get_expected_num_functions(void);
uint32_t val_pin_control_get_expected_pins_in_group(uint32_t identifier);
uint32_t val_pin_control_get_expected_groups_in_func(uint32_t identifier);
uint32_t val_pin_control_get_expected_group_associations(uint32_t identifier, uint32_t index);
uint32_t val_pin_control_get_expected_function_associations(uint32_t identifier, uint32_t index);
uint32_t val_pin_control_get_ext_name_info(uint32_t pc_id, uint32_t param_id);
void val_pin_control_save_ext_name_info(uint32_t param_id, uint32_t pc_id, uint32_t param_value);
#endif /* __VAL_PIN_CONTROL_H__ */
