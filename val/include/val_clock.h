/** @file
 * Copyright (c) 2019-2020, 2023-2025, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef __VAL_CLOCK_H__
#define __VAL_CLOCK_H__

#define VERSION_OFFSET                     0
#define ATTRIBUTE_OFFSET                   0
#define NAME_OFFSET                        1
#define ENABLE_DELAY_OFFSET                5
#define RATE_FLAG_OFFSET                   0
#define RATES_OFFSET                       1
#define RATE_LOWER_WORD_OFFSET             0
#define RATE_UPPER_WORD_OFFSET             1
#define CLOCK_EXT_NAME_OFFSET              1
#define CLOCK_CONFIG_OFFSET                1
#define CLOCK_CONFIG_EXT_VAL_OFFSET        2
#define CLOCK_PARENT_NAME_OFFSET           0
#define CLOCK_PARENTS_FLAGS_OFFSET         0
#define CLOCK_PARENT_START_OFFSET          0
#define CLOCK_PARENTS_ARRAY_OFFSET         1
#define CLOCK_PERMISSIONS_OFFSET           0
#define PERMISSION_OFFSET                  0

#define DELAYED_RESP_CLOCK_ID_OFFSET       0
#define DELAYED_RESP_RATE_LOWER_OFFSET     1
#define DELAYED_RESP_RATE_UPPER_OFFSET     2


#define INVALID_ATTRIBUTE                  0xF
#define CONFIG_SET_ENABLE                  0x1
#define CONFIG_SET_DISABLE                 0x0
#define CLOCK_SET_ASYNC_MODE               0x1
#define CLOCK_SET_SYNC_MODE                0x0
#define CLOCK_SET_INVALID_FLAG             0xFF
#define MASK_FOR_LOWER_WORD                0xFFFFFFFFLL
#define MASK_FOR_UPPER_WORD                0xFFFFFFFF00000000LL
#define CLOCK_RATE_FMT_ARRAY               0x0
#define CLOCK_RATE_FMT_STEP_SIZE           0x1
#define CLOCK_DUTY_CYCLE_EXT_CONFIG        0x1
#define CLOCK_PHASE_EXT_CONFIG             0x2
#define CLOCK_ATTRIBUTES_STATE_MASK        0x1

#define MAX_NUM_OF_CLOCKS 10

typedef struct {
    uint64_t lowest_rate;
    uint64_t highest_rate;
} CLOCK_RATE_INFO_s;

typedef struct {
    uint32_t num_clocks;
    uint32_t max_async_cmd;
    uint32_t clock_attribute[MAX_NUM_OF_CLOCKS];
    uint32_t num_possible_parents[MAX_NUM_OF_CLOCKS];
    uint8_t  clock_name[MAX_NUM_OF_CLOCKS][SCMI_NAME_STR_SIZE];
    uint32_t parent_id[MAX_NUM_OF_CLOCKS][MAX_NUM_OF_CLOCKS];
    CLOCK_RATE_INFO_s clock_rate[MAX_NUM_OF_CLOCKS];
} CLOCK_INFO_s;

/* Common Tests */
uint32_t clock_query_protocol_version(uint32_t *version);
uint32_t clock_query_protocol_attributes(void);
uint32_t clock_query_mandatory_command_support(void);
uint32_t clock_invalid_messageid_call(void);
uint32_t clock_attributes_check(uint32_t version);
uint32_t clock_attributes_check_invalid_clock_id(void);
uint32_t clock_query_describe_rates(void);
uint32_t clock_query_describe_rates_invalid_clock_id(void);
uint32_t clock_rate_set_sync_check(void);
uint32_t clock_rate_set_async_check(void);
uint32_t clock_rate_set_invalid_param_check(void);
uint32_t clock_rate_set_invalid_clock_id_check(void);
uint32_t clock_rate_get_check(void);
uint32_t clock_rate_get_invalid_clock_id_check(void);
uint32_t clock_config_set_invalid_attr_check(void);
uint32_t clock_config_set_invalid_clock_id_check(void);
uint32_t clock_config_set_check(uint32_t version);

/* V2 Test cases */
uint32_t clock_query_ext_domain_name(void);
uint32_t clock_query_domain_ext_name_invalid_domain(void);
uint32_t clock_rate_notification_query(void);
uint32_t clock_rate_notification_invalid_query(void);
uint32_t clock_rate_req_notification_query(void);
uint32_t clock_rate_req_notification_invalid_query(void);
uint32_t clock_parent_get_invalid_clock_id_check(void);

/* V3 Test cases */
uint32_t clock_query_mandatory_command_support_v3(void);
uint32_t clock_parent_name_support_get_check(void);
uint32_t clock_parent_name_support_set_check(void);
uint32_t clock_parent_set_invalid_check(void);
uint32_t clock_parent_set_invalid_clock_id_check(void);
uint32_t clock_config_get_check(void);
uint32_t clock_config_get_invalid_clock_id_check(void);
uint32_t clock_possible_parents_get_check(void);
uint32_t clock_possible_parents_get_invalid_id_check(void);
uint32_t clock_rate_set_sync_check_v3(void);
uint32_t clock_rate_set_async_check_v3(void);
uint32_t clock_query_negotiate_protocol_version(uint32_t *version);
uint32_t clock_get_permissions_check(void);

/* expected values */
uint32_t val_clock_get_expected_num_clocks(void);
uint32_t val_clock_get_expected_max_async_cmd(void);
uint32_t val_clock_get_expected_number_of_rates(uint32_t clock_id);
uint32_t val_check_clock_config_change_support(uint32_t clock_id);
uint32_t val_get_clock_attribute(uint32_t clock_id, uint32_t flag);
uint32_t val_get_clock_attributes_flag(uint32_t attribute, uint32_t flag);
#endif
