/** @file
 * Copyright (c) 2023, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 15)
#define TEST_DESC "pin ctrl function select invalid case check  "
#define PARAMETER_SIZE 3

uint32_t pin_control_query_function_select_invalid_case_check(void)
{
    int32_t  status;
    size_t   return_value_count;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t num_pins = 0, num_functions = 0, attributes = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    num_functions = val_pin_control_get_info(0, PC_NUM_FUNCTIONS);
    if (num_functions == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control function identifiers found");
        return VAL_STATUS_SKIP;
    }

    /* Query pin control function select with invalid identifier */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
    } else {

        val_print(VAL_PRINT_TEST, "\n     [Check 1] Query function select for invalid pin id");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        /* pins 0 to num_pins - 1 are valid, using num_pins as invalid pin */
        parameters[param_count++] = num_pins;
        attributes = (GET_NO_CONFIGS << GET_ALL_CONFIGS_LOW) +
            (PIN_SELECTOR << SELECTOR_LOW);
        parameters[param_count++] = attributes;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                     PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                &return_value_count, return_values);

        if (val_compare_status(status, SCMI_NOT_FOUND) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_TEST,
              "\n     [Check 2] Set function select for invalid function id");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = num_pins - 1;
        /* functions 0 to num_functions - 1 are valid, using num_functions as invalid pin */
        parameters[param_count++] = num_functions;
        attributes = (SET_FUNCTION << FUNCTION_ID_VALID) + PIN_SELECTOR;
        parameters[param_count++] = attributes;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                &return_value_count, return_values);

        if (val_compare_status(status, SCMI_NOT_FOUND) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_TEST,
              "\n     [Check 3] Query function select for invalid flags");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = num_pins - 1;
        parameters[param_count++] = num_functions - 1;
        /* Making reserved bit as 1 to make invalid flag */
        attributes = (1 << (FUNCTION_ID_VALID + 1)) +
        (SET_FUNCTION << FUNCTION_ID_VALID) + PIN_SELECTOR;
        parameters[param_count++] = attributes;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                    PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                &return_value_count, return_values);

        if (val_compare_status(status, SCMI_INVALID_PARAMETERS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;
    }

    return VAL_STATUS_PASS;
}
