/** @file
 * Copyright (c) 2023, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 14)
#define TEST_DESC "pin ctrl function select check               "
#define PARAMETER_SIZE 3

uint32_t pin_control_query_function_select_check(void)
{
    int32_t  status;
    size_t   return_value_count;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t num_groups = 0, num_pins = 0, num_functions = 0;
    uint32_t pc_id = 0, function_id = 0, attributes = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    num_functions = val_pin_control_get_info(0, PC_NUM_FUNCTIONS);
    if (num_functions == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control function identifiers found");
        return VAL_STATUS_SKIP;
    }

    /* Query pin control function select for pin identifiers */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);

        /* Check function select for each pin identifier*/
        for (pc_id = 0; pc_id < num_pins; pc_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pc_id);

            for (function_id = 0; function_id < num_functions; function_id++)
            {
                val_print(VAL_PRINT_TEST, "\n    PIN CONTROL FUNCTION ID: %d", function_id);
                val_print(VAL_PRINT_TEST, "\n     [Check 1] Query function select for pin ids");

                VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
                parameters[param_count++] = pc_id;
                attributes = (GET_NO_CONFIGS << GET_ALL_CONFIGS_LOW) +
                             (PIN_SELECTOR << SELECTOR_LOW);
                parameters[param_count++] = attributes;

                cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
                val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

                if (!((val_compare_status(status, SCMI_SUCCESS) == VAL_STATUS_PASS) ||
                    (val_compare_status(status, SCMI_NOT_SUPPORTED) == VAL_STATUS_PASS) ||
                    (val_compare_status(status, SCMI_IN_USE) == VAL_STATUS_PASS)))
                    return VAL_STATUS_FAIL;

                if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                val_print_return_values(return_value_count, return_values);

                val_print(VAL_PRINT_DEBUG, "\n       SELECTED PIN FUNCTION: %d",
                return_values[RET_FUNCTION_SELECTED_OFFSET]);
            }
        }
    }

    /* check function select for group identifiers */
    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUPS    : %d", num_groups);

        /* Check function select for each group identifier*/
        for (pc_id = 0; pc_id < num_groups; pc_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL GROUP ID: %d", pc_id);

            for (function_id = 0; function_id < num_functions; function_id++)
            {
                val_print(VAL_PRINT_TEST, "\n    PIN CONTROL FUNCTION ID: %d", function_id);
                val_print(VAL_PRINT_TEST, "\n     [Check 2] Query function select for group ids");
                VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
                parameters[param_count++] = pc_id;
                attributes = (GET_NO_CONFIGS << GET_ALL_CONFIGS_LOW) +
                             (GROUP_SELECTOR << SELECTOR_LOW);
                parameters[param_count++] = attributes;

                cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
                val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

                if (!((val_compare_status(status, SCMI_SUCCESS) == VAL_STATUS_PASS) ||
                    (val_compare_status(status, SCMI_NOT_SUPPORTED) == VAL_STATUS_PASS) ||
                    (val_compare_status(status, SCMI_IN_USE) == VAL_STATUS_PASS)))
                    return VAL_STATUS_FAIL;

                if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                val_print_return_values(return_value_count, return_values);

                val_print(VAL_PRINT_DEBUG, "\n       SELECTED GROUP FUNCTION: %d",
                return_values[RET_FUNCTION_SELECTED_OFFSET]);
           }
        }
    }

    return VAL_STATUS_PASS;
}

