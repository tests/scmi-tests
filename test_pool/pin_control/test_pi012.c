/** @file
 * Copyright (c) 2023, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 12)
#define TEST_DESC "pin ctrl settings config invalid param check "

#define PARAMETER_SIZE      5
#define CONFIG_NUM_SET      1

#define CONFIG_ENABLE       1
#define DEFAULT_CONFIG_TYPE 0

uint32_t pin_control_settings_configure_invalid_param_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t attributes = 0;
    uint32_t  num_pins = 0;
    uint32_t pc_id = 0, function_selected = 0;
    uint32_t config_value, config_type;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);


        for (pc_id = 0; pc_id < num_pins; pc_id++)
        {
          val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pc_id);
          val_print(VAL_PRINT_TEST, "\n     [Check 1] Set with invalid Config Type ");

          config_type = RESERVED_CONFIG_TYPE;
          config_value = CONFIG_ENABLE;

        /* Query with invalid config type should fail with INVALID_PARAMETERS*/
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = pc_id;
        parameters[param_count++] = function_selected;
        attributes = (CONFIG_NUM_SET << SET_CONFIGS_LOW) + PIN_SELECTOR;
        parameters[param_count++] = attributes;
        parameters[param_count++] = config_type;
        parameters[param_count++] = config_value;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                               PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

        if (val_compare_status(status, SCMI_INVALID_PARAMETERS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_TEST, "\n     [Check 2] Query with invalid attribute");

        /* Query with invalid attribute should fail with INVALID_PARAMETERS*/
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = pc_id;
        parameters[param_count++] = function_selected;
        parameters[param_count++] = PINCTRL_SET_INVALID_ATTIBUTE;
        parameters[param_count++] = DEFAULT_CONFIG_TYPE;
        parameters[param_count++] = config_value;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_INVALID_PARAMETERS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;
       }
}
return VAL_STATUS_PASS;
}
