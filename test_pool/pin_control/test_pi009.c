/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 9)
#define TEST_DESC "pin ctrl settings get check                  "
#define PARAMETER_SIZE 2

uint32_t pin_control_query_settings_get_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t attributes = 0;
    uint32_t num_groups = 0, num_pins = 0;
    uint32_t pc_id = 0, skip_configs = 0;;
    uint32_t num_remaining_configs, num_configs_returned;
    uint32_t i, *pin_configs_array;
    uint32_t entry_0, entry_1;
    uint32_t function_selected;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Query pin control config get for pin identifiers */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);

        /* Get configs of each pin identifier*/
        for (pc_id = 0; pc_id < num_pins; pc_id++)
        {
            num_remaining_configs = 0;
            num_configs_returned = 0;
            skip_configs = 0;

            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Query config get");

            /* Get all the available configs for each pin identifier */
            do
            {
                VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
                parameters[param_count++] = pc_id;
                attributes = (GET_ALL_CONFIGS << GET_ALL_CONFIGS_LOW) |
                             (PIN_SELECTOR << SELECTOR_LOW) |
                             (skip_configs << SKIP_CONFIGS_LOW);
                parameters[param_count++] = attributes;

                cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
                val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

                if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                val_print_return_values(return_value_count, return_values);

                function_selected = return_values[RET_FUNCTION_SELECTED_OFFSET];
                if (function_selected != NO_FUNCTION_ENABLED) {
                    val_print(VAL_PRINT_DEBUG, "\n       PIN ENABLES FUNCTION: %d",
                                                function_selected);
                }

                num_remaining_configs =
                        VAL_EXTRACT_BITS(return_values[RET_NUM_CONFIGS_OFFSET], 24, 31);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF REMAINING PIN CONFIGS: %d",
                                         num_remaining_configs);

                if (val_reserved_bits_check_is_zero(
                        VAL_EXTRACT_BITS(return_values[RET_NUM_CONFIGS_OFFSET], 8, 23))
                        != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                num_configs_returned =
                        VAL_EXTRACT_BITS(return_values[RET_NUM_CONFIGS_OFFSET], 0, 7);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF PIN CONFIGS RETURNED :  %d",
                                            num_configs_returned);

                pin_configs_array = &return_values[RET_CONFIGS_ARRAY_OFFSET];
                /* Print the pin id's associated with the group */
                for (i = 0; i < num_configs_returned; i++)
                {
                    entry_0 = pin_configs_array[i * 2];
                    val_print(VAL_PRINT_DEBUG, "\n       CONFIG TYPE: %d",
                                                VAL_EXTRACT_BITS(entry_0, 0, 7));
                    entry_1 = pin_configs_array[(i * 2) + 1];
                    val_print(VAL_PRINT_DEBUG, "\n       CONFIG VALUE: %d", entry_1);
                }
                skip_configs += num_configs_returned;
                val_print(VAL_PRINT_DEBUG, "\n       num_remaining_configs: %d",
                                              num_remaining_configs);
            } while (num_remaining_configs > 0);
        }
    }

    /* Query pin control config get for group identifiers */
    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUPS    : %d", num_groups);

        /* Get configs of each group identifier*/
        for (pc_id = 0; pc_id < num_groups; pc_id++)
        {
            num_remaining_configs = 0;
            num_configs_returned = 0;
            skip_configs = 0;

            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL GROUP ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Query config get");

            /* Get all the available configs for each group identifier */
            do
            {
                VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
                parameters[param_count++] = pc_id;
                attributes = (GET_ALL_CONFIGS << GET_ALL_CONFIGS_LOW) +
                             (GROUP_SELECTOR << SELECTOR_LOW);
                parameters[param_count++] = attributes;

                cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
                val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

                if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                val_print_return_values(return_value_count, return_values);

                num_remaining_configs =
                        VAL_EXTRACT_BITS(return_values[RET_NUM_CONFIGS_OFFSET], 24, 31);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF REMAINING GROUP CONFIGS: %d",
                                         num_remaining_configs);

                if (val_reserved_bits_check_is_zero(
                        VAL_EXTRACT_BITS(return_values[RET_NUM_CONFIGS_OFFSET], 8, 23))
                        != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                function_selected = return_values[RET_FUNCTION_SELECTED_OFFSET];
                if (function_selected != NO_FUNCTION_ENABLED) {
                    val_print(VAL_PRINT_DEBUG, "\n       GROUP ENABLES FUNCTION: %d",
                                                function_selected);
                }


                num_configs_returned =
                        VAL_EXTRACT_BITS(return_values[RET_NUM_CONFIGS_OFFSET], 0, 7);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF GROUP CONFIGS RETURNED :  %d",
                                            num_configs_returned);

                pin_configs_array = &return_values[RET_CONFIGS_ARRAY_OFFSET];
                /* Print the pin id's associated with the group */
                for (i = 0; i < num_configs_returned; i++)
                {
                    entry_0 = pin_configs_array[i * 2];
                    val_print(VAL_PRINT_DEBUG, "\n       CONFIG TYPE: %d",
                                                VAL_EXTRACT_BITS(entry_0, 0, 7));
                    entry_1 = pin_configs_array[(i * 2) + 1];
                    val_print(VAL_PRINT_DEBUG, "\n       CONFIG VALUE: %d", entry_0);
                }
                skip_configs += num_configs_returned;
                val_print(VAL_PRINT_DEBUG, "\n       num_remaining_configs: %d",
                                              num_remaining_configs);
            } while (num_remaining_configs > 0);
        }
    }

    return VAL_STATUS_PASS;
}

