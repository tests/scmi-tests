/** @file
 * Copyright (c) 2023, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 19)
#define TEST_DESC "pin ctrl get name check                      "
#define PARAMETER_SIZE 2
#define NO_IDENTIFIERS_FOUND 0x7

uint32_t pin_control_query_ext_name_check(void)
{
    int32_t  status;
    size_t   return_value_count;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t num_groups = 0, num_pins = 0, num_functions = 0;
    uint32_t pc_id = 0;
    uint8_t *extended_name;
    uint32_t ext_name_sup;
    uint32_t run_flag = 0;
    uint32_t num_flag = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Query pin control ext name for pin identifiers */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
        num_flag = 1 << PIN_SELECTOR;
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);

        /* Get ext name for each pin identifier*/
        for (pc_id = 0; pc_id < num_pins; pc_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Query ext name for pin ids");

            /* Check if extended name supported for each pin */
            ext_name_sup = val_pin_control_get_ext_name_info(pc_id, PC_PIN_NAME);
            if (ext_name_sup == 0)
            {
                val_print(VAL_PRINT_TEST, "\n    Pin doesn't support extended name");
                continue;
            }

            run_flag = 1;
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = PIN_SELECTOR;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                    PIN_CONTROL_NAME_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                    &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            val_print_return_values(return_value_count, return_values);
            extended_name = (uint8_t *) &return_values[PIN_CONTROL_EXT_NAME_OFFSET];
            val_print(VAL_PRINT_TEST, "\n       Pin name : %s", extended_name);
        }
    }

    /* Query pin control ext name for group identifiers */
    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
        num_flag = 1 << GROUP_SELECTOR;
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUPS    : %d", num_groups);

        /* Get ext name for each group identifier*/
        for (pc_id = 0; pc_id < num_groups; pc_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL GROUP ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [Check 2] Query ext name for group ids");

            /* Check if extended name supported for each group */
            ext_name_sup = val_pin_control_get_ext_name_info(pc_id, PC_GROUP_NAME);
            if (ext_name_sup == 0)
            {
                val_print(VAL_PRINT_TEST, "\n    Group doesn't support extended name");
                continue;
            }

            run_flag = 1;
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = GROUP_SELECTOR;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                    PIN_CONTROL_NAME_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                    &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            val_print_return_values(return_value_count, return_values);
            extended_name = (uint8_t *) &return_values[PIN_CONTROL_EXT_NAME_OFFSET];
            val_print(VAL_PRINT_TEST, "\n       Group name : %s", extended_name);
        }
    }

    /* Query pin control ext name for group identifiers */
    num_functions = val_pin_control_get_info(0, PC_NUM_FUNCTIONS);
    if (num_functions == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control function identifiers found");
        num_flag = 1 << FUNCTION_SELECTOR;
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM FUNCTIONS    : %d", num_functions);

        /* Get ext name for each function identifier*/
        for (pc_id = 0; pc_id < num_groups; pc_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL FUNCTION ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [Check 2] Query ext name for function ids");

            /* Check if extended name supported for each function */
            ext_name_sup = val_pin_control_get_ext_name_info(pc_id, PC_FUNCTION_NAME);
            if (ext_name_sup == 0)
            {
                val_print(VAL_PRINT_TEST, "\n    Function doesn't support extended name");
                continue;
            }

            run_flag = 1;
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = FUNCTION_SELECTOR;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                    PIN_CONTROL_NAME_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                    &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            val_print_return_values(return_value_count, return_values);
            extended_name = (uint8_t *) &return_values[PIN_CONTROL_EXT_NAME_OFFSET];
            val_print(VAL_PRINT_TEST, "\n       Function name : %s", extended_name);
        }
    }

    /* If no pins/groups/functions are present, skip the test */
    if (num_flag == NO_IDENTIFIERS_FOUND)
    {
        val_print(VAL_PRINT_ERR, "\n       No Pins/Groups/Funtions are present");
        return VAL_STATUS_SKIP;
    }
    else if (run_flag == 0)
    {
        val_print(VAL_PRINT_ERR, "\n       Pins/Groups/Funtions doesn't support ext name");
        return VAL_STATUS_SKIP;
    }
    return VAL_STATUS_PASS;
}

