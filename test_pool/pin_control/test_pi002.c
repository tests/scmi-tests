/** @file
 * Copyright (c) 2023, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 2)
#define TEST_DESC "Pin Control protocol attributes check        "

uint32_t pin_control_query_protocol_attributes(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t *parameters;
    uint32_t num_functions, num_groups, num_pins;
    uint32_t attributes_high, attributes_low;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Query pin control protocol attributes and check number of pins, groups and functions */
    val_print(VAL_PRINT_TEST, "\n     [Check 1] Query protocol attributes");

    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    parameters = NULL; /* No parameters for this command */
    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                     PIN_CONTROL_PROTOCOL_ATTRIBUTES, COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    val_print_return_values(return_value_count, return_values);

    attributes_low = return_values[ATTRIBUTES_LOW_OFFSET];

    num_pins = VAL_EXTRACT_BITS(attributes_low, 0, 15);
    if (val_compare("NUM PINS", num_pins, val_pin_control_get_expected_num_pins()))
        return VAL_STATUS_FAIL;
    if (num_pins > MAX_PIN_IDENTIFIERS) {
        val_print(VAL_PRINT_WARN, "\n     Num of pins exceed max num of supported pins");
        val_print(VAL_PRINT_WARN, "\n     Pins count is changed to max supported limit: %d",
                                  MAX_PIN_IDENTIFIERS);
        num_pins = MAX_PIN_IDENTIFIERS;
    }

    num_groups = VAL_EXTRACT_BITS(attributes_low, 16, 31);
     if (val_compare("NUM GROUPS", num_groups, val_pin_control_get_expected_num_groups()))
        return VAL_STATUS_FAIL;
    if (num_groups > MAX_GROUP_IDENTIFIERS) {
        val_print(VAL_PRINT_WARN, "\n     Num of groups exceed max num of supported groups");
        val_print(VAL_PRINT_WARN, "\n     Groups count is changed to max supported limit: %d",
                                  MAX_GROUP_IDENTIFIERS);
        num_groups = MAX_GROUP_IDENTIFIERS;
    }

    attributes_high = return_values[ATTRIBUTES_HIGH_OFFSET];
    if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(attributes_high,
                                        16, 31) != VAL_STATUS_PASS))
        return VAL_STATUS_FAIL;

    num_functions = VAL_EXTRACT_BITS(attributes_high, 0, 15);
    if (val_compare("NUM FUNCTIONS", num_functions, val_pin_control_get_expected_num_functions()))
        return VAL_STATUS_FAIL;
    if (num_functions > MAX_FUNCTIONS_IDENTIFIERS) {
        val_print(VAL_PRINT_WARN, "\n     Num of functions exceed max num of supported functions");
        val_print(VAL_PRINT_WARN, "\n     Functions count is changed to max supported limit: %d",
                                  MAX_FUNCTIONS_IDENTIFIERS);
        num_functions = MAX_FUNCTIONS_IDENTIFIERS;
    }

    /* Save the number of pins */
    val_pin_control_save_info(PC_NUM_PINS, 0, num_pins);

    /* Save the number of groups */
    val_pin_control_save_info(PC_NUM_GROUPS, 0, num_groups);

    /* Save the number of functions */
    val_pin_control_save_info(PC_NUM_FUNCTIONS, 0, num_functions);

    return VAL_STATUS_PASS;
}
