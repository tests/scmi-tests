/** @file
 * Copyright (c) 2023, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 20)
#define TEST_DESC "pin ctrl name get invalid case check         "
#define PARAMETER_SIZE 2

uint32_t pin_control_query_ext_name_get_invalid_case_check(void)
{
    int32_t  status;
    size_t   return_value_count;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t num_pins = 0;
    uint32_t pc_id;
    uint32_t ext_name_sup;
    uint32_t run_flags = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Query pin control ext name for invalid identifier */
    val_print(VAL_PRINT_TEST, "\n     [Check 1] Get extended name for invalid identifier");

    pc_id = val_pin_control_get_info(0, PC_NUM_PINS);
    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    /* pins 0 to num_pins - 1 are valid, using num_pins as invalid pin */
    parameters[param_count++] = pc_id;
    parameters[param_count++] = PIN_SELECTOR;

    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_NAME_GET, COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (val_compare_status(status, SCMI_NOT_FOUND) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    val_print_return_values(return_value_count, return_values);

    /* Query pin control ext name for invalid parameters */
    val_print(VAL_PRINT_TEST, "\n     [Check 2] Get extended name for invalid parameter");

    pc_id = val_pin_control_get_info(0, PC_NUM_PINS);
    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    parameters[param_count++] = pc_id - 1;
     /* Making reserved bit as 1 to make invalid flag */
    parameters[param_count++] = PIN_SELECTOR  + (1 << REQUEST_FLAGS_RESERVED_LOW);

    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_NAME_GET, COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (val_compare_status(status, SCMI_INVALID_PARAMETERS) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    val_print_return_values(return_value_count, return_values);

    /* Skip if no pin identifiers found */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
        return VAL_STATUS_SKIP;
    }

    val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);

    /* Check pin control name get for unsupported identifiers */
    for (pc_id = 0; pc_id < num_pins; pc_id++)
    {

        /* Check if extended domain name is supported for this pin */
        ext_name_sup = val_pin_control_get_ext_name_info(pc_id, PC_PIN_NAME);
        if (ext_name_sup == 1)
            continue;

        run_flags = 1;
        val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pc_id);
        val_print(VAL_PRINT_TEST, "\n     [Check 3] Get extended name for unsupp. domain");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = num_pins;
        parameters[param_count++] = PIN_SELECTOR;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_NAME_GET,
                COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                &return_value_count, return_values);

        if (val_compare_status(status, SCMI_NOT_FOUND) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;
        val_print_return_values(return_value_count, return_values);
    }

    if (run_flags == 0) {
        val_print(VAL_PRINT_ERR, "\n       All identifiers support extended name");
        return VAL_STATUS_SKIP;
    }

    return VAL_STATUS_PASS;
}
