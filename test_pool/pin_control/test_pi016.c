/** @file
 * Copyright (c) 2023, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 16)
#define TEST_DESC "Pin ctrl request control check               "
#define PARAMETER_SIZE 2

uint32_t pin_control_request_control_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t message_id;
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t num_groups = 0, num_pins = 0, pc_id = 0;
    uint32_t run_flags = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    val_print(VAL_PRINT_TEST, "\n     [Check 1] PIN CONTROL REQUEST support");

    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    message_id = PIN_CONTROL_REQUEST;
    param_count++;
    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_PROTOCOL_MESSAGE_ATTRIBUTES,
                                     COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, &message_id, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    if (status == SCMI_NOT_FOUND) {
        val_print(VAL_PRINT_ERR, "\n       PIN CONTROL REQUEST not supported        ");
        return VAL_STATUS_SKIP;
    }

    if (val_reserved_bits_check_is_zero(return_values[ATTRIBUTE_OFFSET]) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    val_print(VAL_PRINT_TEST, "\n     [Check 2] PIN CONTROL RELEASE support");

    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    message_id = PIN_CONTROL_RELEASE;
    param_count++;
    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_PROTOCOL_MESSAGE_ATTRIBUTES,
                                     COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, &message_id, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    if (status == SCMI_NOT_FOUND) {
        val_print(VAL_PRINT_ERR, "\n       PIN CONTROL RELEASE not supported        ");
        return VAL_STATUS_FAIL;
    }

    if (val_reserved_bits_check_is_zero(return_values[ATTRIBUTE_OFFSET]) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    val_print(VAL_PRINT_TEST, "\n     [Check 3] PIN CONTROL REQUEST check for pin ids");

    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);
        run_flags = 1;

        /* Check function select for each pin identifier*/
        for (pc_id = 0; pc_id < num_pins; pc_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pc_id);

            /* Requesting control of pin id for an agent*/
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = PIN_SELECTOR;
            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_REQUEST,
                    COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                    &return_value_count, return_values);

            if (val_compare_status(status, SCMI_DENIED) == VAL_STATUS_PASS)
            {
                val_print(VAL_PRINT_ERR, "\n       Agent do not have access to the pin");
                return VAL_STATUS_SKIP;
            }

            if (!((val_compare_status(status, SCMI_SUCCESS) == VAL_STATUS_PASS) ||
                (val_compare_status(status, SCMI_IN_USE) == VAL_STATUS_PASS)))
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Releasing control of pin id by an agent */
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = PIN_SELECTOR;
            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_RELEASE,
                    COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                    &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;
        }
    }

    val_print(VAL_PRINT_TEST, "\n     [Check 4] PIN CONTROL REQUEST check for group ids");

    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUPS    : %d", num_groups);
        run_flags = 1;

        /* Check function select for each group identifier*/
        for (pc_id = 0; pc_id < num_groups; pc_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL GROUP ID: %d", pc_id);

            /* Requesting control of group id for an agent*/
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = GROUP_SELECTOR;
            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_REQUEST,
                    COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                    &return_value_count, return_values);

            if (val_compare_status(status, SCMI_DENIED) == VAL_STATUS_PASS)
            {
                val_print(VAL_PRINT_ERR, "\n       Agent do not have access to the group");
                return VAL_STATUS_SKIP;
            }

            if (!((val_compare_status(status, SCMI_SUCCESS) == VAL_STATUS_PASS) ||
                (val_compare_status(status, SCMI_IN_USE) == VAL_STATUS_PASS)))
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Releasing control of group id by an agent */
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = GROUP_SELECTOR;
            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_RELEASE,
                    COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                    &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;
        }
    }

    if (run_flags == 0)
    {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin/group identifiers found");
        return VAL_STATUS_SKIP;
    }

    return VAL_STATUS_PASS;
}
