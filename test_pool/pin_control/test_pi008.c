/** @file
 * Copyright (c) 2023, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 8)
#define TEST_DESC "pin ctrl list associations invalid id check  "
#define PARAMETER_SIZE 3

uint32_t pin_control_query_list_associations_invalid_id_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t pc_id = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Check pin control list associations for invalid group identifiers */
    pc_id = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (pc_id == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
    } else {
        val_print(VAL_PRINT_TEST, "\n     [Check 1] Query list assoc with invalid group id: %d",
                                    pc_id);

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = pc_id;
        parameters[param_count++] = GROUP_SELECTOR;
        parameters[param_count++] = 0;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                PIN_CONTROL_LIST_ASSOCIATIONS, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                &return_value_count, return_values);

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_status(status, SCMI_NOT_FOUND) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;
    }

    return VAL_STATUS_PASS;
}

