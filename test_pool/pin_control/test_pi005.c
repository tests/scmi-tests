/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 5)
#define TEST_DESC "Pin Control attributes check                 "
#define PARAMETER_SIZE 2

uint32_t pin_control_attributes_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t num_pins = 0, identifier = 0;
    uint32_t num_groups = 0, num_functions = 0;
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t attributes = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Discover pin control attributes for all pins */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Pins found                       ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM PINS     : %d", num_pins);

    for (identifier = 0; identifier < num_pins; identifier++)
    {
        val_print(VAL_PRINT_TEST, "\n     PIN ID: %d", identifier);
        val_print(VAL_PRINT_TEST, "\n     [Check 1] Query Pin Control attribute");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = identifier;
        parameters[param_count++] = PIN_SELECTOR;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_ATTRIBUTES,
                                         COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        attributes = return_values[ATTRIBUTE_OFFSET];
        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(attributes, 18, 30) !=
                                            VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        /* If flag parameter is set to PIN, then this value should be 1 */
        if (VAL_EXTRACT_BITS(attributes, 0, 15) != 1)
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_INFO, "\n    Extended Name Support:  %d",
                  VAL_EXTRACT_BITS(attributes, 31, 31));
        val_pin_control_save_ext_name_info(PC_PIN_NAME, identifier,
                  VAL_EXTRACT_BITS(attributes, 31, 31));

        val_print(VAL_PRINT_INFO, "\n    PIN_NAME: %s                         ",
                                   (uint8_t *) &return_values[NAME_OFFSET]);
    }

    /* Discover pin control attributes for all groups */
    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Groups found                       ");
    } else {
        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUPS     : %d", num_groups);

        for (identifier = 0; identifier < num_groups; identifier++)
        {
            val_print(VAL_PRINT_TEST, "\n     GROUP ID: %d", identifier);
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Query Pin Control attribute");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = identifier;
            parameters[param_count++] = GROUP_SELECTOR;
            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_ATTRIBUTES,
                                         COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            val_print_return_values(return_value_count, return_values);

            attributes = return_values[ATTRIBUTE_OFFSET];
            if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(attributes, 18, 30) !=
                                            VAL_STATUS_PASS))
                return VAL_STATUS_FAIL;

           if (val_compare("NUM OF PINS IN GROUP",
                        VAL_EXTRACT_BITS(attributes, 0, 15),
                        val_pin_control_get_expected_pins_in_group(identifier)))
               return VAL_STATUS_FAIL;

            val_print(VAL_PRINT_TEST, "\n    NUM OF PINS IN THE GROUP:  %d",
                      VAL_EXTRACT_BITS(attributes, 0, 15));

            val_pin_control_save_info(PC_NUM_PINS_IN_GROUP, identifier,
                      VAL_EXTRACT_BITS(attributes, 0, 15));

            val_print(VAL_PRINT_INFO, "\n    Extended Name Support:  %d",
                      VAL_EXTRACT_BITS(attributes, 31, 31));
            val_pin_control_save_ext_name_info(PC_GROUP_NAME, identifier,
                      VAL_EXTRACT_BITS(attributes, 31, 31));

            val_print(VAL_PRINT_INFO, "\n    GROUP_NAME: %s                         ",
                                       (uint8_t *) &return_values[NAME_OFFSET]);
        }
    }

    /* Discover pin control attributes for all functions */
    num_functions = val_pin_control_get_info(0, PC_NUM_FUNCTIONS);
    if (num_functions == 0) {
        val_print(VAL_PRINT_ERR, "\n       No FUNCTIONs found                       ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM FUNCTIONS     : %d", num_functions);

    for (identifier = 0; identifier < num_functions; identifier++)
    {
        val_print(VAL_PRINT_TEST, "\n     FUNCTION ID: %d", identifier);
        val_print(VAL_PRINT_TEST, "\n     [Check 1] Query Pin Control attribute");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = identifier;
        parameters[param_count++] = FUNCTION_SELECTOR;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_ATTRIBUTES,
                                         COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        attributes = return_values[ATTRIBUTE_OFFSET];
        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(attributes, 18, 30) !=
                                            VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_TEST, "\n    FUNCTION SUPPORTS GPIO:  %d",
                        VAL_EXTRACT_BITS(attributes, 17, 17));
        val_pin_control_save_info(PC_FUNCTION_GPIO_SUPPORT, identifier,
                        VAL_EXTRACT_BITS(attributes, 17, 17));

        val_print(VAL_PRINT_TEST, "\n    FUNCTION IS SUPPORTED BY GROUP:  %d",
                        VAL_EXTRACT_BITS(attributes, 16, 16));
        val_pin_control_save_info(PC_FUNCTION_GROUP_SUPPORT, identifier,
                        VAL_EXTRACT_BITS(attributes, 16, 16));

        if (val_compare("NUM OF GROUPS IN FUNCTION",
                        VAL_EXTRACT_BITS(attributes, 0, 15),
                        val_pin_control_get_expected_groups_in_func(identifier)))
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_TEST, "\n    NUM OF GROUPS IN THE FUNCTION:  %d",
                  VAL_EXTRACT_BITS(attributes, 0, 15));

        val_pin_control_save_info(PC_NUM_GROUPS_IN_FUNCTION, identifier,
                  VAL_EXTRACT_BITS(attributes, 0, 15));

        val_print(VAL_PRINT_INFO, "\n    Extended Name Support:  %d",
                  VAL_EXTRACT_BITS(attributes, 31, 31));
        val_pin_control_save_ext_name_info(PC_FUNCTION_NAME, identifier,
                  VAL_EXTRACT_BITS(attributes, 31, 31));

        val_print(VAL_PRINT_INFO, "\n    FUNCTION_NAME: %s                         ",
                                   (uint8_t *) &return_values[NAME_OFFSET]);
    }

    return VAL_STATUS_PASS;
}
