/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 11)
#define TEST_DESC "pin ctrl config set check                    "
#define PARAMETER_SIZE 5

#define CONFIG_ENABLE  1
#define CONFIG_DISABLE 0

/************ TEST ALGO ******************
 * Get current settings config values
 * Choose a different values for  config type
 * Set new settings configure values for config type
 * Check new settings configure values has been set.
 * Restore the default settings config values
*****************************************/

uint32_t pin_control_query_settings_configure_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t attributes = 0;
    uint32_t num_groups = 0, num_pins = 0;
    uint32_t pc_id = 0;
    uint32_t *pin_configs_array;
    uint32_t config_type, default_config_value;
    uint32_t new_config_value, function_selected = 0;
    uint32_t entry_0, entry_1;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Query pin control settings configure for pin identifiers */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);

        /* Get configs of each pin identifier*/
        for (pc_id = 0; pc_id < num_pins; pc_id++)
        {
            /* STEP 1:Reading pin configuration */
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [STEP 1] Get Pin settings config ");

            /* Get all the available configs for each pin identifier */

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            attributes = ((PIN_SELECTOR << SELECTOR_LOW) + DEFAULT_CONFIG_TYPE);
            parameters[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            function_selected = return_values[RET_FUNCTION_SELECTED_OFFSET];
            if (function_selected != NO_FUNCTION_ENABLED) {
                val_print(VAL_PRINT_DEBUG, "\n       PIN ENABLES FUNCTION: %d",
                                            function_selected);
            }

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            pin_configs_array = &return_values[RET_CONFIGS_ARRAY_OFFSET];
            /* Print the pin id's associated with the group */
            config_type = VAL_EXTRACT_BITS(pin_configs_array[0], 0, 7);
            val_print(VAL_PRINT_DEBUG, "\n       DEFAULT CONFIG TYPE : %d", config_type);

            default_config_value = pin_configs_array[1];
            val_print(VAL_PRINT_DEBUG, "\n       DEFAULT CONFIG VALUE: %d",
                                                                   default_config_value);

            /* STEP:2 SET Different Config Values for the Config Type */
            val_print(VAL_PRINT_TEST, "\n    [Step 2] Choose new config value for pin");

            if (default_config_value == CONFIG_DISABLE)
                new_config_value = CONFIG_ENABLE;
             else
                new_config_value = CONFIG_DISABLE;

            /*STEP:3 Setting new config value for pin*/
            val_print(VAL_PRINT_TEST, "\n    [Step 3] Set new config value");
            val_print(VAL_PRINT_DEBUG, "\n       CONFIG TYPE     : %d",
                                            VAL_EXTRACT_BITS(config_type, 0, 7));
            val_print(VAL_PRINT_DEBUG, "\n       NEW CONFIG VALUE: %d", new_config_value);

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = function_selected;
            attributes = (1 << SET_CONFIGS_LOW) + PIN_SELECTOR;
            parameters[param_count++] = attributes;
            parameters[param_count++] = config_type;
            parameters[param_count++] = new_config_value;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_SKIP;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* STEP 4 : Verify new config_value has been set */
            val_print(VAL_PRINT_TEST, "\n    [Step 4] Verify new config value ");
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            attributes =  ((PIN_SELECTOR << SELECTOR_LOW) + DEFAULT_CONFIG_TYPE);
            parameters[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            function_selected = return_values[RET_FUNCTION_SELECTED_OFFSET];
            if (function_selected != NO_FUNCTION_ENABLED) {
                val_print(VAL_PRINT_DEBUG, "\n       PIN ENABLES FUNCTION: %d",
                                            function_selected);
            }

            pin_configs_array = &return_values[RET_CONFIGS_ARRAY_OFFSET];
            val_print_return_values(return_value_count, return_values);

            entry_0 = VAL_EXTRACT_BITS(pin_configs_array[0], 0, 7);
            if (val_compare("Config Type        ", entry_0, config_type))
                return VAL_STATUS_FAIL;

            entry_1 = pin_configs_array[1];
            if (val_compare("New Config Value   ", entry_1, new_config_value))
                return VAL_STATUS_FAIL;

            /* STEP 5 : Set default config value back */
            val_print(VAL_PRINT_TEST, "\n    [Step 5] Set default config value");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = function_selected;
            attributes = (1 << SET_CONFIGS_LOW) + PIN_SELECTOR;
            parameters[param_count++] = attributes;
            parameters[param_count++] = config_type;
            parameters[param_count++] = default_config_value;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;
         }
    }

    /* Query pin control config set for group identifiers */
    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
    } else {
        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUPS    : %d", num_groups);

        /* Get configs of each group identifier*/
        for (pc_id = 0; pc_id < num_groups; pc_id++)
        {
            /* STEP 1:Reading pin configuration */
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL GROUP ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [STEP 1] Get Group config ");

            /* Get all the available configs for each group identifier */

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            attributes = ((GROUP_SELECTOR << SELECTOR_LOW) + DEFAULT_CONFIG_TYPE);
            parameters[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            function_selected = return_values[RET_FUNCTION_SELECTED_OFFSET];
            if (function_selected != NO_FUNCTION_ENABLED) {
                val_print(VAL_PRINT_DEBUG, "\n       PIN ENABLES FUNCTION: %d",
                                            function_selected);
            }

            pin_configs_array = &return_values[RET_CONFIGS_ARRAY_OFFSET];
            /* Print the pin id's associated with the group */
            config_type = VAL_EXTRACT_BITS(pin_configs_array[0], 0, 7);
            val_print(VAL_PRINT_DEBUG, "\n       DEFAULT CONFIG TYPE : %d", config_type);

            default_config_value = pin_configs_array[1];
            val_print(VAL_PRINT_DEBUG, "\n       DEFAULT CONFIG VALUE: %d",
                                                                   default_config_value);

            /* STEP:2 SET Different Config Values for the Config Type */
            val_print(VAL_PRINT_TEST, "\n    [Step 2] Choose new config value for group");
            if (default_config_value == CONFIG_DISABLE)
                 new_config_value = CONFIG_ENABLE;
            else
                 new_config_value = CONFIG_DISABLE;

            /*STEP:3 Setting new config value for group*/
            val_print(VAL_PRINT_TEST, "\n    [Step 3] Set new config value");
            val_print(VAL_PRINT_DEBUG, "\n       CONFIG TYPE     : %d",
                                             VAL_EXTRACT_BITS(config_type, 0, 7));
            val_print(VAL_PRINT_DEBUG, "\n       NEW CONFIG VALUE: %d", new_config_value);

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = function_selected;
            attributes = (1 << SET_CONFIGS_LOW) + GROUP_SELECTOR;
            parameters[param_count++] = attributes;
            parameters[param_count++] = config_type;
            parameters[param_count++] = new_config_value;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_SKIP;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* STEP 4 : Verify new config_value has been set */
            val_print(VAL_PRINT_TEST, "\n    [Step 4] Verify new config value ");
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            attributes =  ((GROUP_SELECTOR << SELECTOR_LOW) + DEFAULT_CONFIG_TYPE);
            parameters[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            function_selected = return_values[RET_FUNCTION_SELECTED_OFFSET];
            if (function_selected != NO_FUNCTION_ENABLED) {
                val_print(VAL_PRINT_DEBUG, "\n       PIN ENABLES FUNCTION: %d",
                                            function_selected);
            }

            pin_configs_array = &return_values[RET_CONFIGS_ARRAY_OFFSET];
            val_print_return_values(return_value_count, return_values);

            entry_0 = VAL_EXTRACT_BITS(pin_configs_array[0], 0, 7);
            if (val_compare("Config Type        ", entry_0, config_type))
                return VAL_STATUS_FAIL;

            entry_1 = pin_configs_array[1];
            if (val_compare("New Config Value   ", entry_1, new_config_value))
                return VAL_STATUS_FAIL;

            /* STEP 5 : Set default config value back */
            val_print(VAL_PRINT_TEST, "\n    [Step 5] Set default config value");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = pc_id;
            parameters[param_count++] = function_selected;
            attributes = (1 << SET_CONFIGS_LOW) + GROUP_SELECTOR;
            parameters[param_count++] = attributes;
            parameters[param_count++] = config_type;
            parameters[param_count++] = default_config_value;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                             PIN_CONTROL_SETTINGS_CONFIGURE, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;
        }
    }

    return VAL_STATUS_PASS;
}
