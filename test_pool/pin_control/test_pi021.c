/** @file
 * Copyright (c) 2023, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 21)
#define TEST_DESC "pin ctrl set permissions check               "

#define PARAMETER_SIZE 3

uint32_t pin_control_set_permissions_check(void)
{
    int32_t  status;
    size_t   return_value_count;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameter[PARAMETER_SIZE];
    uint32_t num_pins, num_groups;
    uint32_t attributes = 0;
    uint32_t flags, agent_id;
    uint32_t pin_id, group_id, message_id;
    uint32_t run_test = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    agent_id = val_base_get_info(BASE_TEST_AGENT_ID);

    /* If agent is not trusted , skip the test */
    if (val_check_trusted_agent(agent_id) == 0) {
        val_print(VAL_PRINT_ERR, "\n       Calling agent is untrusted agent            ");
        return VAL_STATUS_SKIP;
    }

    /* If PINCTRL_SET_PERMISSIONS not supported, skip the test */
    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    message_id = PIN_CONTROL_SET_PERMISSIONS;
    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_PROTOCOL_MESSAGE_ATTRIBUTES,
                                      COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, &message_id, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (status != SCMI_SUCCESS) {
        val_print(VAL_PRINT_ERR, "\n       PINCTRL SET PERMISSIONS not supported   ");
        return VAL_STATUS_SKIP;
    }

    /* Check the number of pins */
    num_pins = val_pin_control_get_info(0, PC_NUM_PINS);
    if (num_pins == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control pin identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM PINS    : %d", num_pins);

        /* Deny access to pins for given agent */
        for (pin_id = 0; pin_id < num_pins; pin_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL PIN ID: %d", pin_id);
            status = val_pinctrl_check_access(agent_id, pin_id, PIN_SELECTOR);

            if (status != SCMI_SUCCESS) {
                val_print(VAL_PRINT_DEBUG, "\n       Agent don't have access   ");
                continue;
             }

            run_test = 1;
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Deny access to valid pin  %d", pin_id);

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = agent_id;
            parameter[param_count++] = pin_id;
            flags = (PINCTRL_ACCESS_DENY << PINCTRL_ACCESS_OFFSET) + PIN_SELECTOR;
            parameter[param_count++] = flags; /* Deny access */

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_SET_PERMISSIONS,
                                         COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Agent accessing denied pin should fail */
            val_print(VAL_PRINT_TEST, "\n     [Check 2] Access denied pin");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = pin_id;
            attributes = (GET_ALL_CONFIGS << GET_ALL_CONFIGS_LOW) +
                                         (PIN_SELECTOR << SELECTOR_LOW);
            parameter[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

            if (status == SCMI_SUCCESS) {
                val_print(VAL_PRINT_ERR, "\n    CHECK DENIED PIN ACCESS: FAILED");
                val_print(VAL_PRINT_INFO, "\n    Able to access denied pin");
                return VAL_STATUS_FAIL;
            }

            if (val_compare_status(status, SCMI_DENIED) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Restore agent access to pin */
            val_print(VAL_PRINT_TEST, "\n     [Check 3] Restore denied pin access");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = agent_id;
            parameter[param_count++] = pin_id;
            flags = (PINCTRL_ACCESS_ALLOW << PINCTRL_ACCESS_OFFSET) + PIN_SELECTOR;
            parameter[param_count++] = flags; /* Allow access */

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_SET_PERMISSIONS,
                                         COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Agent should be able to access pin after pin permission restored */
            val_print(VAL_PRINT_TEST, "\n     [Check 4] Access pin after permissions restored");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = pin_id;
            attributes = (GET_ALL_CONFIGS << GET_ALL_CONFIGS_LOW) +
                                              (PIN_SELECTOR << SELECTOR_LOW);
            parameter[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;
        }
      }

    /* Check the number of groups */
    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUP    : %d", num_groups);

        /* Deny access to group for given agent */
        for (group_id = 0; group_id < num_groups; group_id++)
        {
            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL GROUP ID: %d", group_id);
            status = val_pinctrl_check_access(agent_id, group_id, GROUP_SELECTOR);

            if (status != SCMI_SUCCESS) {
                val_print(VAL_PRINT_DEBUG, "\n       Agent don't have access   ");
                continue;
            }

            run_test = 1;
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Deny access to valid group  %d", group_id);

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = agent_id;
            parameter[param_count++] = group_id;
            flags = (PINCTRL_ACCESS_DENY << PINCTRL_ACCESS_OFFSET) + GROUP_SELECTOR;
            parameter[param_count++] = flags; /* Deny access */

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_SET_PERMISSIONS,
                                         COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Agent accessing denied group should fail */
            val_print(VAL_PRINT_TEST, "\n     [Check 2] Access denied group");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = group_id;
            attributes = (GET_ALL_CONFIGS << GET_ALL_CONFIGS_LOW) +
                                           (GROUP_SELECTOR << SELECTOR_LOW);
            parameter[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

            if (status == SCMI_SUCCESS) {
                val_print(VAL_PRINT_ERR, "\n    CHECK DENIED GROUP ACCESS: FAILED");
                val_print(VAL_PRINT_INFO, "\n    Able to access denied group");
                return VAL_STATUS_FAIL;
            }

            if (val_compare_status(status, SCMI_DENIED) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Restore agent access to group */
            val_print(VAL_PRINT_TEST, "\n     [Check 3] Restore denied group access");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = agent_id;
            parameter[param_count++] = group_id;
            flags = (PINCTRL_ACCESS_ALLOW << PINCTRL_ACCESS_OFFSET) + GROUP_SELECTOR;
            parameter[param_count++] = flags; /* Allow access */

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL, PIN_CONTROL_SET_PERMISSIONS,
                                          COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            /* Agent should be able to access group after group permission restored */
            val_print(VAL_PRINT_TEST, "\n     [Check 4] Access group after permissions restored");

            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameter[param_count++] = group_id;
            attributes = (GET_ALL_CONFIGS << GET_ALL_CONFIGS_LOW) +
                                              (GROUP_SELECTOR << SELECTOR_LOW);
            parameter[param_count++] = attributes;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_SETTINGS_GET, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameter, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;
        }
      }
    if (run_test)
        return VAL_STATUS_PASS;
    else
       return VAL_STATUS_SKIP;
}
