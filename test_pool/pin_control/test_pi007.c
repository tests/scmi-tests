/** @file
 * Copyright (c) 2023, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include"val_interface.h"
#include "val_pin_control.h"

#define TEST_NUM  (SCMI_PIN_CONTROL_TEST_NUM_BASE + 7)
#define TEST_DESC "pin ctrl list associations check             "
#define PARAMETER_SIZE 3

uint32_t pin_control_query_list_associations(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t num_groups = 0, num_functions = 0;
    uint32_t pc_id = 0;
    uint32_t index, num_remaining_ids, num_ids_returned;
    uint32_t i, selector_id, *pin_ids_array;
    uint32_t pins_count, groups_count;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Check pin control list associations for valid group identifiers */
    num_groups = val_pin_control_get_info(0, PC_NUM_GROUPS);
    if (num_groups == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control group identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM GROUPS    : %d", num_groups);

        /* Check num of pins for each group identifier*/
        for (pc_id = 0; pc_id < num_groups; pc_id++)
        {
            index = 0;
            num_remaining_ids = 0;
            num_ids_returned = 0;
            pins_count = 0;

            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL GROUP ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Query list associations");

            /* Get all the available pin id's for each group identifier*/
            do
            {
                VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
                parameters[param_count++] = pc_id;
                parameters[param_count++] = GROUP_SELECTOR;
                index += num_ids_returned;
                parameters[param_count++] = index;

                cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_LIST_ASSOCIATIONS, COMMAND_MSG);
                val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

                if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                val_print_return_values(return_value_count, return_values);

                num_remaining_ids = VAL_EXTRACT_BITS(return_values[RET_FLAGS_OFFSET], 16, 31);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF REMAINING PIN IDS: %d",
                                         num_remaining_ids);

                if (val_reserved_bits_check_is_zero(
                    VAL_EXTRACT_BITS(return_values[RET_FLAGS_OFFSET], 12, 15)) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                num_ids_returned = VAL_EXTRACT_BITS(return_values[RET_FLAGS_OFFSET], 0, 11);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF PIN IDS RETURNED :  %d",
                                            num_ids_returned);

                pin_ids_array = &return_values[PIN_CONTROL_IDS_ARRAY_OFFSET];
                /* Print the pin id's associated with the group */
                for (i = 0; i < num_ids_returned; i++)
                {
                    selector_id = pin_ids_array[i];
                    val_print(VAL_PRINT_DEBUG, "\n       PIN ID[%d]    : %d",
                                                i, selector_id);
                }
                pins_count += num_ids_returned;
                val_print(VAL_PRINT_DEBUG, "\n       num_remaining_ids: %d", num_remaining_ids);
            } while (num_remaining_ids > 0);

            val_pin_control_save_info(PC_NUM_PINS_IN_GROUP, pc_id, pins_count);
        }
    }

    /* Check pin control list associations for valid function identifiers */
    num_functions = val_pin_control_get_info(0, PC_NUM_FUNCTIONS);
    if (num_functions == 0) {
        val_print(VAL_PRINT_ERR, "\n       No pin control function identifiers found");
    } else {

        val_print(VAL_PRINT_DEBUG, "\n       NUM FUNCTIONS    : %d", num_functions);

        /* Check num of pins for each function identifier*/
        for (pc_id = 0; pc_id < num_functions; pc_id++)
        {
            index = 0;
            num_remaining_ids = 0;
            num_ids_returned = 0;
            groups_count = 0;

            val_print(VAL_PRINT_TEST, "\n    PIN CONTROL FUNCTION ID: %d", pc_id);
            val_print(VAL_PRINT_TEST, "\n     [Check 1] Query list associations");

            /* Get all the available pin id's for each function identifier*/
            do
            {
                VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
                parameters[param_count++] = pc_id;
                parameters[param_count++] = FUNCTION_SELECTOR;
                index += num_ids_returned;
                parameters[param_count++] = index;

                cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_PIN_CONTROL,
                                                 PIN_CONTROL_LIST_ASSOCIATIONS, COMMAND_MSG);
                val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                             &return_value_count, return_values);

                if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                val_print_return_values(return_value_count, return_values);

                num_remaining_ids = VAL_EXTRACT_BITS(return_values[RET_FLAGS_OFFSET], 16, 31);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF REMAINING GROUP IDS: %d",
                                         num_remaining_ids);

                if (val_reserved_bits_check_is_zero(
                    VAL_EXTRACT_BITS(return_values[RET_FLAGS_OFFSET], 12, 15)) != VAL_STATUS_PASS)
                    return VAL_STATUS_FAIL;

                num_ids_returned = VAL_EXTRACT_BITS(return_values[RET_FLAGS_OFFSET], 0, 11);
                val_print(VAL_PRINT_DEBUG, "\n       NUM OF GROUP IDS RETURNED :  %d",
                                            num_ids_returned);

                pin_ids_array = &return_values[PIN_CONTROL_IDS_ARRAY_OFFSET];
                /* Print the group id's associated with the function */
                for (i = 0; i < num_ids_returned; i++)
                {
                    selector_id = pin_ids_array[i];
                    val_print(VAL_PRINT_DEBUG, "\n       GROUP ID[%d]    : %d",
                                                i, selector_id);
                }
                groups_count += num_ids_returned;
            } while (num_remaining_ids > 0);

            val_pin_control_save_info(PC_NUM_GROUPS_IN_FUNCTION, pc_id, groups_count);
        }
    }
    return VAL_STATUS_PASS;
}

