/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_sensor.h"

#define TEST_NUM  (SCMI_SENSOR_TEST_NUM_BASE + 27)
#define TEST_DESC "Sensor get ext name for invalid ID           "

uint32_t sensor_query_domain_ext_name_invalid_domain(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t sensor_id, num_sensors;
    uint32_t extended_name_supp;
    uint32_t run_flag = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Get Extended name for invalid sensor */
    sensor_id = val_sensor_get_info(NUM_SENSORS) + 1;
    val_print(VAL_PRINT_TEST, "\n     [Check 1] Get extended name for invalid domain");

    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    param_count++;
    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_SENSOR, SENSOR_NAME_GET, COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, &sensor_id, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (val_compare_status(status, SCMI_NOT_FOUND) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
        return VAL_STATUS_FAIL;

    val_print_return_values(return_value_count, return_values);

    /* Skip if no sensors found*/
    num_sensors = val_sensor_get_info(NUM_SENSORS);
    if (num_sensors == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Sensor found                             ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM SENSORS     : %d", num_sensors);

    /* Check ext sensor name for unsupported sensor */
    for (sensor_id = 0; sensor_id < num_sensors; sensor_id++)
    {
        /* Check if extended domain name is supported for this sensor */
        extended_name_supp = val_sensor_ext_get_desc_info(SENSOR_EXTENDED_NAME_SUPPORT, sensor_id);

        if (extended_name_supp == SN_EXTENDED_DOMAIN_NAME)
            continue;

        run_flag = 1;
        val_print(VAL_PRINT_TEST, "\n       SENSOR ID : %d", sensor_id);
        val_print(VAL_PRINT_TEST, "\n     [Check 2] Get extended name for unsupp. domain");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        param_count++;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_SENSOR, SENSOR_NAME_GET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, &sensor_id, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_NOT_FOUND) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);
    }

    if (run_flag == 0) {
        val_print(VAL_PRINT_ERR, "\n       All Domains support extended name");
        return VAL_STATUS_SKIP;
    }

    return VAL_STATUS_PASS;
}
