/** @file
 * Copyright (c) 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_sensor.h"

#define TEST_NUM  (SCMI_SENSOR_TEST_NUM_BASE + 28)
#define TEST_DESC "Sensor query negotiate protocol version      "

extern uint32_t sensor_supported_protocols[SUPPORTED_SENSOR_PROTOCOLS_COUNT];

uint32_t sensor_query_negotiate_protocol_version(uint32_t *version)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t message_id;
    uint32_t parameters;
    uint32_t index;
    uint32_t num_supported_protocols;
    int32_t  negotiate_flag = VAL_STATUS_FAIL;
    uint32_t curr_version = *version;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* If NEGOTIATE_PROTOCOL_VERSION cmds not supported, skip the test */
    VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
    message_id = SENSOR_NEGOTIATE_PROTOCOL_VERSION;
    cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_SENSOR,
                                     SENSOR_PROTOCOL_MESSAGE_ATTRIBUTES, COMMAND_MSG);
    val_send_message(cmd_msg_hdr, param_count, &message_id, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

    if (status == SCMI_NOT_FOUND) {
        val_print(VAL_PRINT_ERR, "\n       SENSOR_NEGOTIATE_PROTOCOL_VERSION not supported  ");
        return VAL_STATUS_SKIP;
    }

    /* Query the negotiate protocol version that agent intends to use */
    val_print(VAL_PRINT_TEST, "\n     [Check 1] Query negotiate protocol version");

    num_supported_protocols =
            sizeof(sensor_supported_protocols)/sizeof(sensor_supported_protocols[0]);
    /* Get index from sensor_supported_protocols with version returned by protocol version */
    for (index = num_supported_protocols; index > 0; index--) {
        if (sensor_supported_protocols[index - 1] < *version) {
            val_print(VAL_PRINT_DEBUG, "\n       Negotiating with version 0x%08X");
            break;
        }
    }

    /* Corner case: If the version returned from the protocol version command is not supported */
    if (index == 0) {
        val_print(VAL_PRINT_ERR, "\n       Not able to get version to negotiate");
        return VAL_STATUS_SKIP;
    }

    /* Start negotiating with supported protocols */
    while (index > 0) {
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters = sensor_supported_protocols[--index];
        param_count = 1;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_SENSOR,
                                         SENSOR_NEGOTIATE_PROTOCOL_VERSION, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, &parameters, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_status(status, SCMI_SUCCESS) == VAL_STATUS_PASS) {
            negotiate_flag = VAL_STATUS_PASS;
            break;
        }
    }

    if (negotiate_flag == VAL_STATUS_PASS) {
        *version = parameters;
        val_print(VAL_PRINT_ERR, "\n       NEGOTIATED VERSION        : 0x%08x", *version);
        /* If negotiated version is lower than supported, check NOT_SUPPORTED is returned */
        if (*version < 0x00030001) {
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters = curr_version;
            param_count = 1;
            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_SENSOR,
                                         SENSOR_NEGOTIATE_PROTOCOL_VERSION, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, &parameters, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_status(status, SCMI_NOT_SUPPORTED) == VAL_STATUS_PASS) {
                return VAL_STATUS_PASS;
            }
        }
    } else {
        val_print(VAL_PRINT_ERR, "\n       Failed to negotiate version", 0);
        return VAL_STATUS_SKIP;
    }

    return VAL_STATUS_PASS;
}
