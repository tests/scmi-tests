/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_clock.h"

#define TEST_NUM  (SCMI_CLOCK_TEST_NUM_BASE + 27)
#define TEST_DESC "Clock parent name support set check        "

#define MAX_PARAMETER_SIZE 2


/************ TEST ALGO ******************
 * Check if clock has any restrictions before changing clock parent
 * Get current parent Id.
 * Choose a new parent Id.
 * Set new parent ID for current clock.
 * Check new parent Id has been set.
 * Restore the default parent Id.
*****************************************/

uint32_t clock_parent_name_support_set_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[MAX_PARAMETER_SIZE];
    uint32_t clock_id, num_clocks;
    uint32_t parent_id;
    uint32_t default_parent_id, new_parent_id;
    uint32_t parent_id_supp, attributes;
    uint32_t flag = 0, clock_state_control = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Skip if no domains found*/
    num_clocks = val_clock_get_info(NUM_CLOCKS, 0);
    if (num_clocks == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Clocks found                      ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM Clocks    : %d", num_clocks);

    /* Check Clock for valid parent ID */
    for (clock_id = 0; clock_id < num_clocks; clock_id++)
    {
        val_print(VAL_PRINT_TEST, "\n       CLOCK ID : %d", clock_id);
        /* Check if parent  name is supported for this clock */
        parent_id_supp = val_get_clock_attribute(clock_id, CLOCK_PARENT_IDENTIFIER_SUPPORT);
        if (parent_id_supp != CLOCK_PARENT_SUPP)
        {
            val_print(VAL_PRINT_ERR, "\n       Clock doesn't support Parent Identifier");
            continue;
        }

        /* Check if current clock id has any restrictions to change clock parent */
        attributes = val_clock_get_info(CLOCK_ATTRIBUTE, clock_id);
        if (val_get_clock_attributes_flag(attributes, CLOCK_RESTRICTED)) {
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            param_count++;
            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_GET_PERMISSIONS, COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, &clock_id, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;
            clock_state_control = VAL_EXTRACT_BITS(return_values[CLOCK_PERMISSIONS_OFFSET], 30, 30);
        }

        if (clock_state_control)
        {
            val_print(VAL_PRINT_ERR, "\n       Clock parent set is restricted for this agent");
            continue;
        }

        flag = 1;
        /*Step:1 Get the parent Id of the current clock */
        val_print(VAL_PRINT_TEST, "\n  [STEP 1] Get Parent ID");
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = clock_id;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_PARENT_GET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);
        parent_id = return_values[CLOCK_PARENT_NAME_OFFSET];
        default_parent_id = parent_id;
        val_print(VAL_PRINT_DEBUG, "\n      Default Clock parent Id : %d", default_parent_id);

        new_parent_id = val_clock_get_possible_parents(clock_id, CLOCK_PARENT_START_OFFSET);
        if (new_parent_id == parent_id && (val_clock_get_num_possible_parents(clock_id) > 1))
            new_parent_id = val_clock_get_possible_parents(clock_id,
                                                               CLOCK_PARENT_START_OFFSET + 1);

        /*Step:2 Set new parent ID for current clock*/
        val_print(VAL_PRINT_TEST, "\n  [STEP 2] Set new Parent ID");
        val_print(VAL_PRINT_DEBUG, "\n      New Clock parent Id : %d", new_parent_id);

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = clock_id;
        parameters[param_count++] = new_parent_id;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_PARENT_SET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        /*Step:3 Verify new Parent Id has been Set*/
        val_print(VAL_PRINT_TEST, "\n  [STEP 3] Verify New Parent ID");
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = clock_id;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_PARENT_GET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);
        parent_id = return_values[CLOCK_PARENT_NAME_OFFSET];
        if (val_compare("New Parent Id       ", parent_id, new_parent_id))
            return VAL_STATUS_FAIL;

        /* STEP:4 : Set default parent Id back */
        val_print(VAL_PRINT_TEST, "\n  [STEP 4] Set default Parent ID");
        val_print(VAL_PRINT_DEBUG, "\n      New Clock parent Id : %d", new_parent_id);

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = clock_id;
        parameters[param_count++] = default_parent_id;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_PARENT_SET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;
     }

     if (flag == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Clock support parent name");
        return VAL_STATUS_SKIP;
    }

    return VAL_STATUS_PASS;
}









