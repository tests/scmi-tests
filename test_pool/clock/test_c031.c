/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_clock.h"

#define TEST_NUM  (SCMI_CLOCK_TEST_NUM_BASE + 31)
#define TEST_DESC "Clock possible parents get check        "

#define MAX_PARAMETER_SIZE 2

uint32_t clock_possible_parents_get_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t parameters[MAX_PARAMETER_SIZE];
    uint32_t clock_id, num_clocks;
    uint32_t parent_id_supp;
    uint32_t skip_parents = 0;
    uint32_t num_remaining_ids, num_ids_returned;
    uint32_t i, selector_id, *parents_ids_array, parent_index;
    uint32_t run_flag = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Skip if no domains found*/
    num_clocks = val_clock_get_info(NUM_CLOCKS, 0);
    if (num_clocks == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Clocks found                      ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM Clocks    : %d", num_clocks);

    /* Check Clock for valid possible parent ID */
    for (clock_id = 0; clock_id < num_clocks; clock_id++)
    {
        skip_parents = 0;
        num_remaining_ids = 0;
        num_ids_returned = 0;
        parent_index = 0;

        val_print(VAL_PRINT_TEST, "\n       CLOCK ID : %d", clock_id);
        val_print(VAL_PRINT_TEST, "\n     [Check 1] Query Parent Name Support");

       /* Check if parent name is supported for this clock */
        parent_id_supp = val_get_clock_attribute(clock_id, CLOCK_PARENT_IDENTIFIER_SUPPORT);
        if (parent_id_supp != CLOCK_PARENT_SUPP)
        {
            val_print(VAL_PRINT_ERR, "\n       Clock doesn't support Parent Identifier");
            continue;
        }
        run_flag = 1;

        do {
            VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
            parameters[param_count++] = clock_id;
            skip_parents += num_ids_returned;
            parameters[param_count++] = skip_parents;

            cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_POSSIBLE_PARENTS_GET,
                                             COMMAND_MSG);
            val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

            if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            val_print_return_values(return_value_count, return_values);

            num_remaining_ids = VAL_EXTRACT_BITS(return_values
                                                          [CLOCK_PARENTS_FLAGS_OFFSET], 24, 31);

            val_print(VAL_PRINT_DEBUG, "\n       NUM OF REMAINING PARENT IDS: %d",
                                         num_remaining_ids);
            if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(return_values
                                        [CLOCK_PARENTS_FLAGS_OFFSET], 8, 23)) != VAL_STATUS_PASS)
                return VAL_STATUS_FAIL;

            num_ids_returned = VAL_EXTRACT_BITS(return_values[CLOCK_PARENTS_FLAGS_OFFSET], 0, 7);
            val_print(VAL_PRINT_DEBUG, "\n       NUM OF CLOCK PARENTS RETURNED :  %d",
                                            num_ids_returned);

            parents_ids_array = &return_values[CLOCK_PARENTS_ARRAY_OFFSET];
            /* Print the parent id's associated with the clock */
            for (i = 0; i < num_ids_returned; i++)
            {
                selector_id = parents_ids_array[i];
                val_print(VAL_PRINT_DEBUG, "\n       PARENT ID[%d]    : %d",
                                                i, selector_id);
                val_clock_save_possible_parents(clock_id, parent_index, selector_id);
                parent_index++;
            }
            val_print(VAL_PRINT_DEBUG, "\n       num_remaining_ids: %d", num_remaining_ids);

        } while (num_remaining_ids > 0);
    }

    if (run_flag == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Clock support parent name");
        return VAL_STATUS_SKIP;
    }
    return VAL_STATUS_PASS;
}
