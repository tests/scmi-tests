/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_clock.h"

#define TEST_NUM  (SCMI_CLOCK_TEST_NUM_BASE + 25)
#define TEST_DESC "Clock config get check                       "


#define MAX_PARAMETER_SIZE 2

uint32_t clock_config_get_check(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t clock_id, num_clocks, parameters[MAX_PARAMETER_SIZE];
    uint32_t attributes, clock_config, ext_config_val;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;
    /* Skip if no clocks found */
    num_clocks = val_clock_get_info(NUM_CLOCKS, 0);
    if (num_clocks == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Clock found                              ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM CLOCKS     : %d", num_clocks);

    /* Get Clock Config value for each clock */
    for (clock_id = 0; clock_id < num_clocks; clock_id++)
    {
        val_print(VAL_PRINT_TEST, "\n     CLOCK ID: %d", clock_id);

        /* Get clock config status */
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = clock_id;
        parameters[param_count++] = 0;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_CONFIG_GET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
             return VAL_STATUS_FAIL;

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
             return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);
        attributes = return_values[ATTRIBUTE_OFFSET];
        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(attributes, 0, 31) != VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        clock_config = return_values[CLOCK_CONFIG_OFFSET];

        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(clock_config, 1, 31) !=
                                                                VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_DEBUG, "\n       CLOCK  State           : %d",
                            VAL_EXTRACT_BITS(clock_config, 0, 0));

        /* Check if Extended configuration is supported */
        attributes = val_clock_get_info(CLOCK_ATTRIBUTE, clock_id);
        if (!val_get_clock_attributes_flag(attributes, CLOCK_EXTENDED_CONFIGURATION_SUPPORT))
            continue;

        /* Get Duty cycle in extended clock config */
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = clock_id;
        parameters[param_count++] = CLOCK_DUTY_CYCLE_EXT_CONFIG;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_CONFIG_GET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
             return VAL_STATUS_FAIL;

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
             return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);
        attributes = return_values[ATTRIBUTE_OFFSET];
        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(attributes, 0, 31) != VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        clock_config = return_values[CLOCK_CONFIG_OFFSET];

        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(clock_config, 1, 31) !=
                                                                VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_DEBUG, "\n       CLOCK  State           : %d",
                            VAL_EXTRACT_BITS(clock_config, 0, 0));

        ext_config_val = return_values[CLOCK_CONFIG_EXT_VAL_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Duty Cycle Config Value : %d", ext_config_val);

        /* Get Phase in extended clock config */
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        parameters[param_count++] = clock_id;
        parameters[param_count++] = CLOCK_PHASE_EXT_CONFIG;

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_CLOCK, CLOCK_CONFIG_GET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
             return VAL_STATUS_FAIL;

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
             return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);
        attributes = return_values[ATTRIBUTE_OFFSET];
        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(attributes, 0, 31) != VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        clock_config = return_values[CLOCK_CONFIG_OFFSET];

        if (val_reserved_bits_check_is_zero(VAL_EXTRACT_BITS(clock_config, 1, 31) !=
                                                                VAL_STATUS_PASS))
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_DEBUG, "\n       CLOCK  State           : %d",
                            VAL_EXTRACT_BITS(clock_config, 0, 0));

        ext_config_val = return_values[CLOCK_CONFIG_EXT_VAL_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Phase Config Value : %d", ext_config_val);
    }

    return VAL_STATUS_PASS;
}
