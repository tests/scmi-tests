/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_powercap.h"

#define TEST_NUM  (SCMI_POWERCAP_TEST_NUM_BASE + 9)
#define TEST_DESC "Powercap set value check                     "

#define PARAMETER_SIZE        3

/********* TEST ALGO ********************
 * Check for Power cap DOMAINS
 * Set Powercap synchronously and expect delayed response
 * Set Powercap synchronously and ignore delayed response
 * Set Powercap asynchronously and ignore delayed response
 * Set Powercap asynchronously and expect delayed response
 * Wait for delayed response POWERCAP_CAP_SET_COMPLETE
*****************************************/

uint32_t powercap_set_power_cap_value(void)
{
    size_t param_count;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t return_value_count;
    int32_t status;
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t num_domains;
    uint32_t domain_id = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Skip if no powercaps domains found*/
    num_domains = val_powercap_get_info(NUM_POWERCAP_DOMAINS, 0x0);

    if (num_domains == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Powercap domains found                      ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM DOMAINS    : %d", num_domains);

    /* Set Powercap value for valid domain */
    for (domain_id = 0; domain_id < num_domains; domain_id++)
    {
        uint32_t attributes;

        val_print(VAL_PRINT_TEST, "\n       POWERCAP DOMAIN : %d", domain_id);

        attributes = val_powercap_get_info(POWERCAP_ATTRIBUTES, domain_id);

        /* Step 1 : Set Powercap synchronously and expect delayed response */
        val_print(VAL_PRINT_TEST, "\n     [Check 1] Set Powercap [Sync + Delay]");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status)
        parameters[param_count++] = domain_id;
        /* PC_CAP_SET_EXPECT will be ignored by the platform on a non-async request */
        parameters[param_count++] = val_get_cap_set_flag(PC_CAP_SET_SYNC, PC_CAP_SET_EXPECT);
        parameters[param_count++] = val_powercap_get_info(POWERCAP_POWERCAP, domain_id);

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_POWERCAP, POWERCAP_CAP_SET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        /* Step 2 : Set Powercap synchronously and ignore delayed response */
        val_print(VAL_PRINT_TEST, "\n     [Check 2] Set Powercap [Sync + Ignore]");
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status)
        parameters[param_count++] = domain_id;
        /* PC_CAP_SET_EXPECT will be ignored by the platform on a non-async request */
        parameters[param_count++] = val_get_cap_set_flag(PC_CAP_SET_SYNC, PC_CAP_SET_IGNORE);
        parameters[param_count++] = val_powercap_get_info(POWERCAP_POWERCAP, domain_id);

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_POWERCAP, POWERCAP_CAP_SET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        if (val_get_powercap_attributes_flag(attributes, PC_ASYNC_PC_SET_SUPP) == 0) {
            val_print(VAL_PRINT_TEST, "\n       Async PC set NOT supported...skip.");
            continue;
        }

        /* Step 3 : Set Powercap asynchronously and ignore delayed response */
        val_print(VAL_PRINT_TEST, "\n     [Check 3] Set Powercap [Async + Ignore]");
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status)
        parameters[param_count++] = domain_id;
        parameters[param_count++] = val_get_cap_set_flag(PC_CAP_SET_ASYNC, PC_CAP_SET_IGNORE);
        parameters[param_count++] = val_powercap_get_info(POWERCAP_POWERCAP, domain_id);

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_POWERCAP, POWERCAP_CAP_SET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        /* Expect an error since on PC_CAP_SET_IGNORE the platform should NOT have sent
         * any delayed response, and that will result in a timeout.
         */
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        val_receive_delayed_response(&rsp_msg_hdr, &status, &return_value_count, return_values);
        if (val_compare_status(status, SCMI_GENERIC_ERROR) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print(VAL_PRINT_TEST, "\n       DELAYED MSG    : NOT received (as expected)");

        /* Step 4 : Set Powercap asynchronously and expect delayed response */
        val_print(VAL_PRINT_TEST, "\n     [Check 4] Set Powercap [Async + Expect]");
        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status)
        parameters[param_count++] = domain_id;
        parameters[param_count++] = val_get_cap_set_flag(PC_CAP_SET_ASYNC, PC_CAP_SET_EXPECT);
        parameters[param_count++] = val_powercap_get_info(POWERCAP_POWERCAP, domain_id);

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_POWERCAP, POWERCAP_CAP_SET, COMMAND_MSG);
        val_send_message_async(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                     &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        /* Wait for delayed response POWERCAP_CAP_SET_COMPLETE*/
        val_print(VAL_PRINT_TEST, "\n     [Check 5] Wait for async delayed response");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        val_receive_delayed_response(&rsp_msg_hdr, &status, &return_value_count, return_values);

        val_print(VAL_PRINT_TEST, "\n       DELAYED MSG HDR   : 0x%08x", rsp_msg_hdr);
        if (val_compare("PROTOCOL ID", VAL_EXTRACT_BITS(rsp_msg_hdr, 10, 17),
                        PROTOCOL_POWERCAP))
            return VAL_STATUS_FAIL;
        if (val_compare("MSG TYPE   ", VAL_EXTRACT_BITS(rsp_msg_hdr, 8, 9),
                         DELAYED_RESPONSE_MSG))
            return VAL_STATUS_FAIL;
        if (val_compare("MSG ID     ", VAL_EXTRACT_BITS(rsp_msg_hdr, 0, 7),
                         POWERCAP_CAP_SET_COMPLETE))
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        /* Compare send & received powercap values */
        if (val_compare("Domain ID           ",
                         domain_id, return_values[DELAYED_RESP_DOMAIN_ID_OFFSET]))
            return VAL_STATUS_FAIL;

        if (val_compare("POWERCAP        ",
                        val_powercap_get_info(POWERCAP_POWERCAP, domain_id),
                        return_values[DELAYED_RESP_POWER_CAP_OFFSET]))
            return VAL_STATUS_FAIL;
    }

    return VAL_STATUS_PASS;
}
