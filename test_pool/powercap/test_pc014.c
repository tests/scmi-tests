/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_powercap.h"

#define TEST_NUM  (SCMI_POWERCAP_TEST_NUM_BASE + 14)
#define TEST_DESC "Powercap set value check                     "

#define PARAMETER_SIZE        3

uint32_t powercap_set_pai_value(void)
{
    size_t param_count;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t return_value_count;
    int32_t status;
    uint32_t parameters[PARAMETER_SIZE];
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t num_domains;
    uint32_t domain_id = 0;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Skip if no powercaps domains found*/
    num_domains = val_powercap_get_info(NUM_POWERCAP_DOMAINS, 0x0);

    if (num_domains == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Powercap domains found                      ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM DOMAINS    : %d", num_domains);

    /* Set pai value for valid domain */
    for (domain_id = 0; domain_id < num_domains; domain_id++)
    {
        val_print(VAL_PRINT_TEST, "\n       POWERCAP DOMAIN : %d", domain_id);
        val_print(VAL_PRINT_TEST, "\n     [Check 1] Set Pai value");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status)
        parameters[param_count++] = domain_id;
        parameters[param_count++] = 0x0;
        parameters[param_count++] = val_powercap_get_info(POWERCAP_PAI, domain_id);

        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_POWERCAP, POWERCAP_PAI_SET, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, parameters, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);
    }

    return VAL_STATUS_PASS;
}
