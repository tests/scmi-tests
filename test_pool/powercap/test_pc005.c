/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include "val_interface.h"
#include "val_powercap.h"

#define NAME_OFFSET                        1

#define TEST_NUM  (SCMI_POWERCAP_TEST_NUM_BASE + 5)
#define TEST_DESC "Powercap domain attributes check             "

uint32_t powercap_query_domain_attributes(void)
{
    int32_t  status;
    uint32_t rsp_msg_hdr;
    uint32_t cmd_msg_hdr;
    size_t   param_count;
    size_t   return_value_count;
    uint32_t return_values[MAX_RETURNS_SIZE];
    uint32_t domain_id, num_domains, attributes;
    uint32_t min_pai, max_pai, pai_step;
    uint32_t min_power_cap, max_power_cap, power_cap_step, ext_name_supp;
    uint32_t sustainable_power, accuracy, parent_id;
    char *name;

    if (val_test_initialize(TEST_NUM, TEST_DESC) != VAL_STATUS_PASS)
        return VAL_STATUS_SKIP;

    /* Skip if no domains found*/
    num_domains = val_powercap_get_info(NUM_POWERCAP_DOMAINS, 0x0);
    if (num_domains == 0) {
        val_print(VAL_PRINT_ERR, "\n       No Powercap domains found                      ");
        return VAL_STATUS_SKIP;
    }
    val_print(VAL_PRINT_DEBUG, "\n       NUM DOMAINS    : %d", num_domains);

    /* Check Powercap domain attributes for valid domain */
    for (domain_id = 0; domain_id < num_domains; domain_id++)
    {
        val_print(VAL_PRINT_TEST, "\n       POWERCAP DOMAIN : %d", domain_id);
        val_print(VAL_PRINT_TEST, "\n     [Check 1] Query attributes");

        VAL_INIT_TEST_PARAM(param_count, rsp_msg_hdr, return_value_count, status);
        param_count++;
        cmd_msg_hdr = val_msg_hdr_create(PROTOCOL_POWERCAP,
                                         POWERCAP_DOMAIN_ATTRIBUTES, COMMAND_MSG);
        val_send_message(cmd_msg_hdr, param_count, &domain_id, &rsp_msg_hdr, &status,
                         &return_value_count, return_values);

        if (val_compare_status(status, SCMI_SUCCESS) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        if (val_compare_msg_hdr(cmd_msg_hdr, rsp_msg_hdr) != VAL_STATUS_PASS)
            return VAL_STATUS_FAIL;

        val_print_return_values(return_value_count, return_values);

        attributes = return_values[ATTRIBUTE_OFFSET];
        val_powercap_save_info(POWERCAP_ATTRIBUTES, domain_id, attributes);

        /* Extract powercap domain attributes */
        val_print(VAL_PRINT_DEBUG, "\n       PC change notify supp         : %d",
                  val_get_powercap_attributes_flag(attributes, PC_CHANGE_NOTIFY_SUPP));
        val_print(VAL_PRINT_DEBUG, "\n       PC measure change notify supp : %d",
                  val_get_powercap_attributes_flag(attributes, PC_MEASUR_CHANG_NOTIFY_SUPP));
        val_print(VAL_PRINT_DEBUG, "\n       Async PC set supp             : %d",
                  val_get_powercap_attributes_flag(attributes, PC_ASYNC_PC_SET_SUPP));
        val_print(VAL_PRINT_DEBUG, "\n       Extended PC name supp             : %d",
                  val_get_powercap_attributes_flag(attributes, PC_EXTENDED_NAME));
        val_print(VAL_PRINT_DEBUG, "\n       PC config supp                : %d",
                  val_get_powercap_attributes_flag(attributes, PC_CONFIG_SUPP));
        val_print(VAL_PRINT_DEBUG, "\n       PC power monitor supp         : %d",
                  val_get_powercap_attributes_flag(attributes, PC_POWER_MONITOR_SUPP));
        val_print(VAL_PRINT_DEBUG, "\n       PC PAI config supp            : %d",
                  val_get_powercap_attributes_flag(attributes, PC_PAI_CONFIG_SUPP));
        val_print(VAL_PRINT_DEBUG, "\n       PC power unit                 : %d",
                  val_get_powercap_attributes_flag(attributes, PC_POWER_UNIT));
        val_print(VAL_PRINT_DEBUG, "\n       PC fast channel supp          : %d",
                  val_get_powercap_attributes_flag(attributes, PC_FASTCHANNEL_SUPPORT));

        ext_name_supp = val_get_powercap_attributes_flag(attributes, PC_EXTENDED_NAME);
        name = (char *)val_powercap_get_expected_name(domain_id);

        /* Domain name */
        if (ext_name_supp) {
            if (val_compare_str("DOMAIN NAME", (char *)&return_values[NAME_OFFSET],
                            name + strnlen(name, SCMI_NAME_STR_SIZE) - 16, SCMI_NAME_STR_SIZE))
                return VAL_STATUS_FAIL;
        } else {
            if (val_compare_str("DOMAIN NAME", (char *)&return_values[NAME_OFFSET],
                            name, SCMI_NAME_STR_SIZE))
                return VAL_STATUS_FAIL;
        }

        /* PAI values */
        min_pai = return_values[MIN_PAI_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Min PAI              : %d", min_pai);
        max_pai = return_values[MAX_PAI_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Max PAI              : %d", max_pai);
        pai_step = return_values[PAI_STEP_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       PAI step             : %d", pai_step);

        /* Power capping values */
        min_power_cap = return_values[MIN_POWER_CAP_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Min power capping    : %d", min_power_cap);
        val_powercap_save_info(POWERCAP_MIN_POWER, domain_id, min_power_cap);

        max_power_cap = return_values[MAX_POWER_CAP_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Max power capping    : %d", max_power_cap);
        val_powercap_save_info(POWERCAP_MAX_POWER, domain_id, max_power_cap);

        power_cap_step = return_values[POWER_CAP_STEP_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       power capping step   : %d", power_cap_step);

        sustainable_power = return_values[SUSTAINAIBLE_POWER_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Sustainable power    : %d", sustainable_power);

        accuracy = return_values[ACCURACY_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Accuracy             : %d", accuracy);

        parent_id = return_values[PARENT_ID_OFFSET];
        val_print(VAL_PRINT_DEBUG, "\n       Parent ID            : %d", parent_id);
    }

    return VAL_STATUS_PASS;
}
