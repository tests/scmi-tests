
**Guide for testing on LINUX**
=================================

Table of Contents:
- [Introduction](#introduction)
- [Software stack](#software-stack)
- [Linux kernel](#linux-kernel)
  * [Mailbox test driver](#mailbox-test-driver)
  * [Doorbell support patches for mailbox](#doorbell-support-patches-for-mailbox)
  * [Additional changes to enable mailbox test driver](#additional-changes-to-enable-mailbox-test-driver)
  * [SCMI Raw mode support](#scmi-raw-driver)
- [Kernel build](#kernel-build)

Introduction
-------
This document lists the instructions that must be followed to run SCMI ACS as Linux application using one of the available test interfaces on any of the available targets.
For an introduction to the System Guidance for Mobile (SGM) platforms, please refer to the [Arm Developer documentation].

Software Stack
-------
Arm provides a [super-project] with guides for building and running a full software stack on Arm platforms. This project provides a convenient wrapper around the various build systems involved in the software stack. Please contact Arm at  support-connect@arm.com  for support on software stack of Linux platform.

Linux kernel
-------
There are two main ways in which the SCMI ACS, running as a Linux application, can inject and snoop SCMI traffic back and forth from the SCMI backend server that is intended to be tested.

A test interface based on the Mailbox test driver was provided at first: this setup assumes that the SCMI backend server under test is placed in a setup where the communication channel is based on mailboxes and you'll need a properly configured Mailbox test driver as detailed below.

Alternatively, on more recent kernels, the standard SCMI stack can be configured to expose on debugfs a few entries can be used to inject and snoop bare SCMI raw messages; this way the traffic going back and forth will use whatever underlying transport is currently configured on the system: as a consequence it is possible to verify the compliance of the SCMI server under test no matter where it is placed and which transport medium eploys.

The following alternative changes must be made in the Linux kernel source code after downloading the software stack for SGM/TC.

### Mailbox test driver
To use SCMI test agent on SGM/TC target, the Linux kernel must be rebuilt to include the mailbox test driver with mailbox doorbell support and additional changes. Doorbell support patches for mailbox, and additional changes to enable mailbox test driver are tested against Linux kernel version 4.13.

### Doorbell support patches for mailbox
The doorbell support for mailbox driver is enabled by applying a patch series that is currently discussed in LKML. For more information, see [Mailbox doorbell support patches]. These patches must be applied to the linux kernel.

### Additional changes to enable mailbox test driver
In addition to applying the patches, follow these steps before starting the kernel build.

`Enable mailbox test driver`: Set CONFIG_MAILBOX_TEST=y in kernel config to include mailbox test driver in the kernel.

`Modify mailbox driver to prevent format conversion`: The current version of mailbox driver always converts raw binary data to hex format. For SCMI test agent, we expect raw data unmodified for processing. The change that is shown below prevents the format change.
<br> The driver support to add this as a configurable option is planned for the future.
<br> Until that change is added, the change that is shown below is required in the mbox_test_message_read function in drivers/mailbox/mailbox-test.c:

```
    }
    *(touser + l) = '\0';

+   ret = simple_read_from_buffer(userbuf, count, ppos, tdev->rx_buffer,
+                   MBOX_HEXDUMP_MAX_LEN);
+
    memset(tdev->rx_buffer, 0, MBOX_MAX_MSG_LEN);
    mbox_data_ready = false;

    spin_unlock_irqrestore(&tdev->lock, flags);

-   ret = simple_read_from_buffer(userbuf, count, ppos, touser, MBOX_HEXDUMP_MAX_LEN);
waitq_err:
    __set_current_state(TASK_RUNNING);
    remove_wait_queue(&tdev->waitq, &wait);
```
The current change in mailbox test driver is not an ideal solution. A better solution is to use sysfs entry for configurability. This enhancement will be upstreamed in the future, thereby making this change redundant.

### SCMI Raw mode support
Alternatively, on supported Kernels, it is possible to configure the SCMI test agent on any target to use the Kernel SCMI Raw access mode; in such a case the Kernel must be rebuild enabling CONFIG_ARM_SCMI_RAW_MODE_SUPPORT=y and the SCMI ACS suite itself must be rebuild specifying TRANS=raw on the make command line, like in:

> `make  PLAT=linux TRANS=raw TARGET=sgm776 PROTOCOLS=base,clock,power_domain,performance,system_power,sensor,voltage,reset,powercap,pin_control VERBOSE=1`

If TRANS= is not specified the usual Mailbox driver backend is instead compiled into the SCMI ACS suite.

No device tree change is needed in this case.

The SCMI ACS then starts injecting/snooping SCMI messages via the following debugfs API entries, as exposed by the Kernel OSPM Agent running in Raw mode:

```
	/sys/kernel/debug/scmi/
	`-- 0
	    |-- atomic_threshold_us
	    |-- instance_name
	    |-- raw
	    |   |-- channels
	    |   |   |-- 0x10
	    |   |   |   |-- message
	    |   |   |   `-- message_async
	    |   |   `-- 0x13
	    |   |       |-- message
	    |   |       `-- message_async
	    |   |-- errors
	    |   |-- message
	    |   |-- message_async
	    |   |-- notification
	    |   `-- reset
	    `-- transport
            |-- is_atomic
            |-- max_msg_size
            |-- max_rx_timeout_ms
            |-- rx_max_msg
            |-- tx_max_msg
            `-- type
```

## Kernel build
Build the kernel and device tree after making the changes and use the run-scripts for running the SGM/TC

For instructions to build the test suite for Linux platform and running it, see relevant sections in [User Guide].

- - - - - - - - - - - - - - - -

_Copyright (c) 2019-2023, Arm Limited and Contributors. All rights reserved._

[ARM Developer documentation]:        https://developer.arm.com/tools-and-software/simulation-models/fixed-virtual-platforms
[Mailbox doorbell support patches]:    https://lkml.org/lkml/2017/5/24/339
[User Guide]:                ./user_guide.md
[super-project]:  https://git.linaro.org/landing-teams/working/arm/arm-reference-platforms.git/about/docs/user-guide.rst
