/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/
#ifdef PIN_CONTROL_PROTOCOL

#include <pal_interface.h>
#include <pal_pin_control_expected.h>
/*-----------  Common POWER API's across platforms ----------*/

/**
  @brief   This API is used for checking num of pins
  @param   none
  @return  num of pins
**/
uint32_t pal_pin_control_get_expected_num_pins(void)
{
    return PAL_SCMI_ANYTHING;
}

/**
  @brief   This API is used for checking num of groups
  @param   none
  @return  num of groups
**/
uint32_t pal_pin_control_get_expected_num_groups(void)
{
    return PAL_SCMI_ANYTHING;
}

/**
  @brief   This API is used for checking num of functions
  @param   none
  @return  num of functions
**/
uint32_t pal_pin_control_get_expected_num_functions(void)
{
    return PAL_SCMI_ANYTHING;
}

/**
  @brief   This API is used for checking num of pins in a group
  @param   identifier of the pin
  @return  num of pins in a group
**/
uint32_t pal_pin_control_get_expected_pins_in_group(uint32_t identifier)
{
    return PAL_SCMI_ANYTHING;
}

/**
  @brief   This API is used for checking num of groups associated with the function
  @param   identifier of the function
  @return  num of groups in function
**/
uint32_t pal_pin_control_get_expected_groups_in_func(uint32_t identifier)
{
    return PAL_SCMI_ANYTHING;
}

/**
  @brief   This API is used to get identifier of pin associated with the group
  @param   identifier of the group
  @return  pin identifier of group in index
**/
uint32_t pal_pin_control_get_expected_group_associations(uint32_t identifier, uint32_t index)
{
    return PAL_SCMI_ANYTHING;
}

/**
  @brief   This API is used to get identifier of group associated with the function
  @param   identifier of the function
  @return  pin identifier of function in index
**/
uint32_t pal_pin_control_get_expected_function_associations(uint32_t identifier, uint32_t index)
{
    return PAL_SCMI_ANYTHING;
}

/**
@brief   This API is used to check if pin or group is accessible for given agent id
           1. Caller       -  Test Suite.
  @param   agent_id     agent id
  @param   identifier   The identifier of pin or group
  @param   selector     Selector of pin or group
  @return  status       0 on access
**/
uint32_t pal_pinctrl_check_access(uint32_t agent_id, uint32_t identifier, uint32_t selector)

{
   return linux_pinctrl_check_access(agent_id, identifier, selector);
}
#endif
