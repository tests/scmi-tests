/** @file
 * Copyright (c) 2023, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

#include <errno.h>
#include <limits.h>
#include <poll.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <signal.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <pal_platform.h>

#include "val_interface.h"

#define NO_ERROR 0
#define ERROR 5

#define TRANS_DEFAULT_BASE_PATH         "/sys/kernel/debug/scmi/0"

#define TRANS_MAX_MSG_SZ_FILE           "transport/max_msg_size"
#define TRANS_RX_TMO_MS                 "transport/max_rx_timeout_ms"

#define TRANS_RAW_RESET_FILE            "raw/reset"
#define TRANS_MESSAGE_FILE              "raw/message"
#define TRANS_MESSAGE_ASYNC_FILE        "raw/message_async"
#define TRANS_NOTIF_FILE                "raw/notification"
#define TRANS_ERROR_FILE                "raw/errors"

#define MAX_BUF                 32
#define MESSAGE_PAYLOAD_START   4

struct linux_scmi_raw_config {
    int fd_base;
    int fd_reset;
    unsigned long max_msg_size;
    unsigned long rx_timeout_ms;
    void *buffer;
};

static struct linux_scmi_raw_config *rawc;

static int safe_read(int fd, void *buf, size_t count)
{
    int ret, bytes = 0;

    do {
        ret = read(fd, buf + bytes, count - bytes);
        if (ret < 0) {
            if (errno == -EINTR)
                continue;
            return ret;
        }
        bytes += ret;
    } while (ret);

    return bytes;
}

static int safe_write(int fd, const void *buf, size_t count)
{
    int bytes = 0;

    do {
        int ret;

        ret = write(fd, buf + bytes, count - bytes);
        if (ret < 0) {
            if (errno == -EINTR)
                continue;
            return ret;
        }
        bytes += ret;
    } while (count > bytes);

    return bytes;
}

static int linux_read_integer_from_file(int fd_base, char *filepath,
                                        unsigned long *val)
{
    int fd, count;
    unsigned long value;
    unsigned char buffer[MAX_BUF];

    *val = 0;
    fd = openat(fd_base, filepath, O_RDONLY | O_NONBLOCK);
    if (fd < 0)
        return ERROR;

    bzero(buffer, MAX_BUF);
    /* read returned message into the buffer */
    count = read(fd, buffer, MAX_BUF);
    if (count < 0) {
        close(fd);
        return ERROR;
    }

    value = strtoul((const char *)buffer, NULL, 10);
    if (value == ULONG_MAX) {
        close(fd);
        return ERROR;
    }

    *val = value;
    close(fd);

    return 0;
}

static void scmi_raw_mode_cleanup(void)
{
    if (!rawc)
        return;

    close(rawc->fd_reset);
    close(rawc->fd_base);

    free(rawc->buffer);
    free(rawc);
}

static void term_handler(int sig)
{
    scmi_raw_mode_cleanup();

    raise(sig);
}

/*!
 * @brief Interface function that performs any needed Linux initialization.
 */
uint32_t linux_initialize_system(void *info)
{
    int ret;
    const char *base_path = TRANS_DEFAULT_BASE_PATH;
    struct linux_scmi_raw_config *conf;
    struct sigaction action;

    conf = malloc(sizeof(*conf));
    if (!conf)
        return ERROR;

    memset(conf, 0, sizeof(*conf));

    if (info)
        base_path = (const char *)info;
    val_print(VAL_PRINT_INFO, "\n   Using SCMI kernel Raw transport rooted at:%s/raw", base_path);

    conf->fd_base = open(base_path, O_RDONLY | O_NONBLOCK);
    if (conf->fd_base < 0) {
        val_print(VAL_PRINT_ERR, "\n   Failed to open:%s", base_path);
        goto err_conf;
    }

    ret = linux_read_integer_from_file(conf->fd_base, TRANS_RX_TMO_MS,
                                       &conf->rx_timeout_ms);
    if (ret)
        goto err_fd_base;

    ret = linux_read_integer_from_file(conf->fd_base, TRANS_MAX_MSG_SZ_FILE,
                                       &conf->max_msg_size);
    if (ret)
        goto err_fd_base;

    /* Add space for the SCMI message header */
    conf->max_msg_size += sizeof(uint32_t);
    conf->buffer = malloc(conf->max_msg_size);
    if (!conf->buffer)
        goto err_fd_base;

    conf->fd_reset = openat(conf->fd_base,
                            TRANS_RAW_RESET_FILE, O_RDWR | O_SYNC);
    if (conf->fd_reset < 0)
        goto err_buffer;

    bzero(&action, sizeof(struct sigaction));
    action.sa_handler = term_handler;
    action.sa_flags = SA_RESETHAND;
    ret = sigaction(SIGINT, &action, 0);
    if (ret)
        goto err_fd_reset;

    /* Flush any pending SCMI messages on start...just in case */
    val_print(VAL_PRINT_INFO, "\n   Resetting SCMI kernel Raw queues.");
    safe_write(conf->fd_reset, "Y\n", 3);

    rawc = conf;
    atexit(scmi_raw_mode_cleanup);

    return NO_ERROR;

err_fd_reset:
    close(conf->fd_reset);
err_buffer:
    free(conf->buffer);
err_fd_base:
    close(conf->fd_base);
err_conf:
    free(conf);

    return ERROR;
}

static int32_t __linux_send_message(uint32_t message_header_send, size_t parameter_count,
        const uint32_t *parameters, uint32_t *message_header_rcv, int32_t *status,
        size_t *return_values_count, uint32_t *return_values, const char *filepath)
{
    int fd_message;
    uint32_t ret = 0;
    struct pollfd pfd;
    ssize_t count;
    uint32_t counter;
    uint8_t *buffer;

    buffer = rawc->buffer;
    memset(buffer, 0x0, rawc->max_msg_size);

    for (counter = 0; counter < 4; ++counter)
        buffer[counter] = (message_header_send >> (counter * 8)) & 0xFF;

    for (counter = 0; counter < (parameter_count * 4); counter += 4) {
        uint32_t byte_shift;

        for (byte_shift = 0; byte_shift < 4; ++byte_shift) {
            buffer[MESSAGE_PAYLOAD_START+counter+byte_shift] =
                (parameters[counter/4] >> (byte_shift * 8)) & 0xFF;
        }
    }

    fd_message = openat(rawc->fd_base, filepath, O_RDWR | O_SYNC);
    if (fd_message < 0)
        return ERROR;

    safe_write(fd_message, buffer,
          sizeof(uint32_t) + parameter_count * sizeof(uint32_t));

    /* polling */
    pfd.fd = fd_message;
    pfd.events = POLLIN;
    ret = poll(&pfd, 1, rawc->rx_timeout_ms + 1000);
    if (ret <= 0) {
        close(fd_message);
        return ERROR;
    }

    /* read returned message into the buffer */
    count = safe_read(fd_message, buffer, rawc->max_msg_size);
    if (count < 0) {
        close(fd_message);
        return ERROR;
    }

    /* read the payload part */
    for (counter = 0; counter < count / 4; ++counter) {
        if (counter == 0) {
            /* extract header */
            *message_header_rcv = *((uint32_t *)buffer + counter);
        } else if (counter == 1) {
            /* extract status */
            *status = *((uint32_t *)buffer + counter);
        } else {
            /* extract payload */
            /* return_values starts from 0, hence subtract 2 here */
            return_values[(counter - 2)] = *((uint32_t *)buffer + counter);
            (*return_values_count)++;
        }
    }

    close(fd_message);

    return NO_ERROR;
}

/*!
 * @brief Interface function that sends a
 * command to SCMI raw message interfaces and receives a platform
 * message.
 */
int32_t linux_send_message(uint32_t message_header_send, size_t parameter_count,
        const uint32_t *parameters, uint32_t *message_header_rcv, int32_t *status,
        size_t *return_values_count, uint32_t *return_values)
{
    return __linux_send_message(message_header_send, parameter_count, parameters,
                                message_header_rcv, status, return_values_count,
                                return_values, TRANS_MESSAGE_FILE);
}

/*!
 * @brief Interface function that sends an asynchronous
 * command to SCMI raw message interfaces and receives the platform
 * immediate reply message. Delayed response is collected with a dedicated function.
 */
int32_t linux_send_message_async(uint32_t message_header_send, size_t parameter_count,
        const uint32_t *parameters, uint32_t *message_header_rcv, int32_t *status,
        size_t *return_values_count, uint32_t *return_values)
{
    return __linux_send_message(message_header_send, parameter_count, parameters,
                                message_header_rcv, status, return_values_count,
                                return_values, TRANS_MESSAGE_ASYNC_FILE);
}

/*!
 * @brief Interface function that waits for
 * delayed response.
 */
int linux_wait_for_response(uint32_t *message_header_rcv,
        int32_t *status, size_t *return_values_count, uint32_t *return_values,
        bool *message_ready, uint32_t timeout)
{
    int fd_message;
    uint32_t ret = 0;
    uint8_t *buffer;
    uint32_t counter;
    struct pollfd pfd;
    ssize_t count;

    buffer = rawc->buffer;
    memset(buffer, 0x0, rawc->max_msg_size);

    fd_message = openat(rawc->fd_base, TRANS_MESSAGE_FILE, O_RDONLY | O_SYNC);
    if (fd_message < 0)
        return ERROR;

    /* polling */
    pfd.fd = fd_message;
    pfd.events = POLLIN;
    ret = poll(&pfd, 1, rawc->rx_timeout_ms + 1000);
    if (ret <= 0) {
        close(fd_message);
        return ERROR;
    }

    /* read returned message into the buffer */
    count = safe_read(fd_message, buffer, rawc->max_msg_size);
    if (count < 0) {
        close(fd_message);
        return ERROR;
    }

    /* read the payload part */
    for (counter = 0; counter < count / 4; ++counter) {
        if (counter == 0) {
            /* extract header */
            *message_header_rcv = *((uint32_t *)buffer + counter);
        } else if (counter == 1) {
            /* extract status */
            *status = *((uint32_t *)buffer + counter);
        } else {
            /* extract payload */
            /* return_values starts from 0, hence subtract 2 here */
            return_values[(counter - 2)] = *((uint32_t *)buffer + counter);
            (*return_values_count)++;
        }
    }

    close(fd_message);

    return NO_ERROR;
}

/*!
 * @brief Interface function that waits for
 * notification.
 */
int linux_wait_for_notification(uint32_t *message_header_rcv,
        size_t *return_values_count, uint32_t *return_values,
        uint32_t timeout)
{
    int fd_message;
    uint32_t ret = 0;
    uint8_t *buffer;
    uint32_t counter;
    struct pollfd pfd;
    ssize_t count;

    buffer = rawc->buffer;
    memset(buffer, 0x0, rawc->max_msg_size);

    fd_message = openat(rawc->fd_base, TRANS_NOTIF_FILE, O_RDONLY | O_SYNC);
    if (fd_message < 0)
        return ERROR;

    /* polling */
    pfd.fd = fd_message;
    pfd.events = POLLIN;
    ret = poll(&pfd, 1, timeout * 30);
    if (ret <= 0) {
        close(fd_message);
        return ERROR;
    }

    /* read returned message into the buffer */
    count = safe_read(fd_message, buffer, rawc->max_msg_size);
    if (count < 0) {
        close(fd_message);
        return ERROR;
    }

    /* read the payload part */
    for (counter = 0; counter < count / 4; ++counter) {
        if (counter == 0) {
            /* extract header */
            /* plus 1 to skip the Length */
            *message_header_rcv = *((uint32_t *)buffer + counter);
        } else {
            /* extract payload */
            return_values[(counter - 1)] = *((uint32_t *)buffer + counter);
            (*return_values_count)++;
        }
    }

    close(fd_message);

    return NO_ERROR;
}

/*!
 * @brief Interface function that gets accessible device for given agent
 */
uint32_t linux_agent_get_accessible_device(uint32_t agent_id)
{
    return NO_ERROR;
}

/*!
 * @brief Interface function that gets inaccessible device for given agent
 */
uint32_t linux_agent_get_inaccessible_device(uint32_t agent_id)
{
    return NO_ERROR;
}

/*!
 * @brief Interface function that gets protocol which can access given device
 */
uint32_t linux_device_get_accessible_protocol(uint32_t device_id)
{
    return NO_ERROR;
}

/*!
 * @brief Interface function that checks if the given pin or group is accessible for given agent
 */
uint32_t linux_pinctrl_check_access(uint32_t agent_id, uint32_t identifier, uint32_t selector)
{
    return NO_ERROR;
}
