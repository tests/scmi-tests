/** @file
 * Copyright (c) 2022 Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 **/

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

/*!
 * @brief Interface function that maps a physical memory area to a virtual one
 * using Linux OS facilities.
 *
 */
int linux_map_phys_to_virt(unsigned long paddr, size_t length, unsigned long *vaddr)
{
    int fd;
    long page_size;
    unsigned long map_mask;
    void *map_base;

    page_size = sysconf(_SC_PAGESIZE);
    if (page_size == -1)
        return page_size;

    fd = open("/dev/mem", O_RDWR, O_SYNC);
    if (fd < 0)
        return fd;

    map_mask = page_size - 1;
    map_base = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED,
                    fd, (paddr & ~map_mask));
    close(fd);
    if (map_base == (void *) -1)
        return -1;

    *vaddr = (unsigned long)map_base + (paddr & map_mask);

    return 0;
}

/*!
 * @brief Interface function that unmaps a previously mapped virtual memory area
 * using Linux OS facilities.
 *
 */
int linux_unmap_phys_to_virt(unsigned long paddr, unsigned long vaddr, size_t length)
{
    long page_size;
    unsigned long map_mask, map_base;

    page_size = sysconf(_SC_PAGESIZE);
    if (page_size == -1)
        return page_size;

    map_mask = page_size - 1;
    map_base = vaddr - (paddr & map_mask);

    munmap((void *)map_base, length);

    return 0;
}
