/** @file
 * Copyright (c) 2021, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifdef POWERCAP_PROTOCOL

#include "pal_interface.h"
#include "pal_powercap_expected.h"

/**
  @brief   This API is used for checking num of powercap  domain
  @param   none
  @return  num of powercap domain
**/
uint32_t pal_powercap_get_expected_num_domains(void)
{
    return num_powercap_domains;
}

/**
  @brief   This API is used for checking powercap domain name
  @param   domain id
  @return  powercap domain name
**/
uint8_t *pal_powercap_get_expected_name(uint32_t domain_id)
{
    return (uint8_t *)powercap_domain_name[domain_id];
}

#endif
