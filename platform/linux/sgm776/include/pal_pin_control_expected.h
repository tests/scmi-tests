/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef __PAL_PIN_CONTROL_EXPECTED_H__
#define __PAL_PIN_CONTROL_EXPECTED_H__

#include <pal_platform.h>

#ifdef PIN_CONTROL_PROTOCOL
/* Expected PIN CONTROL parameters */
static uint32_t num_groups = 1;
static uint32_t num_pins = 2;
static uint32_t num_functions = 1;

static uint16_t num_pins_in_group[] = {1, 2};
static uint16_t num_groups_in_function[] = {1, 1};

static uint32_t *group_associations[] = {
    (uint32_t[]) { /* GROUP 0 */
        1,
    },
    (uint32_t[]) { /* GROUP 1 */
        2,
        3,
    },
};

static uint32_t *function_associations[] = {
    (uint32_t[]) { /* FUNCTION 0 */
        1,
    },
    (uint32_t[]) { /* FUNCTION 1 */
        2,
    },
};
#endif

#endif /* __PAL_PIN_CONTROL_EXPECTED_H__ */
