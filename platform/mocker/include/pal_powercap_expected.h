/** @file
 * Copyright (c) 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef __PAL_POWERCAP_EXPECTED_H__
#define __PAL_POWERCAP_EXPECTED_H__

#include <pal_platform.h>

static uint32_t num_powercap_domain = 0x02;

static char *powercap_domain_name[] = {
    "EXT_POWERCAP_EXTENDED_GPU", "CPU"
};

static uint32_t min_pai[] = {100, 150};
static uint32_t max_pai[] = {300, 350};
static uint32_t pai_step[] = {4, 5};
static uint32_t min_power_cap[] = {120, 110};
static uint32_t max_power_cap[] = {300, 340};
static uint32_t power_cap_step[] = {10, 8};
static uint32_t sustainable_power[] = {150, 200};
static uint32_t accuracy[] = {50, 50};
static uint32_t pai[] = {130, 180};
static uint32_t power_cap[] = {150, 200};
static uint32_t avg_power[] = {200, 400};
static uint32_t powercap_extended_name_support[] ={1, 0};

static uint32_t supported_powercap_protocols[] = {
    SUPPORTED_POWERCAP_VERSION_1,
    SUPPORTED_POWERCAP_VERSION_2,
};

#endif /* __PAL_POWERCAP_EXPECTED_H__ */

