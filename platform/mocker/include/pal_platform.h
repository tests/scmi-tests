/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef __PAL_PLATFORM__
#define __PAL_PLATFORM__

#include <inttypes.h>
#include <stddef.h>
#include <assert.h>

#define BASE_PROTOCOL_ID              0x10
#define POWER_DOMAIN_PROTOCOL_ID      0x11
#define SYSTEM_POWER_PROTOCOL_ID      0x12
#define PERFORMANCE_PROTOCOL_ID       0x13
#define CLOCK_PROTOCOL_ID             0x14
#define SENSOR_PROTOCOL_ID            0x15
#define RESET_PROTOCOL_ID             0x16
#define VOLTAGE_PROTOCOL_ID           0x17
#define POWERCAP_PROTOCOL_ID          0x18
#define PIN_CONTROL_PROTOCOL_ID       0x19

#define SUPPORTED_BASE_VERSION_1       0x00010000
#define SUPPORTED_BASE_VERSION_2       0x00020000
#define SUPPORTED_BASE_VERSION_2P1     0x00020001

#define SUPPORTED_CLOCK_VERSION_1      0x00010000
#define SUPPORTED_CLOCK_VERSION_2      0x00020000
#define SUPPORTED_CLOCK_VERSION_3      0x00030000

#define SUPPORTED_POWERCAP_VERSION_1   0x00010000
#define SUPPORTED_POWERCAP_VERSION_2   0x00020000

#define SUPPORTED_RESET_VERSION_1      0x00010000
#define SUPPORTED_RESET_VERSION_2      0x00020000
#define SUPPORTED_RESET_VERSION_3      0x00030000
#define SUPPORTED_RESET_VERSION_3P1    0x00030001

#define SUPPORTED_SENSOR_VERSION_1     0x00010000
#define SUPPORTED_SENSOR_VERSION_2     0x00020000
#define SUPPORTED_SENSOR_VERSION_3     0x00030000
#define SUPPORTED_SENSOR_VERSION_3P1   0x00030001

#define SUPPORTED_VOLTAGE_VERSION_1    0x00010000
#define SUPPORTED_VOLTAGE_VERSION_2    0x00020000
#define SUPPORTED_VOLTAGE_VERSION_2P1  0x00020001

#define SUPPORTED_PERFORMANCE_VERSION_1    0x00010000
#define SUPPORTED_PERFORMANCE_VERSION_2    0x00020000
#define SUPPORTED_PERFORMANCE_VERSION_3    0x00030000
#define SUPPORTED_PERFORMANCE_VERSION_4    0x00040000

#define SUPPORTED_PIN_CONTROL_VERSION_1    0x00010000

#define SUPPORTED_POWER_DOMAIN_VERSION_1   0x00010000
#define SUPPORTED_POWER_DOMAIN_VERSION_2   0x00020000
#define SUPPORTED_POWER_DOMAIN_VERSION_2P1 0x00020001
#define SUPPORTED_POWER_DOMAIN_VERSION_3   0x00030000
#define SUPPORTED_POWER_DOMAIN_VERSION_3P1 0x00030001

#define SUPPORTED_SYSTEM_POWER_VERSION_1   0x00010000
#define SUPPORTED_SYSTEM_POWER_VERSION_2   0x00020000
#define SUPPORTED_SYSTEM_POWER_VERSION_2P1 0x00020001

enum BITS_HEADER {
    RESERVED_LOW =      28,
    RESERVED_HIGH =     31,
    TOKEN_LOW =         18,
    TOKEN_HIGH =        27,
    PROTOCOL_ID_LOW =   10,
    PROTOCOL_ID_HIGH =  17,
    MESSAGE_TYPE_LOW =  8,
    MESSAGE_TYPE_HIGH = 9,
    MESSAGE_ID_LOW =    0,
    MESSAGE_ID_HIGH =   7
};

#define SCMI_EXRACT_BITS(data, bit_upper, bit_lower)\
        ({\
            uint32_t mask, bits;\
            mask = ((1 << (bit_upper - bit_lower + 1)) - 1) << bit_lower;\
            bits = (data & mask) >> bit_lower;\
            bits;\
        })

uint32_t agent_get_accessible_device(uint32_t agent_id);
uint32_t agent_get_inaccessible_device(uint32_t agent_id);
uint32_t device_get_accessible_protocol(uint32_t device_id);
uint32_t check_trusted_agent(uint32_t agent_id);
uint32_t pinctrl_check_access(uint32_t agent_id, uint32_t identifier, uint32_t selector);

void base_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void system_power_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void sensor_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void clock_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void performance_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void power_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void reset_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void voltage_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void powercap_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void pin_control_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values);

void fill_base_protocol(void);
void fill_power_protocol(void);
void fill_system_power_protocol(void);
void fill_clock_protocol(void);
void fill_sensor_protocol(void);
void fill_performance_protocol(void);
void fill_reset_protocol(void);
void fill_voltage_protocol(void);
void fill_powercap_protocol(void);
void fill_pin_control_protocol(void);

#endif /*__PAL_PLATFORM__*/
