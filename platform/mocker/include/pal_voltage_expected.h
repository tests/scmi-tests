/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef __PAL_VOLTAGE_EXPECTED_H__
#define __PAL_VOLTAGE_EXPECTED_H__

#include <pal_platform.h>

static uint32_t num_voltage_domains = 0x02;

static char *voltage_domain_names[] = {
    "USB",
    "EXT_ENHANCEMENT_VOLTAGE_1"
};

static uint32_t *voltage_describe_levels[] = {
    (uint32_t[]) {15, 30, 40, 90},  /* Voltage Domain 0 */
    (uint32_t[]) {10, 40, 60, 110} /* Voltage Domain 1 */
};

static uint32_t operational_level[] = {40, 60};

static uint32_t supported_voltage_protocols[] = {
    SUPPORTED_VOLTAGE_VERSION_1,
    SUPPORTED_VOLTAGE_VERSION_2,
    SUPPORTED_VOLTAGE_VERSION_2P1,
};

#endif /* __PAL_VOLTAGE_EXPECTED_H__ */
