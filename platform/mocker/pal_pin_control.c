/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include <pal_interface.h>
#include <pal_platform.h>
#include <pal_pin_control_expected.h>

/**
  @brief   This API is used for checking num of pins
  @param   none
  @return  num of pins
**/
uint32_t pal_pin_control_get_expected_num_pins(void)
{
    return num_pins;
}

/**
  @brief   This API is used for checking num of groups
  @param   none
  @return  num of groups
**/
uint32_t pal_pin_control_get_expected_num_groups(void)
{
    return num_groups;
}

/**
  @brief   This API is used for checking num of functions
  @param   none
  @return  num of functions
**/
uint32_t pal_pin_control_get_expected_num_functions(void)
{
    return num_functions;
}

/**
@brief   This API is used to check if pin or group is accessible for given agent id
           1. Caller       -  Test Suite.
  @param   agent_id     agent id
  @param   identifier   The identifier of pin or group
  @param   selector     Selector of pin or group
  @return  status       0 on access
**/
uint32_t pal_pinctrl_check_access(uint32_t agent_id,uint32_t identifier, uint32_t selector)

{
   return pinctrl_check_access(agent_id, identifier, selector);
}

/**
  @brief   This API is used for checking num of pins in a group
  @param   identifier of the pin
  @return  num of pins in a group
**/
uint32_t pal_pin_control_get_expected_pins_in_group(uint32_t identifier)
{
    return num_pins_in_group[identifier];
}

/**
  @brief   This API is used for checking num of groups associated with the function
  @param   identifier of the function
  @return  num of groups in function
**/
uint32_t pal_pin_control_get_expected_groups_in_func(uint32_t identifier)
{
    return num_groups_in_function[identifier];
}

/**
  @brief   This API is used to get identifier of pin associated with the group
  @param   identifier of the group
  @return  pin identifier of group in index
**/
uint32_t pal_pin_control_get_expected_group_associations(uint32_t identifier, uint32_t index)
{
    return group_associations[identifier][index];
}

/**
  @brief   This API is used to get identifier of group associated with the function
  @param   identifier of the function
  @return  pin identifier of function in index
**/
uint32_t pal_pin_control_get_expected_function_associations(uint32_t identifier, uint32_t index)
{
    return function_associations[identifier][index];
}

/**
  @brief   This API is used to check if a pin, group or function have extended name support
  @param   identifier  idenfifier of pin, group or function
  @param   index
  @return  1 on support
**/
uint32_t pal_pin_control_get_extended_name_support(uint32_t identifier, uint32_t index)
{
    return pin_control_extended_name_support[identifier][index];
}

/**
  @brief   This API is used to get number of supported protocol versions
  @param   none
  @return  num of protocol versions supported
**/

uint32_t pal_pin_control_get_expected_supported_num_protocols(void)
{
    return NUM_ELEMS(supported_pin_control_protocols);
}

/**
  @brief   This API is used to check if pin is enabled for a function
  @param   identifier of the pin
  @return  identifier of functoin that is enabled by pin
**/
uint32_t pal_pin_control_get_pins_enabled_function(uint32_t identifier)
{
    return pins_enabled_function[identifier];
}

/**
  @brief   This API is used to check if group is enabled for a function
  @param   identifier of the group
  @return  identifier of functoin that is enabled by group
**/
uint32_t pal_pin_control_get_groups_enabled_function(uint32_t identifier)
{
    return groups_enabled_function[identifier];
}
