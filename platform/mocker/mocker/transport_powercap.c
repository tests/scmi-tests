/** @file
 * Copyright (c) 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/
#include <powercap_common.h>
#include <powercap_protocol.h>
#include <pal_powercap_expected.h>

struct arm_scmi_powercap_protocol powercap_protocol;

void fill_powercap_protocol()
{
    powercap_protocol.protocol_version = POWERCAP_VERSION;
    powercap_protocol.number_domains = num_powercap_domain;
    powercap_protocol.powercap_domain_name = powercap_domain_name;
    powercap_protocol.min_pai = min_pai;
    powercap_protocol.max_pai = max_pai;
    powercap_protocol.pai_step = pai_step;
    powercap_protocol.min_power_cap = min_power_cap;
    powercap_protocol.max_power_cap = max_power_cap;
    powercap_protocol.power_cap_step = power_cap_step;
    powercap_protocol.sustainable_power = sustainable_power;
    powercap_protocol.accuracy = accuracy;
    powercap_protocol.pai = pai;
    powercap_protocol.power_cap = power_cap;
    powercap_protocol.avg_power = avg_power;
    powercap_protocol.powercap_extended_name_support = powercap_extended_name_support;
}


void powercap_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values)
{
      uint32_t return_idx, parameter_idx, domain_id;
      char *name;
      uint32_t i;

      switch(message_id)
      {
      case POWERCAP_PROTO_VER_MSG_ID:
          *status = SCMI_STATUS_SUCCESS;
          *return_values_count = 1;
          return_values[0] = powercap_protocol.protocol_version;
          break;

      case POWERCAP_PROTO_ATTR_MSG_ID:
          *status = SCMI_STATUS_SUCCESS;
          *return_values_count = 1;
          return_idx = OFFSET_RET(struct arm_scmi_powercap_protocol_attributes, attributes);
          return_values[return_idx] = powercap_protocol.number_domains;
          break;

      case POWERCAP_PROTO_MSG_ATTR_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_protocol_message_attributes,
                                       message_id);

          if (parameters[parameter_idx] == POWERCAP_CAP_NOTIFY_MSG_ID ||
              parameters[parameter_idx] == POWERCAP_MEASURMENTS_NOTIFY_MSG_ID) {
              *status = SCMI_STATUS_SUCCESS;
              break;
          }

          if (parameters[parameter_idx] > POWERCAP_NEGOTIATE_PROTOCOL_MSG_ID)
              *status = SCMI_STATUS_NOT_FOUND;
          else {
              *status = SCMI_STATUS_SUCCESS;
              *return_values_count = 1;
              return_idx = OFFSET_RET(struct arm_scmi_powercap_protocol_message_attributes,
                                      attributes);
              return_values[return_idx] = 0;
          }
          break;

      case POWERCAP_DOMAIN_ATTR_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_domain_attributes, domain_id);
          domain_id = parameters[parameter_idx];
          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          name = powercap_protocol.powercap_domain_name[domain_id];
          *status = SCMI_STATUS_SUCCESS;
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, attributes);
          return_values[return_idx] = 0x1 << POWERCAP_DOMAIN_UNIT_LOW |
                                      0x1 << POWERCAP_DOMAIN_PAI_CONFIG_SUPPORT_LOW |
                                      0x1 << POWERCAP_DOMAIN_MONITOR_SUPPORT_LOW |
                                      0x1 << POWERCAP_DOMAIN_CONFIG_SUPPORT_LOW |
                                      powercap_protocol.powercap_extended_name_support[domain_id]
                                      << POWERCAP_DOMAIN_EXT_NAME_SUPPORT_LOW |
                                      0x1 << POWERCAP_DOMAIN_MEASURE_NOTIFY_SUPPORT_LOW |
                                      0x1 << POWERCAP_DOMAIN_CHANGE_NOTIFY_SUPPORT_LOW;

          if (powercap_protocol.powercap_extended_name_support[domain_id])
              snprintf((char*)&return_values[1], MSG_SIZE, "%s", name + strnlen(name, 25) - 16);
          else
              snprintf((char *) &return_values[1], MSG_SIZE, "%s", name);

          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, min_pai);
          return_values[return_idx] = powercap_protocol.min_pai[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, max_pai);
          return_values[return_idx] = powercap_protocol.max_pai[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, pai_step);
          return_values[return_idx] = powercap_protocol.pai_step[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, min_power_cap);
          return_values[return_idx] = powercap_protocol.min_power_cap[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, max_power_cap);
          return_values[return_idx] = powercap_protocol.max_power_cap[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, power_cap_step);
          return_values[return_idx] = powercap_protocol.power_cap_step[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, sustainable_power);
          return_values[return_idx] = powercap_protocol.sustainable_power[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, accuracy);
          return_values[return_idx] = powercap_protocol.accuracy[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_attributes, parent_id);
          return_values[return_idx] = 0xFFFFFFFF;
          *return_values_count = strnlen((const char*)return_values, 25) + 10;
          break;

      case POWERCAP_CAP_GET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_cap_get, domain_id);
          domain_id = parameters[parameter_idx];
          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          *status = SCMI_STATUS_SUCCESS;
          return_idx = OFFSET_RET(struct arm_scmi_powercap_cap_get, power_cap);
          return_values[return_idx] = powercap_protocol.power_cap[domain_id];
          *return_values_count = 1;
          break;

      case POWERCAP_CAP_SET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_cap_set, domain_id);
          domain_id = parameters[parameter_idx];
          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_cap_set, flags);
          if (parameters[parameter_idx] > 3) {
              *status = SCMI_STATUS_INVALID_PARAMETERS;
              break;
          }

          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_cap_set, power_cap);
          if (parameters[parameter_idx] == 0) {
              *status = SCMI_STATUS_INVALID_PARAMETERS;
              break;
          }

          *status = SCMI_STATUS_SUCCESS;
          break;

      case POWERCAP_PAI_GET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_pai_get, domain_id);
          domain_id = parameters[parameter_idx];
          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          *status = SCMI_STATUS_SUCCESS;
          return_idx = OFFSET_RET(struct arm_scmi_powercap_pai_get, pai);
          return_values[return_idx] = powercap_protocol.pai[domain_id];
          *return_values_count = 1;
          break;

      case POWERCAP_PAI_SET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_pai_set, domain_id);
          domain_id = parameters[parameter_idx];
          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_pai_set, flags);
          if (parameters[parameter_idx] != 0) {
              *status = SCMI_STATUS_INVALID_PARAMETERS;
              break;
          }

          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_pai_set, pai);
          if (parameters[parameter_idx] == 0) {
              *status = SCMI_STATUS_INVALID_PARAMETERS;
              break;
          }
          *status = SCMI_STATUS_SUCCESS;
          break;

      case POWERCAP_MEASUREMENTS_GET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_measurements_get, domain_id);
          domain_id = parameters[parameter_idx];
          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          return_idx = OFFSET_RET(struct arm_scmi_powercap_measurements_get, power);
          return_values[return_idx] = powercap_protocol.avg_power[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_powercap_measurements_get, pai);
          return_values[return_idx] = powercap_protocol.pai[domain_id];
          *status = SCMI_STATUS_SUCCESS;
          *return_values_count = 2;
          break;

      case POWERCAP_DOMAIN_NAME_GET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_domain_name_get, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          return_idx = OFFSET_RET(struct arm_scmi_powercap_domain_name_get, flags);
          return_values[return_idx] = 0;
          if (powercap_protocol.powercap_extended_name_support[domain_id]) {
              *status = SCMI_STATUS_SUCCESS;
              name = powercap_protocol.powercap_domain_name[domain_id];
              snprintf((char *)&return_values[1], MSG_SIZE, "%s", name);
          } else {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          *return_values_count = strnlen((const char*)return_values, 25) + 1;
          break;

      case POWERCAP_CAP_NOTIFY_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_cap_notify, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          *status = SCMI_STATUS_SUCCESS;
          break;

      case POWERCAP_MEASURMENTS_NOTIFY_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_powercap_measurements_notify, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= powercap_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          *status = SCMI_STATUS_SUCCESS;
          break;

    case POWERCAP_NEGOTIATE_PROTOCOL_MSG_ID:
        *status = SCMI_STATUS_NOT_SUPPORTED;
        if (powercap_protocol.protocol_version < 0x00020000) {
            *status = SCMI_STATUS_NOT_SUPPORTED;
            break;
        }
        return_idx = OFFSET_PARAM(struct arm_scmi_powercap_negotiate_protocol, version);
        for (i = NUM_ELEMS(supported_powercap_protocols); i > 0; i--) {
            if (supported_powercap_protocols[i - 1] == parameters[return_idx]) {
                powercap_protocol.protocol_version = parameters[return_idx];
                *status = SCMI_STATUS_SUCCESS;
                break;
            }
        }
        break;

      default:
          *status = SCMI_STATUS_NOT_FOUND;;
          break;
      }
}
