/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef VOLTAGE_COMMON_H_
#define VOLTAGE_COMMON_H_

#include <protocol_common.h>

#define SCMI_MAX_DESCRIBE_LEVELS 4

/* Structure for voltage protocol version*/
struct arm_scmi_voltage_protocol_version {
    struct  {
        uint32_t version;
    } returns;
};

struct arm_scmi_voltage_protocol_attributes {
    struct {
        uint32_t attributes;
    } returns;

    enum {
        VOLTAGE_ATTRIBUTES_LOW = 0,
        VOLTAGE_ATTRIBUTES_HIGH = 15,
        VOLTAGE_RESERVED_LOW = 16,
        VOLTAGE_RESERVED_HIGH = 31
    } attributes_bits;
};

struct arm_scmi_voltage_protocol_message_attributes {
    struct {
        uint32_t message_id;
    } parameters;
    struct {
        uint32_t attributes;
    } returns;
};


struct arm_scmi_voltage_domain_attributes {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t attributes;
        uint8_t name[16];
    } returns;

    enum {
        VOLTAGE_DOMAIN_RESERVED_LOW = 0,
        VOLTAGE_DOMAIN_RESERVED_HIGH = 31,
    } domain_attributes_bits;

};

struct arm_scmi_voltage_domain_attributes_v2 {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t attributes;
        uint8_t name[16];
    } returns;

    enum {
        VOLTAGE_DOMAIN_RESERVED_LOW_V2 = 0,
        VOLTAGE_DOMAIN_RESERVED_HIGH_V2 = 29,
        VOLTAGE_EXTENDED_DOMAIN_NAME_LOW_V2 = 30,
        VOLTAGE_EXTENDED_DOMAIN_NAME_HIGH_V2 = 30,
        VOLTAGE_DOMAIN_LEVEL_SET_LOW_V2 = 31,
        VOLTAGE_DOMAIN_LEVEL_SET_HIGH_V2 = 31
    } domain_attributes_bits_v2;
};

struct arm_scmi_voltage_describe_levels {
    struct {
        uint32_t domain_id;
        uint32_t level_index;
    } parameters;
    struct {
        uint32_t flags;
        int32_t  voltage[SCMI_MAX_DESCRIBE_LEVELS];
    } returns;

    enum {
        VOLTAGE_DESCRIBE_LEVELS_LOW = 0,
        VOLTAGE_DESCRIBE_LEVELS_HIGH = 11,
        VOLTAGE_DESCRIBE_RETURN_FORMAT_LOW = 12,
        VOLTAGE_DESCRIBE_RETURN_FORMAT_HIGH = 12,
        VOLTAGE_DESCRIBE_RESERVED_LOW = 13,
        VOLTAGE_DESCRIBE_RESERVED_HIGH = 15,
        VOLTAGE_DESCRIBE_REMAINING_LEVEL_LOW = 16,
        VOLTAGE_DESCRIBE_REMAINING_LEVEL_HIGH = 31
    } describe_flags_bits;
};

struct arm_scmi_voltage_config_set {
    struct {
        uint32_t domain_id;
        uint32_t config;
    } parameters;

    enum {
        VOLTAGE_CONFIG_SET_MODE_LOW = 0,
        VOLTAGE_CONFIG_SET_MODE_HIGH = 3,
        VOLTAGE_CONFIG_SET_RESERVED_LOW = 4,
        VOLTAGE_CONFIG_SET_RESERVED_HIGH = 31,
    } voltage_set_config_bits;

};

struct arm_scmi_voltage_config_get {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t config;
    } returns;

    enum {
        VOLTAGE_CONFIG_GET_MODE_LOW = 0,
        VOLTAGE_CONFIG_GET_MODE_HIGH = 3,
        VOLTAGE_CONFIG_GET_RESERVED_LOW = 4,
        VOLTAGE_CONFIG_GET_RESERVED_HIGH = 31,
    } voltage_get_config_bits;

};

struct arm_scmi_voltage_level_get {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        int32_t voltage_level;
    } returns;
};

struct arm_scmi_voltage_level_set {
    struct {
        uint32_t domain_id;
        uint32_t flags;
        int32_t voltage_level;
    } parameters;

    enum {
        VOLTAGE_LEVEL_SET_OPERATION_LOW = 0,
        VOLTAGE_LEVEL_SET_OPERATION_HIGH = 0,
        VOLTAGE_LEVEL_SET_RESERVED_LOW = 1,
        VOLTAGE_LEVEL_SET_RESERVED_HIGH = 31
    } level_flags_bits;
};

struct arm_scmi_voltage_domain_name_get {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t flags;
        uint8_t name[64];
    } returns;

    enum {
        VOLTAGE_DOMAIN_NAME_GET_RESERVED_LOW = 0,
        VOLTAGE_DOMAIN_NAME_GET_RESERVED_HIGH = 31
    } name_flags_bits;
};

struct arm_scmi_voltage_negotiate_protocol {
    struct  {
        uint32_t version;
    } parameters;
};
#endif /* VOLTAGE_COMMON_H_ */
