/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef VOLTAGE_PROTOCOL_H_
#define VOLTAGE_PROTOCOL_H_

#include <inttypes.h>
#include <stdbool.h>

#ifdef SCMI_VERSION_3
#define VOLTAGE_VERSION 0x00010000
#elif SCMI_VERSION_3P1
#define VOLTAGE_VERSION 0x00020000
#else
#define VOLTAGE_VERSION 0x00020001
#endif

#define VOLTAGE_PROTO_ID                   0x17
#define VOLTAGE_PROTO_VER_MSG_ID           0x0
#define VOLTAGE_PROTO_ATTR_MSG_ID          0x1
#define VOLTAGE_PROTO_MSG_ATTR_MSG_ID      0x2
#define VOLTAGE_DOMAIN_ATTR_MSG_ID         0x3
#define VOLTAGE_DESCRIBE_LEVELS_MSG_ID     0x4
#define VOLTAGE_CONFIG_SET_MSG_ID          0x5
#define VOLTAGE_CONFIG_GET_MSG_ID          0x6
#define VOLTAGE_LEVEL_SET_MSG_ID           0x7
#define VOLTAGE_LEVEL_GET_MSG_ID           0x8
#define VOLTAGE_DOMAIN_NAME_GET_MSG_ID     0x9
#define VOLTAGE_NEGOTIATE_PROTOCOL_MSG_ID  0x10

struct arm_scmi_voltage_protocol {
  /*
   * This enum is to specify the flag_mask and have to be ensured that
   *  it is in the same order as the below structure elements after
   *  flags_mask member.
   */
  enum {
      VOLTAGE_DOMAIN_PROTOCOL_VERSION = 1,
      VOLTAGE_NUMBER_DOMAINS,
      VOLTAGE_DOMAIN_NAME,
      VOLTAGE_DESCRIBE_LEVELS,
      VOLTAGE_OPERATIONAL_LEVEL
  } FLAGS;
  /*
   * The expectation is that there is no more
   * than 32 elements specified via this expected results data structure.
   * This mask is used to indicate which of the structure members hold valid
   * data.
   */
  uint32_t flags_mask;
  uint32_t protocol_version;

  uint32_t number_domains;

  int32_t voltage_level;

  int32_t invalid_voltage_level;

  uint32_t **voltage_describe_levels;
  uint32_t *operational_level;
  /*
   * This should have the names of the domains in this platform bounded by the
   * number of domains descriptors.
   */
  char  **voltage_domain_name;
};

#endif /* VOLTAGE_PROTOCOL_H_ */
