/** @file
 * Copyright (c) 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef POWERCAP_PROTOCOL_H_
#define POWERCAP_PROTOCOL_H_

#include <inttypes.h>
#include <stdbool.h>

#ifdef SCMI_VERSION_3P1
#define POWERCAP_VERSION 0x00010000
#else
#define POWERCAP_VERSION 0x00020000
#endif

#define POWERCAP_PROTO_ID                   0x18
#define POWERCAP_PROTO_VER_MSG_ID           0x0
#define POWERCAP_PROTO_ATTR_MSG_ID          0x1
#define POWERCAP_PROTO_MSG_ATTR_MSG_ID      0x2
#define POWERCAP_DOMAIN_ATTR_MSG_ID         0x3
#define POWERCAP_CAP_GET_MSG_ID             0x4
#define POWERCAP_CAP_SET_MSG_ID             0x5
#define POWERCAP_PAI_GET_MSG_ID             0x6
#define POWERCAP_PAI_SET_MSG_ID             0x7
#define POWERCAP_DOMAIN_NAME_GET_MSG_ID     0x8
#define POWERCAP_MEASUREMENTS_GET_MSG_ID    0x9
#define POWERCAP_CAP_NOTIFY_MSG_ID          0xA
#define POWERCAP_MEASURMENTS_NOTIFY_MSG_ID  0xB
#define POWERCAP_NEGOTIATE_PROTOCOL_MSG_ID  0x10

struct arm_scmi_powercap_protocol {
    /*
     * This enum is to specify the flag_mask and have to be ensured that
     *  it is in the same order as the below structure elements after
     *  flags_mask member.
     */
    enum {
        POWERCAP_DOMAIN_PROTOCOL_VERSION = 1,
        POWERCAP_NUMBER_DOMAINS,
        POWERCAP_DOMAIN_NAME,
        POWERCAP_MIN_PAI,
        POWERCAP_MAX_PAI,
        POWERCAP_PAI_STEP,
        POWERCAP_MIN_POWER_CAP,
        POWERCAP_MAX_POWER_CAP,
        POWERCAP_POWER_CAP_STEP,
        POWERCAP_SUSTAINABLE_POWER,
        POWERCAP_ACCURACY,
        POWERCAP_PAI,
        POWERCAP_POWER_CAP,
        POWERCAP_AVERAGE_POWER,
        POWERCAP_EXTENDED_NAME_SUPPORT
    } FLAGS;
    /*
     * The expectation is that there is no more
     * than 32 elements specified via this expected results data structure.
     * This mask is used to indicate which of the structure members hold valid
     * data.
     */
    uint32_t flags_mask;
    uint32_t protocol_version;

    uint32_t number_domains;

    int32_t powercap_level;

    uint32_t *min_pai;
    uint32_t *max_pai;
    uint32_t *pai_step;
    uint32_t *min_power_cap;
    uint32_t *max_power_cap;
    uint32_t *power_cap_step;
    uint32_t *sustainable_power;
    uint32_t *accuracy;
    uint32_t *pai;
    uint32_t *power_cap;
    uint32_t *avg_power;
    uint32_t *powercap_extended_name_support;

    int32_t invalid_powercap_level;
    /*
     * This should have the names of the domains in this platform bounded by the
     * number of domains descriptors.
     */
    char  **powercap_domain_name;
};
#endif /* POWERCAP_PROTOCOL_H_ */
