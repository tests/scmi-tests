/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef PIN_CONTROL_PROTOCOL_H_
#define PIN_CONTROL_PROTOCOL_H_

#include <inttypes.h>
#include <stdbool.h>

#define PINCTRL_VERSOIN 0x00010000
#define RESERVED        0
#define PINCTRL_PROTO_ID                0x19
#define FLAG_PINS                       0
#define FLAG_GROUPS                     1
#define FLAG_FUNCTIONS                  2

#define MAX_NUM_PIN                     4
#define MAX_NUM_GROUP                   2

#define PINCTRL_PROTO_VER_MSG_ID        0x0
#define PINCTRL_PROTO_ATTRIB_MSG_ID     0x1
#define PINCTRL_PROTO_MSG_ATTRIB_MSG_ID 0x2
#define PINCTRL_ATTRIB_MSG_ID           0x3
#define PINCTRL_LIST_ASSOC_MSG_ID       0x4
#define PINCTRL_SETTINGS_GET_MSG_ID     0x5
#define PINCTRL_SETTINGS_CONFIGURE_MSG_ID 0x6
#define PINCTRL_FUNC_SELECT_MSG_ID      0x7
#define PINCTRL_REQUEST_MSG_ID          0x8
#define PINCTRL_RELEASE_MSG_ID          0x9
#define PINCTRL_NAME_GET_MSG_ID         0xA
#define PINCTRL_SET_PERM_MSG_ID         0xB
#define PINCTRL_NEGOTIATE_PROTOCOL_MSG_ID 0x10

struct arm_scmi_pin_control_config_default {
    uint32_t type;
    uint32_t value;
};

struct pinctrl_access_info
{
    char *name;
    bool access;
};

struct arm_scmi_pin_control_protocol {
    /*
     * This enum is to specify the flag_mask and have to be ensured that
     *  it is in the same order as the below structure elements after
     *num_pins  flags_mask member.
     */
    enum {
        PIN_CONTROL_PROTOCOL_VERSION,
        PIN_CONTROL_NUMBER_GROUPS,
        PIN_CONTROL_NUMBER_PINS,
        PIN_CONTROL_NUMBER_FUNCTIONS,
        PIN_CONTROL_NUMBER_PINS_IN_GROUP,
        PIN_CONTROL_NUMBER_GROUPS_IN_FUNCTIONS,
        PIN_CONTROL_PIN_NAME,
        PIN_CONTROL_GROUP_NAME,
        PIN_CONTROL_FUNCTION_NAME,
        PIN_CONTROL_GROUP_ASSOCIATIONS,
        PIN_CONTROL_FUNCTION_ASSOCIATIONS,
        PIN_CONTROL_EXTENDED_NAME_SUPPORT
    } FLAGS;
    /*
     * The expectation is that there is no more
     * than 32 elements specified via this expected results data structure.
     * This mask is used to indicate which of the structure members hold valid
     * data.
     */
    uint32_t flags_mask;
    uint32_t protocol_version;

    /* number of pin groups */
    uint32_t number_groups;
    /* number of pins */
    uint32_t number_pins;
    /* number of functions */
    uint32_t number_functions;

    /* number of pins in the group*/
    uint16_t *number_pins_in_group;
    /* number of groups associated with the function*/
    uint16_t *number_groups_in_function;
    /*
     * This should have the names of the domains in this platform bounded by the
     * number of domains descriptors.
     */
    char **pin_name;
    char **group_name;
    char **function_name;
    /* Array of group or pin identifiers*/
    uint32_t **group_associations;
    uint32_t **function_associations;
    uint32_t **pin_control_extended_name_support;
};

#endif /* PIN_CONTROL_PROTOCOL_H_ */
