/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef PIN_CONTROL_COMMON_H_
#define PIN_CONTROL_COMMON_H_

#include <protocol_common.h>

#define PINCTRL_CONFIG_ENTRY 2
#define SCMI_PIN_GROUP_ARRAY_SIZE 5

struct arm_scmi_pin_control_protocol_version {
    struct {
        uint32_t version;
    } returns;
};

struct arm_scmi_pin_control_protocol_attributes {
    struct {
        uint32_t attributes_low;
        uint32_t attributes_high;
    } returns;
    /* enums for attributes_low */
    enum {
        PINCTRL_NUM_PINS_LOW = 0,
        PINCTRL_NUM_PINS_HIGH = 15,
        PINCTRL_NUM_PIN_GROUPS_LOW = 16,
        PINCTRL_NUM_PIN_GROUPS_HIGH = 31
    } attributes_low_bits;
    /* enums for attributes_high */
    enum {
        PINCTRL_NUM_FUNCTIONS_LOW = 0,
        PINCTRL_NUM_FUNCTIONS_HIGH = 15,
        PINCTRL_NUM_RESERVED_LOW = 16,
        PINCTRL_NUM_RESERVED_HIGH = 31
    } attributes_high_bits;
};

struct arm_scmi_pin_control_protocol_message_attributes {
    struct {
        uint32_t message_id;
    } parameters;
    struct {
        uint32_t attributes;
    } returns;
};

struct arm_scmi_pin_control_attributes {
    struct {
        uint32_t identifier;
        uint32_t flags;
    } parameters;
    struct {
        uint32_t attributes;
        uint8_t name[SCMI_NAME_STR_SIZE];
    } returns;
    /* enums for flags */
    enum {
        PINCTRL_FLAGS_SELECTOR_LOW = 0,
        PINCRTL_FLAGS_SELECTOR_HIGH = 1,
        PINCTRL_FLAGS_RESERVED_LOW = 2,
        PINCTRL_FLAGS_RESERVED_HIGH = 31
    } flags_bits;
/* enums for attributes */
    enum {
        PINCTRL_ATTRIBUTES_NUM_PINS_GROUPS_LOW = 0,
        PINCTRL_ATTRIBUTES_NUM_PINS_GROUPS_HIGH = 15,
        PINCTRL_ATTRIBUTES_FUNC_GROUP_SUPPORT_LOW = 16,
        PINCTRL_ATTRIBUTES_FUNC_GROUP_SUPPORT_HIGH = 16,
        PINCTRL_ATTRIBUTES_FUNC_GPIO_SUPPORT_LOW = 17,
        PINCTRL_ATTRIBUTES_FUNC_GPIO_SUPPORT_HIGH = 17,
        PINCTRL_ATTRIBUTES_RESERVED_LOW = 18,
        PINCTRL_ATTRIBUTES_RESERVED_HIGH = 30,
        PINCTRL_ATTRIBUTES_EXTENDED_NAME_LOW = 31,
        PINCTRL_ATTRIBUTES_EXTENDED_NAME_HIGH = 31
    } attributes;
};

struct arm_scmi_pin_control_list_associations {
    struct {
        uint32_t identifier;
        uint32_t list_parameters_flags;
        uint32_t index;
    } parameters;
    struct {
        uint32_t list_returns_flags;
        uint16_t array[SCMI_PIN_GROUP_ARRAY_SIZE];
    } returns;
    /* enums for parameters flags */
    enum {
        PINCTRL_LIST_PARAMS_FLAGS_SELECTOR_LOW = 0,
        PINCTRL_LIST_PARAMS_FLAGS_SELECTOR_HIGH = 1,
        PINCTRL_LIST_PARAMS_FLAGS_RESERVED_LOW = 2,
        PINCTRL_LIST_PARAMS_FLAGS_RESERVED_HIGH = 31
    } list_parameters_flags_bits;
    /* enums for returns flags */
    enum {
        PINCTRL_LIST_RETURNS_FLAGS_NUM_IDENTIFIERS_LOW = 0,
        PINCTRL_LIST_RETURNS_FLAGS_NUM_IDENTIFIERS_HIGH = 11,
        PINCTRL_LIST_RETURNS_FLAGS_RESERVED_LOW = 12,
        PINCTRL_LIST_RETURNS_FLAGS_RESERVED_HIGH = 15,
        PINCTRL_LIST_RETURNS_FLAGS_NUM_REMAINING_LOW = 16,
        PINCTRL_LIST_RETURNS_FLAGS_NUM_REMAINING_HIGH = 31
    } list_returns_flags_bits;
};

struct arm_scmi_pin_control_settings_get {
    struct {
        uint32_t identifier;
        uint32_t attributes;
    } parameters;
    struct {
        uint32_t function_selected;
        uint32_t num_configs;
        uint32_t configs[PINCTRL_CONFIG_ENTRY];
    } returns;
    /* enum for attributes */
    enum {
        PINCTRL_SETTINGS_GET_CONFIGTYPE_LOW = 0,
        PINCTRL_SETTINGS_GET_CONFIGTYPE_HIGH = 7,
        PINCTRL_SETTINGS_GET_SKIPCONFIGS_LOW = 8,
        PINCTRL_SETTINGS_GET_SKIPCONFIGS_HIGH = 15,
        PINCTRL_SETTINGS_GET_SELECTOR_LOW = 16,
        PINCTRL_SETTINGS_GET_SELECTOR_HIGH = 17,
        PINCTRL_SETTINGS_GET_ALLCONFIGS_LOW = 18,
        PINCTRL_SETTINGS_GET_ALLCONFIGS_HIGH = 18,
        PINCTRL_SETTINGS_GET_RESERVED_LOW = 19,
        PINCTRL_SETTINGS_GET_RESERVED_HIGH = 31
    } attributes_bits;
    /* enums for num_configs*/
    enum {
        PINCTRL_SETTINGS_GET_NUM_CONFIGS_LOW = 0,
        PINCTRL_SETTINGS_GET_NUM_CONFIGS_HIGH = 7,
        PINCTRL_SETTINGS_GET_NUM_CONFIGS_RESERVED_LOW = 8,
        PINCTRL_SETTINGS_GET_NUM_CONFIGS_RESERVED_HIGH = 23,
        PINCTRL_SETTINGS_GET_NUM_REMAINING_CONFIGS_LOW = 24,
        PINCTRL_SETTINGS_GET_NUM_REMAINING_CONFIGS_HIGH = 31
    } num_configs_bits;
    /* enums for configs entry 0 */
    enum {
        PINCTRL_SETTINGS_GET_CONFIG_TYPE_LOW = 0,
        PINCTRL_SETTINGS_GET_CONFIG_TYPE_HIGH = 7,
        PINCTRL_SETTINGS_GET_ENTRY_RESERVED_LOW = 8,
        PINCTRL_SETTINGS_GET_ENTRY_RESERVED_HIGH = 31
    } get_configs_entry_0_bits;
};

struct arm_scmi_pin_control_settings_configure {
    struct {
        uint32_t identifier;
        uint32_t function_id;
        uint32_t attributes;
        uint32_t configs[PINCTRL_CONFIG_ENTRY];
    } parameters;
    /* enums for attributes */
    enum {
        PINCTRL_SETTINGS_CONFIGURE_SELECTOR_LOW = 0,
        PINCTRL_SETTINGS_CONFIGURE_SELECTOR_HIGH = 1,
        PINCTRL_SETTINGS_CONFIGURE_NUM_CONFIGS_LOW = 2,
        PINCTRL_SETTINGS_CONFIGURE_NUM_CONFIGS_HIGH = 9,
        PINCTRL_SETTINGS_CONFIGURE_FUNC_ID_LOW = 10,
        PINCTRL_SETTINGS_CONFIGURE_FUNC_ID_HIGH = 10,
        PINCTRL_SETTINGS_CONFIGURE_RESERVED_LOW = 11,
        PINCTRL_SETTINGS_CONFIGURE_RESERVED_HIGH = 31
    } set_attributes_bits;
    /* enums for configs entry 0 */
    enum {
        PINCTRL_SETTINGS_CONFIGURE_CONFIG_TYPE_LOW = 0,
        PINCTRL_SETTINGS_CONFIGURE_CONFIG_TYPE_HIGH = 7,
        PINCTRL_SETTINGS_CONFIGURE_ENTRY_RESERVED_LOW = 8,
        PINCTRL_SETTINGS_CONFIGURE_ENTRY_RESERVED_HIGH = 31
    } set_configs_entry_0_bits;
};

struct arm_scmi_pin_control_function_select {
    struct {
        uint32_t identifier;
        uint32_t function_id;
        uint32_t flags;
    } parameters;
    /* enums for flags */
    enum {
        PINCTRL_FUNCTION_SELECT_SELECTOR_LOW = 0,
        PINCTRL_FUNCTION_SELECT_SELECTOR_HIGH = 1,
        PINCTRL_FUNCTION_SELECT_RESERVED_LOW = 2,
        PINCTRL_FUNCTION_SELECT_RESERVED_HIGH = 31
    } function_select_flags_bits;
};

struct arm_scmi_pin_control_request {
    struct {
        uint32_t identifier;
        uint32_t flags;
    } parameters;
    /* enums for flags */
    enum {
        PINCTRL_REQUEST_SELECTOR_LOW = 0,
        PINCTRL_REQUEST_SELECTOR_HIGH = 1,
        PINCTRL_REQUEST_RESERVED_LOW = 2,
        PINCTRL_REQUEST_RESERVED_HIGH = 31
    } function_request_flags_bits;
};

struct arm_scmi_pin_control_release {
    struct {
        uint32_t identifier;
        uint32_t flags;
    } parameters;
    /* enums for flags */
    enum {
        PINCTRL_RELEASE_SELECTOR_LOW = 0,
        PINCTRL_RELEASE_SELECTOR_HIGH = 1,
        PINCTRL_RELEASE_RESERVED_LOW = 2,
        PINCTRL_RELEASE_RESERVED_HIGH = 31
    } function_release_flags_bits;
};

struct arm_scmi_pin_control_set_permissions {
    struct {
        uint32_t agent_id;
        uint32_t identifier;
        uint32_t flags;
    } parameters;
    /* enums for flags */
    enum {
        PINCTRL_SET_PERMISSIONS_SELECTOR_LOW = 0,
        PINCTRL_SET_PERMISSIONS_SELECTOR_HIGH = 1,
        PINCTRL_SET_PERMISSIONS_PERMISSION_LOW = 2,
        PINCTRL_SET_PERMISSIONS_PERMISSION_HIGH = 2,
        PINCTRL_SET_PERMISSIONS_RESERVED_LOW = 3,
        PINCTRL_SET_PERMISSIONS_RESERVED_HIGH = 31
    } set_permissions_flags_bits;
};

struct arm_scmi_pin_control_name_get {
    struct {
        uint32_t identifier;
        uint32_t flags;
    } parameters;
    struct {
        uint32_t flags;
        uint8_t name[64];
    } returns;
    /* enums for parameters flags */
    enum {
        PINCTRL_NAME_SELECTOR_LOW = 0,
        PINCTRL_NAME_SELECTOR_HIGH = 1,
        PINCTRL_NAME_RESERVED_LOW = 2,
        PINCTRL_NAME_RESERVED_HIGH = 31
    } name_flags_bits;

};

struct arm_scmi_pin_control_negotiate_protocol {
    struct  {
        uint32_t version;
    } parameters;
};
#endif /* PIN_CONTROL_COMMON_H_ */
