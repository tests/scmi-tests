/** @file
 * Copyright (c) 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifndef POWERCAP_COMMON_H_
#define POWERCAP_COMMON_H_

#include <protocol_common.h>

/* Structure for powercap protocol version*/
struct arm_scmi_powercap_protocol_version {
    struct  {
        uint32_t version;
    } returns;
};

struct arm_scmi_powercap_protocol_attributes {
    struct {
        uint32_t attributes;
    } returns;

    enum {
        POWERCAP_ATTRIBUTES_LOW = 0,
        POWERCAP_ATTRIBUTES_HIGH = 15,
        POWERCAP_RESERVED_LOW = 16,
        POWERCAP_RESERVED_HIGH = 31
    } attributes_bits;
};

struct arm_scmi_powercap_protocol_message_attributes {
    struct {
        uint32_t message_id;
    } parameters;
    struct {
        uint32_t attributes;
    } returns;

    enum {
        POWERCAP_FASTCHANNEL_SUPPORT_LOW = 0,
        POWERCAP_FASTCHANNEL_SUPPORT_HIGH = 0,
        POWERCAP_MSG_RESERVED_LOW = 1,
        POWERCAP_MSG_RESERVED_HIGH = 31
    } powercap_attributes_bits;
};

struct arm_scmi_powercap_domain_attributes {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t attributes;
        uint8_t name[16];
        uint32_t min_pai;
        uint32_t max_pai;
        uint32_t pai_step;
        uint32_t min_power_cap;
        uint32_t max_power_cap;
        uint32_t power_cap_step;
        uint32_t sustainable_power;
        uint32_t accuracy;
        uint32_t parent_id;
    } returns;

    enum {
        POWERCAP_DOMAIN_RESERVED_LOW = 0,
        POWERCAP_DOMAIN_RESERVED_HIGH = 21,
        POWERCAP_DOMAIN_FASTCHANNEL_LOW = 22,
        POWERCAP_DOMAIN_FASTCHANNEL_HIGH = 22,
        POWERCAP_DOMAIN_UNIT_LOW = 23,
        POWERCAP_DOMAIN_UNIT_HIGH = 24,
        POWERCAP_DOMAIN_PAI_CONFIG_SUPPORT_LOW = 25,
        POWERCAP_DOMAIN_PAI_CONFIG_SUPPORT_HIGH = 25,
        POWERCAP_DOMAIN_MONITOR_SUPPORT_LOW = 26,
        POWERCAP_DOMAIN_MONITOR_SUPPORT_HIGH = 26,
        POWERCAP_DOMAIN_CONFIG_SUPPORT_LOW = 27,
        POWERCAP_DOMAIN_CONFIG_SUPPORT_HIGH = 27,
        POWERCAP_DOMAIN_EXT_NAME_SUPPORT_LOW = 28,
        POWERCAP_DOMAIN_EXT_NAME_SUPPORT_HIGH = 28,
        POWERCAP_DOMAIN_ASYNC_CAP_SET_SUPPORT_LOW = 29,
        POWERCAP_DOMAIN_ASYNC_CAP_SET_SUPPORT_HIGH = 29,
        POWERCAP_DOMAIN_MEASURE_NOTIFY_SUPPORT_LOW = 30,
        POWERCAP_DOMAIN_MEASURE_NOTIFY_SUPPORT_HIGH = 30,
        POWERCAP_DOMAIN_CHANGE_NOTIFY_SUPPORT_LOW = 31,
        POWERCAP_DOMAIN_CHANGE_NOTIFY_SUPPORT_HIGH = 31,
    } domain_attributes_bits;
};

struct arm_scmi_powercap_cap_get {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t power_cap;
    } returns;
};

struct arm_scmi_powercap_cap_set {
    struct {
        uint32_t domain_id;
        uint32_t flags;
        uint32_t power_cap;
    } parameters;

    enum {
        POWERCAP_CAP_SET_DELAYED_RESPONSE_LOW = 0,
        POWERCAP_CAP_SET_DELAYED_RESPONSE_HIGH = 0,
        POWERCAP_CAP_SET_ASYNC_FLAG_LOW = 1,
        POWERCAP_CAP_SET_ASYNC_FLAG_HIGH = 1,
        POWERCAP_CAP_SET_RESERVED_LOW = 2,
        POWERCAP_CAP_SET_RESERVED_HIGH = 31,
    } cap_set_flags_bits;
};

struct arm_scmi_powercap_pai_get {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t pai;
    } returns;
};

struct arm_scmi_powercap_pai_set {
    struct {
        uint32_t domain_id;
        uint32_t flags;
        uint32_t pai;
    } parameters;
};

struct arm_scmi_powercap_measurements_get {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t power;
        uint32_t pai;
    } returns;
};

struct arm_scmi_powercap_domain_name_get {
    struct {
        uint32_t domain_id;
    } parameters;
    struct {
        uint32_t flags;
        uint8_t name[64];
    } returns;
};

struct arm_scmi_powercap_cap_notify {
    struct {
        uint32_t domain_id;
        uint32_t notify_enable;
    } parameters;
    enum {
        POWERCAP_CAP_NOTIFY_ENABLE_LOW = 0,
        POWERCAP_CAP_NOTIFY_ENABLE_HIGH = 0,
        POWERCAP_CAP_NOTIFY_RESERVED_LOW = 1,
        POWERCAP_CAP_NOTIFY_RESERVED_HIGH = 31
    } cap_notify_enable_bits;
};

struct arm_scmi_powercap_measurements_notify {
    struct {
        uint32_t domain_id;
        uint32_t notify_enable;
        uint32_t power_thresh_low;
        uint32_t power_thresh_high;
    } parameters;
    enum {
        POWERCAP_MEASURE_NOTIFY_ENABLE_LOW = 0,
        POWERCAP_MEASURE_NOTIFY_ENABLE_HIGH = 0,
        POWERCAP_MEASURE_RESERVED_LOW = 1,
        POWERCAP_MEASURE_RESERVED_HIGH = 31
    } measure_notify_enable_bits;
};

struct arm_scmi_powercap_negotiate_protocol {
    struct  {
        uint32_t version;
    } parameters;
};
#endif /* POWERCAP_COMMON_H_ */
