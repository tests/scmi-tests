/** @file
 * Copyright (c) 2021, 2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/
#include <voltage_common.h>
#include <voltage_protocol.h>
#include <pal_voltage_expected.h>

struct arm_scmi_voltage_protocol voltage_protocol;

void fill_voltage_protocol()
{
    voltage_protocol.protocol_version = VOLTAGE_VERSION;
    voltage_protocol.number_domains = num_voltage_domains;
    voltage_protocol.voltage_domain_name = voltage_domain_names;
    voltage_protocol.voltage_describe_levels = voltage_describe_levels;
    voltage_protocol.operational_level = operational_level;
}

void voltage_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values)
{
      uint32_t return_idx, parameter_idx, domain_id, total_levels = 4;
      uint32_t i;
      char *name;

      switch(message_id)
      {
      case VOLTAGE_PROTO_VER_MSG_ID:
          *status = SCMI_STATUS_SUCCESS;
          *return_values_count = 1;
          return_idx =  OFFSET_RET(struct arm_scmi_voltage_protocol_version, version);
          return_values[return_idx] = voltage_protocol.protocol_version;
          break;

      case VOLTAGE_PROTO_ATTR_MSG_ID:
          *status = SCMI_STATUS_SUCCESS;
          *return_values_count = 1;
          return_idx =  OFFSET_RET(struct arm_scmi_voltage_protocol_attributes, attributes);
          return_values[return_idx] = voltage_protocol.number_domains;
          break;

      case VOLTAGE_PROTO_MSG_ATTR_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_protocol_message_attributes,
                                       message_id);
          if(parameters[parameter_idx] > VOLTAGE_NEGOTIATE_PROTOCOL_MSG_ID) {
                *status = SCMI_STATUS_NOT_FOUND;
            } else {
                *status = SCMI_STATUS_SUCCESS;
                *return_values_count = 1;
                return_values[OFFSET_RET(
                struct arm_scmi_voltage_protocol_message_attributes, attributes)] = 0x0;
            }
          break;

      case VOLTAGE_DOMAIN_ATTR_MSG_ID:
          if (voltage_protocol.protocol_version == 0x00010000) {
              parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_domain_attributes, domain_id);
              domain_id = parameters[parameter_idx];

              if (domain_id >= voltage_protocol.number_domains) {
                  *status = SCMI_STATUS_NOT_FOUND;
                  break;
              }
              *status = SCMI_STATUS_SUCCESS;
              return_idx = OFFSET_RET(struct arm_scmi_voltage_domain_attributes, attributes);
              return_values[return_idx] = 0;
              snprintf((char *)&return_values[1], MSG_SIZE, "%s",
                     voltage_protocol.voltage_domain_name[domain_id]);
              *return_values_count = strnlen((const char*)return_values, 25) + 1;
              break;
          } else {
              parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_domain_attributes_v2, domain_id);
              domain_id = parameters[parameter_idx];

              if (domain_id >= voltage_protocol.number_domains) {
                  *status = SCMI_STATUS_NOT_FOUND;
                  break;
              }
              *status = SCMI_STATUS_SUCCESS;
              name = voltage_protocol.voltage_domain_name[domain_id];

              if (strnlen(name, 25) > 16) {
                  return_idx = OFFSET_RET(struct arm_scmi_voltage_domain_attributes_v2, attributes);
                  return_values[return_idx] = 1 << VOLTAGE_EXTENDED_DOMAIN_NAME_LOW_V2;
                  snprintf((char *)&return_values[1], MSG_SIZE, "%s", name + strnlen(name, 25) -16);
              } else {
                  return_idx = OFFSET_RET(struct arm_scmi_voltage_domain_attributes_v2, attributes);
                  return_values[return_idx] = 0;
                  snprintf((char *)&return_values[1], MSG_SIZE, "%s", name);
              }
              *return_values_count = strnlen((const char*)return_values, 25) + 1;
              break;
          }

      case VOLTAGE_DESCRIBE_LEVELS_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_describe_levels, domain_id);
          domain_id = parameters[parameter_idx];
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_describe_levels, level_index);

          if (domain_id >= voltage_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          if (parameters[parameter_idx] >= total_levels) {
              *status = SCMI_STATUS_OUT_OF_RANGE;
              break;
          }

          *status = SCMI_STATUS_SUCCESS;
          return_idx = OFFSET_RET(struct arm_scmi_voltage_describe_levels, flags);
          return_values[return_idx] = total_levels;
          for (i = 0; i < total_levels; i++) {
              return_idx = OFFSET_RET(struct arm_scmi_voltage_describe_levels, voltage) + i;
              return_values[return_idx] = voltage_protocol.voltage_describe_levels[domain_id][i];
          }
          *return_values_count = total_levels + 1;
          break;

      case VOLTAGE_CONFIG_SET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_config_set, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= voltage_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          *status = SCMI_STATUS_SUCCESS;
          break;

      case VOLTAGE_CONFIG_GET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_config_get, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= voltage_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

         *status = SCMI_STATUS_SUCCESS;
          return_idx = OFFSET_RET(struct arm_scmi_voltage_config_get, config);
          return_values[return_idx] = 0x7; /* Architecture mode and Voltage Supply on */
          *return_values_count = 1;
          break;

      case VOLTAGE_LEVEL_SET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_level_set, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= voltage_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_level_set, flags);
          if (parameters[parameter_idx] != 0) {
              *status = SCMI_STATUS_INVALID_PARAMETERS;
              break;
          }

          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_level_set, voltage_level);
          if ((parameters[parameter_idx] <
                          voltage_protocol.voltage_describe_levels[domain_id][0]) ||
              (parameters[parameter_idx] >
                          voltage_protocol.voltage_describe_levels[domain_id][total_levels-1]))
          {
              *status = SCMI_STATUS_INVALID_PARAMETERS;
              break;
          }

          *status = SCMI_STATUS_SUCCESS;
          voltage_protocol.operational_level[domain_id] = parameters[parameter_idx];
          break;

      case VOLTAGE_LEVEL_GET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_level_get, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= voltage_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          *status = SCMI_STATUS_SUCCESS;
          return_idx = OFFSET_RET(struct arm_scmi_voltage_level_get, voltage_level);
          return_values[return_idx] = voltage_protocol.operational_level[domain_id];
          *return_values_count = 1;
          break;

      case VOLTAGE_DOMAIN_NAME_GET_MSG_ID:
          parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_domain_name_get, domain_id);
          domain_id = parameters[parameter_idx];

          if (domain_id >= voltage_protocol.number_domains) {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }
          name = voltage_protocol.voltage_domain_name[domain_id];
          return_idx = OFFSET_RET(struct arm_scmi_voltage_domain_name_get, flags);
          return_values[return_idx] = 0;
          if (strnlen(name, 25) > 16) {
              *status = SCMI_STATUS_SUCCESS;
              snprintf((char *)&return_values[1], MSG_SIZE, "%s", name);
          } else {
              *status = SCMI_STATUS_NOT_FOUND;
              break;
          }

          *return_values_count = strnlen((const char*)return_values, 25) + 1;
          break;

    case VOLTAGE_NEGOTIATE_PROTOCOL_MSG_ID:
        *status = SCMI_STATUS_NOT_SUPPORTED;
        if (voltage_protocol.protocol_version < 0x00020001) {
            *status = SCMI_STATUS_NOT_SUPPORTED;
            break;
        }
        parameter_idx = OFFSET_PARAM(struct arm_scmi_voltage_negotiate_protocol, version);
        for (i = NUM_ELEMS(supported_voltage_protocols); i > 0; i--) {
            if (supported_voltage_protocols[i - 1] == parameters[parameter_idx]) {
                voltage_protocol.protocol_version = parameters[parameter_idx];
                *status = SCMI_STATUS_SUCCESS;
                break;
            }
        }
        break;

      default:
          *status = SCMI_STATUS_NOT_FOUND;
          break;
      }
}
