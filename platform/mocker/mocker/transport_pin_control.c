/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include <pin_control_protocol.h>
#include <pin_control_common.h>
#include <pal_pin_control_expected.h>

struct arm_scmi_pin_control_protocol pin_control_protocol;
struct arm_scmi_pin_control_config_default  pin_config[MAX_NUM_PIN];
struct arm_scmi_pin_control_config_default  group_config[MAX_NUM_GROUP];

void fill_pin_control_protocol()
{
    pin_control_protocol.protocol_version = PINCTRL_VERSOIN;
    pin_control_protocol.number_groups = num_groups;
    pin_control_protocol.number_pins = num_pins;
    pin_control_protocol.number_functions = num_functions;
    pin_control_protocol.number_pins_in_group = num_pins_in_group;
    pin_control_protocol.number_groups_in_function = num_groups_in_function;
    pin_control_protocol.group_associations = group_associations;
    pin_control_protocol.function_associations = function_associations;
    pin_control_protocol.pin_control_extended_name_support =
                                          pin_control_extended_name_support;
}
static struct pinctrl_access_info agent_pin_access[] =
{
/* pin access */
  {"PIN_0", true},
  {"PIN_1", true},
  {"PIN_2", true},
  {"PIN_3", true}
};

static struct pinctrl_access_info agent_group_access[] =
{
 /* group access */
  {"GROUP_0", true},
  {"GROUP_1", true}
};

uint32_t pinctrl_check_access(uint32_t agent_id, uint32_t identifier, uint32_t selector)
{
  if(selector == FLAG_PINS && agent_pin_access[identifier].access != true)
      return SCMI_STATUS_DENIED;
  else
      if(selector == FLAG_GROUPS && agent_group_access[identifier].access != true)
          return SCMI_STATUS_DENIED;

  return SCMI_STATUS_SUCCESS;
}

void pin_control_send_message(uint32_t message_id, uint32_t parameter_count,
        const uint32_t *parameters, int32_t *status,
        size_t *return_values_count, uint32_t *return_values)
{
    uint32_t return_idx = 0;
    uint32_t pc_id = 0, parameter_idx = 0;
    uint32_t flags = 0, function_id = 0;
    char * str;
    uint32_t i, total_num_configs, all_configs;
    uint32_t access;
    char name[64];

    switch(message_id)
    {
    case PINCTRL_PROTO_VER_MSG_ID:
        *status = SCMI_STATUS_SUCCESS;
        *return_values_count = 1;
        return_values[0] = pin_control_protocol.protocol_version;
        break;
    case PINCTRL_PROTO_ATTRIB_MSG_ID:
        *status = SCMI_STATUS_SUCCESS;
        *return_values_count = 2;
        return_idx = OFFSET_RET(struct arm_scmi_pin_control_protocol_attributes, attributes_low);
        return_values[return_idx] =
                      (pin_control_protocol.number_pins << PINCTRL_NUM_PINS_LOW) +
                      (pin_control_protocol.number_groups << PINCTRL_NUM_PIN_GROUPS_LOW);
        return_values[OFFSET_RET(
                struct arm_scmi_pin_control_protocol_attributes,
                attributes_high)] =
                      (pin_control_protocol.number_functions << PINCTRL_NUM_FUNCTIONS_LOW) +
                      (RESERVED << PINCTRL_NUM_RESERVED_LOW);
        break;
    case PINCTRL_PROTO_MSG_ATTRIB_MSG_ID:
        parameter_idx = OFFSET_PARAM(
            struct arm_scmi_pin_control_protocol_message_attributes, message_id);
        if(parameters[parameter_idx] > PINCTRL_NEGOTIATE_PROTOCOL_MSG_ID) {
            *status = SCMI_STATUS_NOT_FOUND;
        } else {
            *status = SCMI_STATUS_SUCCESS;
            *return_values_count = 1;
            return_values[OFFSET_RET(
                struct arm_scmi_pin_control_protocol_message_attributes,
                attributes)] = 0x0;
        }
        break;
    case PINCTRL_ATTRIB_MSG_ID:
        /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_attributes,
                identifier);
        pc_id = parameters[parameter_idx];

        /* Get flag (pin or group or function) */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_attributes,
                flags);
        flags = parameters[parameter_idx];
        *status = SCMI_STATUS_SUCCESS;

        /* If flag is pin */
        if (flags == FLAG_PINS) {
            if (pc_id >= pin_control_protocol.number_pins)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            *return_values_count = 2;
            /* Get attributes */
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_attributes, attributes);
            return_values[return_idx] =
                   pin_control_protocol.pin_control_extended_name_support[flags][pc_id] <<
                   PINCTRL_ATTRIBUTES_EXTENDED_NAME_LOW |
                   1 << PINCTRL_ATTRIBUTES_NUM_PINS_GROUPS_LOW;
            /* Get name */
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_attributes, name);
            str = (char *)&return_values[return_idx];
            if (pin_control_protocol.pin_control_extended_name_support[flags][pc_id]) {
                snprintf(name, MSG_SIZE, "pin_ext_uart_n_artx_%d", pc_id);
                snprintf(str, MSG_SIZE, "%s", name + strnlen(name, 25) - 16);
            } else
                snprintf(str, MSG_SIZE, "pin_x%d", pc_id);
        /* If flag id group */
        } else if (flags == FLAG_GROUPS) {
            if (pc_id >= pin_control_protocol.number_groups)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            *return_values_count = 2;
            /* Get attributes */
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_attributes, attributes);
            return_values[return_idx] =
                   pin_control_protocol.pin_control_extended_name_support[flags][pc_id] <<
                                        PINCTRL_ATTRIBUTES_EXTENDED_NAME_LOW |
                                        (pin_control_protocol.number_pins_in_group[pc_id] <<
                                        PINCTRL_ATTRIBUTES_NUM_PINS_GROUPS_LOW);
            /* Get name */
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_attributes, name);
            str = (char *)&return_values[return_idx];
            if (pin_control_protocol.pin_control_extended_name_support[flags][pc_id]) {
                 snprintf(name, MSG_SIZE, "grp_ext_uart_n_artx_%d", pc_id);
                 snprintf(str, MSG_SIZE, "%s", name + strnlen(name, 25) - 16);
            } else
                 snprintf(str, MSG_SIZE, "grp_rxtx_z%d", pc_id);
        /* If flag is function */
        } else if (flags == FLAG_FUNCTIONS) {
            if (pc_id >= pin_control_protocol.number_functions)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            *return_values_count = 2;
            /* Get attributes */
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_attributes, attributes);
            return_values[return_idx] = 
                   pin_control_protocol.pin_control_extended_name_support[flags][pc_id] <<
                                        PINCTRL_ATTRIBUTES_EXTENDED_NAME_LOW |
                                        (pin_control_protocol.number_groups_in_function[pc_id] <<
                                        PINCTRL_ATTRIBUTES_NUM_PINS_GROUPS_LOW);
            /* Get name */
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_attributes, name);
            str = (char *)&return_values[return_idx];
            if (pin_control_protocol.pin_control_extended_name_support[flags][pc_id]) {
                snprintf(name, MSG_SIZE, "f_ext_uart_n_artx_%d", pc_id);
                snprintf(str, MSG_SIZE, "%s", name + strnlen(name, 25) - 16);
            } else
                snprintf(str, MSG_SIZE, "f_uart_w%d", pc_id);
        } else {
            *status = SCMI_STATUS_NOT_FOUND;
        }
        break;
    case PINCTRL_LIST_ASSOC_MSG_ID:
        parameter_idx = OFFSET_PARAM(
            struct arm_scmi_pin_control_list_associations, list_parameters_flags);
        flags = parameters[parameter_idx];

        if (flags == FLAG_GROUPS) {
            /* Get pin control id */
            parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_list_associations,
                    identifier);
            pc_id = parameters[parameter_idx];
            /* Check if group identifier is valid */
            if (pc_id >= pin_control_protocol.number_groups) {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            /* Check if group index is valid */
            if(parameters[OFFSET_PARAM(
                          struct arm_scmi_pin_control_list_associations, index)] >=
                          pin_control_protocol.number_pins_in_group[pc_id])
            {
                *status = SCMI_STATUS_OUT_OF_RANGE;
                return;
            }
            *status = SCMI_STATUS_SUCCESS;
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_list_associations,
                                    list_returns_flags);
            return_values[return_idx] = pin_control_protocol.number_pins_in_group[pc_id]
                                        << PINCTRL_LIST_RETURNS_FLAGS_NUM_IDENTIFIERS_LOW;
            for (i = 0; i < pin_control_protocol.number_pins_in_group[pc_id]; i++)
            {
                return_values[OFFSET_RET(struct arm_scmi_pin_control_list_associations, array) + i]
                             = pin_control_protocol.group_associations[pc_id][i];
            }
            *return_values_count = 2;
        } else if (flags == FLAG_FUNCTIONS) {
            /* Get pin control id */
            parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_list_associations,
                    identifier);
            pc_id = parameters[parameter_idx];
            /* Check if function identifier is valid */
            if (pc_id >= pin_control_protocol.number_functions) {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            /* Check if funciton index is valid */
            if(parameters[OFFSET_PARAM(
                          struct arm_scmi_pin_control_list_associations, index)] >=
                          pin_control_protocol.number_groups_in_function[pc_id])
            {
                *status = SCMI_STATUS_OUT_OF_RANGE;
                return;
            }
            *status = SCMI_STATUS_SUCCESS;
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_list_associations,
                                    list_returns_flags);
            return_values[return_idx] = pin_control_protocol.number_groups_in_function[pc_id]
                                        << PINCTRL_LIST_RETURNS_FLAGS_NUM_IDENTIFIERS_LOW;
            for (i = 0; i < pin_control_protocol.number_groups_in_function[pc_id]; i++)
            {
                return_values[OFFSET_RET(struct arm_scmi_pin_control_list_associations, array) + i]
                             = pin_control_protocol.function_associations[pc_id][i];
            }
            *return_values_count = 2;
        } else {
            *status = SCMI_STATUS_NOT_FOUND;
            return;
        }
        break;
    case PINCTRL_SETTINGS_GET_MSG_ID:
        /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_settings_get,
                identifier);
        pc_id = parameters[parameter_idx];

        flags = ((parameters[OFFSET_PARAM(struct arm_scmi_pin_control_settings_get, attributes)] >>
                                           PINCTRL_SETTINGS_GET_SELECTOR_LOW) & 0x3);

        if (flags == FLAG_PINS) {
            /* Check if pin identifier is valid */
            if (pc_id >= pin_control_protocol.number_pins) {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            if(!(agent_pin_access[pc_id].access)) {
                *status = SCMI_STATUS_DENIED;
                return;
            }

            return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                        function_selected)] = pins_enabled_function[pc_id];
            *status = SCMI_STATUS_SUCCESS;

        } else if (flags == FLAG_GROUPS) {
            /* Check if group identifier is valid */
            if (pc_id >= pin_control_protocol.number_groups) {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            if(!(agent_group_access[pc_id].access)) {
                *status = SCMI_STATUS_DENIED;
                return;
            }

            return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                        function_selected)] = groups_enabled_function[pc_id];
            *status = SCMI_STATUS_SUCCESS;

        } else {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
        }
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_settings_get, attributes);
        all_configs = ((parameters[parameter_idx] >> PINCTRL_SETTINGS_GET_ALLCONFIGS_LOW) & 0x1);
        if (all_configs == 1) {
            total_num_configs = 2;
            return_idx = OFFSET_RET(struct arm_scmi_pin_control_settings_get, num_configs);
            return_values[return_idx] = total_num_configs;

            for (i = 0; i < total_num_configs; i++) {
                return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                           configs) + i * total_num_configs] = i;
                return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                            configs) + i * total_num_configs + 1] = 0;

            }
            *return_values_count = 1 + (total_num_configs * 2);
        }
        else if (flags == FLAG_PINS) {
                 return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                           configs) ] = pin_config[pc_id].type;
                 return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                            configs) + 1] = pin_config[pc_id].value;
                 *return_values_count = 3;
              }
        else {
                 return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                           configs) ] = group_config[pc_id].type;
                 return_values[OFFSET_RET(struct arm_scmi_pin_control_settings_get,
                            configs) + 1] = group_config[pc_id].value;
                 *return_values_count = 3;
         }

       break;
    case PINCTRL_SETTINGS_CONFIGURE_MSG_ID:
       /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_settings_configure,
                identifier);
        pc_id = parameters[parameter_idx];

        flags = ((parameters[OFFSET_PARAM(struct arm_scmi_pin_control_settings_configure,
                                        attributes)] >> PINCTRL_SETTINGS_CONFIGURE_SELECTOR_LOW) & 0x3);
        if (flags == FLAG_PINS) {
            /* Check if pin identifier is valid */
            if (pc_id >= pin_control_protocol.number_pins) {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            *status = SCMI_STATUS_SUCCESS;

        } else if (flags == FLAG_GROUPS) {
            /* Check if group identifier is valid */
            if (pc_id >= pin_control_protocol.number_groups) {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            *status = SCMI_STATUS_SUCCESS;

        } else {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
        }
        total_num_configs =
                ((parameters[OFFSET_PARAM(struct arm_scmi_pin_control_settings_configure,
                             attributes)] >> PINCTRL_SETTINGS_CONFIGURE_NUM_CONFIGS_LOW) & 0xFF);

        if(flags == FLAG_PINS) {
            if ((parameters[OFFSET_PARAM(struct arm_scmi_pin_control_settings_configure,
                                                                    configs)] > 20) &&
               (parameters[OFFSET_PARAM(struct arm_scmi_pin_control_settings_configure,
                                                                    configs)] < 192)) {
                    *status = SCMI_STATUS_INVALID_PARAMETERS;
                     return;
            }
            pin_config[pc_id].type = parameters[OFFSET_PARAM(struct
                          arm_scmi_pin_control_settings_configure, configs)];
            pin_config[pc_id].value = parameters[OFFSET_PARAM(struct
                          arm_scmi_pin_control_settings_configure, configs) + 1];
        } else {
            if ((parameters[OFFSET_PARAM(struct arm_scmi_pin_control_settings_configure,
                                                                    configs)] > 20) &&
               (parameters[OFFSET_PARAM(struct arm_scmi_pin_control_settings_configure,
                                                                    configs)] < 192)) {
                    *status = SCMI_STATUS_INVALID_PARAMETERS;
                     return;
            }
            group_config[pc_id].type = parameters[OFFSET_PARAM(struct
                          arm_scmi_pin_control_settings_configure, configs)];
            group_config[pc_id].value = parameters[OFFSET_PARAM(struct
                          arm_scmi_pin_control_settings_configure, configs) + 1];
        }
    break;
    case PINCTRL_FUNC_SELECT_MSG_ID:
        /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_function_select,
                identifier);
        pc_id = parameters[parameter_idx];

        /* Get function id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_function_select,
                function_id);
        function_id = parameters[parameter_idx];
        if( function_id >= pin_control_protocol.number_functions)
        {
            *status = SCMI_STATUS_NOT_FOUND;
            return;
        }

        /* Get flag (pin or group or function) */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_function_select,
                flags);
        flags = parameters[parameter_idx];
        *status = SCMI_STATUS_SUCCESS;

        /* If flag is pin */
        if (flags == FLAG_PINS) {
            if (pc_id >= pin_control_protocol.number_pins)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
         /* If flag is group */
        } else if (flags == FLAG_GROUPS) {
            if (pc_id >= pin_control_protocol.number_groups)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
        } else {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
        }
        break;
    case PINCTRL_REQUEST_MSG_ID:
        /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_request, identifier);
        pc_id = parameters[parameter_idx];
        /* Get flag (pin or group) */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_request, flags);
        flags = parameters[parameter_idx];
        *status = SCMI_STATUS_SUCCESS;

        /* If flag is pin */
        if (flags == FLAG_PINS) {
            if (pc_id >= pin_control_protocol.number_pins)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
         /* If flag is group */
        } else if (flags == FLAG_GROUPS) {
            if (pc_id >= pin_control_protocol.number_groups)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
        } else {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
        }
       break;
    case PINCTRL_RELEASE_MSG_ID:
         /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_release, identifier);
        pc_id = parameters[parameter_idx];
        /* Get flag (pin or group) */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_release, flags);
        flags = parameters[parameter_idx];
        *status = SCMI_STATUS_SUCCESS;

        /* If flag is pin */
        if (flags == FLAG_PINS) {
            if (pc_id >= pin_control_protocol.number_pins)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
         /* If flag is group */
        } else if (flags == FLAG_GROUPS) {
            if (pc_id >= pin_control_protocol.number_groups)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
        } else {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
        }
       break;
    case PINCTRL_NAME_GET_MSG_ID:
         /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_release, identifier);
        pc_id = parameters[parameter_idx];
        /* Get flag (pin or group) */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_release, flags);
        flags = parameters[parameter_idx];
        *status = SCMI_STATUS_SUCCESS;

        /* If flag is pin */
        if (flags == FLAG_PINS) {
            if (pc_id >= pin_control_protocol.number_pins)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            snprintf((char *)&return_values[1], MSG_SIZE, "pin_ext_uart_n_artx_%d", pc_id);

         /* If flag is group */
        } else if (flags == FLAG_GROUPS) {
            if (pc_id >= pin_control_protocol.number_groups)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            snprintf((char *)&return_values[1], MSG_SIZE, "grp_ext_uart_n_artx_%d", pc_id);

         /* If flag is function */
        } else if (flags == FLAG_FUNCTIONS) {
            if (pc_id >= pin_control_protocol.number_functions)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            snprintf((char *)&return_values[1], MSG_SIZE, "f_ext_uart_n_artx_%d", pc_id);

       } else {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
       }
        return_idx = OFFSET_RET(struct arm_scmi_pin_control_name_get, flags);
        return_values[return_idx] = 0;
        *return_values_count = strnlen((const char*)return_values, 25) + 1;
        break;
    case PINCTRL_SET_PERM_MSG_ID:
       /* Get pin control id */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_set_permissions, identifier);
        pc_id = parameters[parameter_idx];
        /* Get flag (pin or group) */
        parameter_idx = OFFSET_PARAM(struct arm_scmi_pin_control_set_permissions, flags);
        access = (parameters[parameter_idx] & 0x4) >> 2;
        flags = parameters[parameter_idx];

        /* Reserved bits[31:3] must be zero*/
        if ((flags & (~(0x7))) > 0)
        {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
        }
        flags = parameters[parameter_idx] & 0x3;
        /* If flag is pin */
        if (flags == FLAG_PINS) {
            if (pc_id >= pin_control_protocol.number_pins)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            agent_pin_access[pc_id].access = access;
            *status = SCMI_STATUS_SUCCESS;
            return;
        /* If flag is group */
        } else if (flags == FLAG_GROUPS) {
            if (pc_id >= pin_control_protocol.number_groups)
            {
                *status = SCMI_STATUS_NOT_FOUND;
                return;
            }
            agent_group_access[pc_id].access = access;
            *status = SCMI_STATUS_SUCCESS;
            return;
        } else {
            *status = SCMI_STATUS_INVALID_PARAMETERS;
            return;
        }
       *status = SCMI_STATUS_NOT_FOUND;
        break;
     case PINCTRL_NEGOTIATE_PROTOCOL_MSG_ID:
        *status = SCMI_STATUS_NOT_SUPPORTED;
        if (pin_control_protocol.protocol_version < 0x00020001) {
            *status = SCMI_STATUS_NOT_SUPPORTED;
            break;
        }
        flags = OFFSET_PARAM(struct arm_scmi_pin_control_negotiate_protocol, version);
        for (i = NUM_ELEMS(supported_pin_control_protocols); i > 0; i--) {
            if (supported_pin_control_protocols[i - 1] == parameters[flags]) {
                pin_control_protocol.protocol_version = parameters[flags];
                *status = SCMI_STATUS_SUCCESS;
                break;
            }
        }
        break;
   default:
        *status = SCMI_STATUS_NOT_FOUND;
    }
}
