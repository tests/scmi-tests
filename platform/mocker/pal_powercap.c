/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#include <pal_interface.h>
#include <pal_powercap_expected.h>

/**
  @brief   This API is used for checking num of powercap  domain
  @param   none
  @return  num of powercap domain
**/
uint32_t pal_powercap_get_expected_num_domains(void)
{
    return num_powercap_domain;
}

/**
  @brief   This API is used for checking powercap domain name
  @param   domain id
  @return  powercap domain name
**/
uint8_t *pal_powercap_get_expected_name(uint32_t domain_id)
{
    return (uint8_t *)powercap_domain_name[domain_id];
}

/**
  @brief   This API is used for checking powercap min pai
  @param   domain id
  @return  min pai
**/
uint32_t pal_powercap_get_min_pai(uint32_t domain_id)
{
    return min_pai[domain_id];
}

/**
  @brief   This API is used for checking powercap max_pai
  @param   domain id
  @return  max pai
**/
uint32_t pal_powercap_get_max_pai(uint32_t domain_id)
{
    return max_pai[domain_id];
}

/**
  @brief   This API is used for checking powercap pai step
  @param   domain id
  @return  pai step
**/
uint32_t pal_powercap_get_pai_step(uint32_t domain_id)
{
    return pai_step[domain_id];
}

/**
  @brief   This API is used for checking powercap min_power_cap
  @param   domain id
  @return  min_power_cap
**/
uint32_t pal_powercap_get_min_power_cap(uint32_t domain_id)
{
    return min_power_cap[domain_id];
}

/**
  @brief   This API is used for checking powercap max_power_cap
  @param   domain id
  @return  max_power_cap
**/
uint32_t pal_powercap_get_max_power_cap(uint32_t domain_id)
{
    return max_power_cap[domain_id];
}

/**
  @brief   This API is used for checking powercap power_cap_step
  @param   domain id
  @return  power_cap_step
**/
uint32_t pal_powercap_get_power_cap_step(uint32_t domain_id)
{
    return power_cap_step[domain_id];
}

/**
  @brief   This API is used for checking powercap sustainable power
  @param   domain id
  @return  sustainable power
**/
uint32_t pal_powercap_get_sustainable_power(uint32_t domain_id)
{
    return sustainable_power[domain_id];
}

/**
  @brief   This API is used for checking powercap accuracy
  @param   domain id
  @return  accuracy
**/
uint32_t pal_powercap_get_accuracy(uint32_t domain_id)
{
    return accuracy[domain_id];
}

/**
  @brief   This API is used for checking powercap pai
  @param   domain id
  @return  pai
**/
uint32_t pal_powercap_get_pai(uint32_t domain_id)
{
    return pai[domain_id];
}

/**
  @brief   This API is used for checking powercap power_cap
  @param   domain id
  @return  power_cap
**/
uint32_t pal_powercap_get_power_cap(uint32_t domain_id)
{
    return power_cap[domain_id];
}

/**
  @brief   This API is used for checking powercap avg_power
  @param   domain id
  @return  avg_power
**/
uint32_t pal_powercap_get_avg_power(uint32_t domain_id)
{
    return avg_power[domain_id];
}

/**
  @brief   This API is used for checking extended name support
  @param   domain id
  @return  1 on support
**/
uint32_t pal_powercap_get_extended_name_support(uint32_t domain_id)
{
    return powercap_extended_name_support[domain_id];
}

uint32_t pal_powercap_get_expected_supported_num_protocols(void)
{
    return NUM_ELEMS(supported_powercap_protocols);
}
