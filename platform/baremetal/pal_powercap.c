/** @file
 * Copyright (c) 2021, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifdef POWERCAP_PROTOCOL

#include "pal_interface.h"
#include "pal_powercap_expected.h"

extern void pal_powercap_set_expected(const void *);
static struct arm_scmi_powercap_expected *g_powercap_info;


/**
  @brief   Sets powercap domain protocol info
  @param   platform powercap domain protocol info
  @return  none
**/
void pal_powercap_set_expected(const void *info)
{
    if (info == NULL)
        return;

    g_powercap_info = (struct arm_scmi_powercap_expected *)info;
}

/*-----------  Common PAL API's across platforms ----------*/

/**
  @brief   This API is used for checking num of powercap domain
  @param   none
  @return  num of powercap domain
**/
uint32_t pal_powercap_get_expected_num_domains(void)
{
    if (g_powercap_info == NULL)
        return 0;

    return g_powercap_info->number_domains;
}

/**
  @brief   This API is used for checking powercap domain name
  @param   domain id
  @return  powercap domain name
**/
uint8_t *pal_powercap_get_expected_name(uint32_t domain_id)
{
    if (g_powercap_info == NULL)
        return NULL;

    return (uint8_t *)g_powercap_info->powercap_domain_name[domain_id];
}

#endif
