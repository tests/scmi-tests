/** @file
 * Copyright (c) 2023-2024, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/
#ifndef __PAL_PIN_CONTROL_EXPECTED_H__
#define __PAL_PIN_CONTROL_EXPECTED_H__

#include <pal_platform.h>

#define NONE       0

struct arm_scmi_pin_control_expected {
    /*
     * This enum is to specify the flag_mask and have to be ensured that
     *  it is in the same order as the below structure elements after
     *  flags_mask member.
     */
    enum {
        PIN_CONTROL_PROTOCOL_VERSION,
        PIN_CONTROL_NUMBER_GROUPS,
        PIN_CONTROL_NUMBER_PINS,
        PIN_CONTROL_NUMBER_FUNCTIONS,
        PIN_CONTROL_NUMBER_PINS_IN_GROUP,
        PIN_CONTROL_NUMBER_GROUPS_IN_FUNCTIONS,
        PIN_CONTROL_PIN_NAME,
        PIN_CONTROL_GROUP_NAME,
        PIN_CONTROL_FUNCTION_NAME,
        PIN_CONTROL_GROUP_ASSOCIATIONS,
        PIN_CONTROL_FUNCTION_ASSOCIATIONS
    } FLAGS;
    /*
     * The expectation is that there is no more
     * than 32 elements specified via this expected results data structure.
     * This mask is used to indicate which of the structure members hold valid
     * data.
     */
    uint32_t flags_mask;
    uint32_t protocol_version;

    /* number of pin groups */
    uint32_t number_groups;
    /* number of pins */
    uint32_t number_pins;
    /* number of functions */
    uint32_t number_functions;

    /* number of pins in the group*/
    uint16_t *number_pins_in_group;
    /* number of groups associated with the function*/
    uint16_t *number_groups_in_function;
    /*
     * This should have the extended name associated with a pin, group, or function in this
     * platform bounded by the number of identifiers.
     */
    char **pin_name;
    char **group_name;
    char **function_name;
    /* Array of group or pin identifiers*/
    uint32_t **group_associations;
    uint32_t **function_associations;
};

#endif /* __PAL_PIN_CONTROL_EXPECTED_H__ */
