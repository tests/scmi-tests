/** @file
 * Copyright (c) 2023, Arm Limited or its affiliates. All rights reserved.
 * SPDX-License-Identifier : Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
**/

#ifdef PIN_CONTROL_PROTOCOL

#include "pal_interface.h"
#include "pal_pin_control_expected.h"

extern void pal_pin_control_set_expected(const void *);
static struct arm_scmi_pin_control_expected *g_pin_control_info;

/**
  @brief   Sets pin control protocol info
  @param   platform pin control protocol info
  @return  none
**/
void pal_pin_control_set_expected(const void *info)
{
    if (info == NULL)
        return;
    g_pin_control_info = (struct arm_scmi_pin_control_expected *)info;
}

/*-----------  Common PAL API's across platforms ----------*/

/**
  @brief   This API is used for checking num of pins
  @param   none
  @return  num of pins
**/
uint32_t pal_pin_control_get_expected_num_pins(void)
{
    return g_pin_control_info->number_pins;
}

/**
  @brief   This API is used for checking num of groups
  @param   none
  @return  num of groups
**/
uint32_t pal_pin_control_get_expected_num_groups(void)
{
    return g_pin_control_info->number_groups;
}

/**
  @brief   This API is used for checking num of functions
  @param   none
  @return  num of functions
**/
uint32_t pal_pin_control_get_expected_num_functions(void)
{
    return g_pin_control_info->number_functions;
}

/**
  @brief   This API is used for checking num of pins a group
  @param   identifier of the pin
  @return  num of pins in a group
**/
uint32_t pal_pin_control_get_expected_pins_in_group(uint32_t identifier)
{
    return g_pin_control_info->number_pins_in_group[identifier];
}

/**
  @brief   This API is used for checking num of groups associated with a function
  @param   identifier of the function
  @return  num of groups in a function
**/
uint32_t pal_pin_control_get_expected_groups_in_func(uint32_t identifier)
{
    return g_pin_control_info->number_groups_in_function[identifier];
}

/**
  @brief   This API is used to get identifier of pin associated with the group
  @param   identifier of the group
  @return  pin identifier of group in index
**/
uint32_t pal_pin_control_get_expected_group_associations(uint32_t identifier, uint32_t index)
{
    return g_pin_control_info->group_associations[identifier][index];
}

/**
  @brief   This API is used to get identifier of group associated with the function
  @param   identifier of the function
  @return  pin identifier of function in index
**/
uint32_t pal_pin_control_get_expected_function_associations(uint32_t identifier, uint32_t index)
{
    return g_pin_control_info->function_associations[identifier][index];
}

/**
@brief   This API is used to check if pin or group is accessible for given agent id
           1. Caller       -  Test Suite.
  @param   agent_id     agent id
  @param   identifier   The identifier of pin or group
  @param   selector     Selector of pin or group
  @return  status       0 on access
**/
uint32_t pal_pinctrl_check_access(uint32_t agent_id, uint32_t identifier, uint32_t selector)

{
      return NONE;
}
#endif
